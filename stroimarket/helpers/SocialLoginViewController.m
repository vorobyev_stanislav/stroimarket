//
//  SocialLoginViewController.m
//  FFY
//
//  Created by apple on 26.01.16.
//  Copyright © 2016 Vorobyev Stanislav. All rights reserved.
//

#import "SocialLoginViewController.h"
#import <CommonCrypto/CommonDigest.h>

@interface SocialLoginViewController () <UIWebViewDelegate>

@end

@implementation SocialLoginViewController

#pragma mark - Life cycle

- (void)awakeFromNib {
  [super awakeFromNib];
}

- (void)viewDidLoad {

  [super viewDidLoad];
  // Do any additional setup after loading the view.

  NSString *urlString = @"";

  switch (self.type) {

  case SOCIALNET_VK:

    if (self.VKClientId != nil && self.VKScope) {

      urlString = [NSString
          stringWithFormat:@"https://oauth.vk.com/authorize?client_id=%@&display=mobile&redirect_uri=https://oauth.vk.com/blank.html&scope=%@&response_type=token&v=5.44", self.VKClientId, self.VKScope];
    }
    break;

  case SOCIALNET_FB:

    if (self.FBClientId != nil && self.FBScope) {

      urlString = [NSString
          stringWithFormat:@"https://www.facebook.com/dialog/oauth?client_id=%@&redirect_uri=%@&response_type=token&scope=%@", self.FBClientId, self.FBredirectUri, self.FBScope];
    }
    break;

  case SOCIALNET_OK:

    if (self.OKClientId != nil && self.OKScope) {

      urlString = [NSString
          stringWithFormat:@"https://connect.ok.ru/oauth/authorize?client_id=%@&scope=%@&response_type=token&redirect_uri=%@&layout=m", self.OKClientId, self.OKScope, self.OKredirectUri];
    }
    break;

  default:
    break;
  }

    NSLog(@"soc url %@", urlString);
  self.webBrowser.delegate = (id)self;
  NSURL *nsUrl = [NSURL URLWithString:urlString];
  NSURLRequest *request = [NSURLRequest
       requestWithURL:nsUrl
          cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
      timeoutInterval:30];
  [self.webBrowser loadRequest:request];
}

- (void)viewWillAppear:(BOOL)animated {

    [SVProgressHUD show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - actions

+ (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}

- (IBAction)closeSocialController:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIWebView delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {

    [SVProgressHUD dismiss];
}

- (BOOL)webView:(UIWebView *)webView
    shouldStartLoadWithRequest:(NSURLRequest *)request
                navigationType:(UIWebViewNavigationType)navigationType {

    NSString *url = request.URL.absoluteString;
    NSLog(@"loadUrl=%@", url);

    switch (self.type) {

    case SOCIALNET_VK:

        if ([url rangeOfString:@"#access_token"].location != NSNotFound) {

            NSArray *stringArray = [url componentsSeparatedByString:@"#"];
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

            for (NSString *param in [[NSString stringWithFormat:@"%@", stringArray[1]]
               componentsSeparatedByString:@"&"]) {
                NSArray *elts = [param componentsSeparatedByString:@"="];
                if ([elts count] < 2) {
                    continue;
                }
                [params setObject:[elts lastObject] forKey:[elts firstObject]];
            }

            [self.SocialDelegate VKLoginFinishWithAccessToken:params];

        } else {
            if ([url rangeOfString:@"#"].location != NSNotFound) {
                NSArray *stringArray = [url componentsSeparatedByString:@"#"];
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

                for (NSString *param in
             [[NSString stringWithFormat:@"%@", stringArray[1]]
                 componentsSeparatedByString:@"&"]) {
                    NSArray *elts = [param componentsSeparatedByString:@"="];
                    if ([elts count] < 2) {
                        continue;
                    }
                    [params setObject:[elts lastObject] forKey:[elts firstObject]];
                }
                self.webBrowser.hidden = YES;
                [self.SocialDelegate VKLoginFinishWithError:params];
            }
        }

    break;

    case SOCIALNET_FB:

          if([[[url componentsSeparatedByString:@"?"] objectAtIndex:0] isEqualToString:_FBredirectUri]) {
              if([url rangeOfString:@"#access_token"].location != NSNotFound) {
                  NSArray *stringArray = [url componentsSeparatedByString:@"#"];
                  NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                  
                  for (NSString *param in [[NSString stringWithFormat:@"%@", stringArray[1]]
                                           componentsSeparatedByString:@"&"]) {
                      NSArray *elts = [param componentsSeparatedByString:@"="];
                      if ([elts count] < 2)
                          continue;
                      [params setObject:[elts lastObject] forKey:[elts firstObject]];
                  }
                  self.webBrowser.hidden = YES;
                  [self.SocialDelegate FBLoginFinishWithAccessToken:params];
              }
              else {
                  if ([url rangeOfString:@"#"].location != NSNotFound) {
                      NSArray *stringArray = [url componentsSeparatedByString:@"#"];
                      NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                      
                      for (NSString *param in
                           [[NSString stringWithFormat:@"%@", stringArray[1]]
                            componentsSeparatedByString:@"&"]) {
                               NSArray *elts = [param componentsSeparatedByString:@"="];
                               if ([elts count] < 2)
                                   continue;
                               [params setObject:[elts lastObject] forKey:[elts firstObject]];
                           }
                      self.webBrowser.hidden = YES;
                      [self.SocialDelegate FBLoginFinishWithError:params];
                  }
              }
          }

        break;
            
    case SOCIALNET_OK:
        
        if([url rangeOfString:self.OKredirectUri].location != NSNotFound) {
            if([url rangeOfString:@"#access_token"].location != NSNotFound) {
                NSArray *stringArray = [url componentsSeparatedByString:@"#"];
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                
                for (NSString *param in [[NSString stringWithFormat:@"%@", stringArray[1]]
                                         componentsSeparatedByString:@"&"]) {
                    NSArray *elts = [param componentsSeparatedByString:@"="];
                    if ([elts count] < 2)
                        continue;
                    [params setObject:[elts lastObject] forKey:[elts firstObject]];
                }
                self.webBrowser.hidden = YES;
                [self.SocialDelegate OKLoginFinishWithAccessToken:params];
            }
            else {
                if ([url rangeOfString:@"#"].location != NSNotFound) {
                    NSArray *stringArray = [url componentsSeparatedByString:@"#"];
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    
                    for (NSString *param in
                         [[NSString stringWithFormat:@"%@", stringArray[1]]
                          componentsSeparatedByString:@"&"]) {
                             NSArray *elts = [param componentsSeparatedByString:@"="];
                             if ([elts count] < 2)
                                 continue;
                             [params setObject:[elts lastObject] forKey:[elts firstObject]];
                         }
                    self.webBrowser.hidden = YES;
                    [self.SocialDelegate OKLoginFinishWithError:params];
                }
            }
        }
        
        break;
  }
  return true;
}

#pragma mark - FB functions

+ (void)getVKProfile:(NSInteger)userId completed:(void(^)(NSString *firstName, NSString *lastName))completed {
    NSString *myurl = [NSString stringWithFormat:@"https://api.vk.com/method/users.get?user_ids=%li&fields=bdate", userId];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"resp %@", responseObject);
        if(completed) {
            NSArray *rr = [responseObject valueForKey:@"response"];
            completed([rr[0] valueForKey:@"first_name"], [rr[0] valueForKey:@"last_name"]);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        if(completed) {
            completed(@"", @"");
        }
        
    }];
}

+ (void)getFBRequest:(NSString*)request params:(NSDictionary*)params success:(void (^)(id))success failure:(void (^)(id))failure {
    NSString *paramsStr=@"";
    
    NSArray* keys = [params allKeys];
    int i=0;
    for(NSString* key in keys) {
        id obj = [params objectForKey:key];
        if(i>0)
        {
            paramsStr = [NSString stringWithFormat:@"%@&%@=%@",paramsStr, key, obj];
        }
        else
        {
            paramsStr = [NSString stringWithFormat:@"%@%@=%@",paramsStr, key, obj];
        }
        i++;
    }
    
    NSString *myurl = [NSString stringWithFormat:@"https://graph.facebook.com/%@?%@", request, paramsStr];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if(success) success(responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        if(failure) failure(error);
        
    }];
}

#pragma mark - OK functions

+ (void)getOKRequest:(NSString*)request sessionSecretKey:(NSString*)sessionKey success:(void (^)(id))success failure:(void (^)(id))failure {
    
//    NSString *sigRaw = [NSString stringWithFormat:@"application_key=%@format=jsonmethod=%@%@", OKappKey, request, sessionKey];
//    
//    NSString *sig = [self md5:sigRaw];
//    NSString *myurl = [NSString stringWithFormat:@"https://api.ok.ru/fb.do?application_key=%@&method=%@&access_token=%@&sig=%@&format=json", OKappKey, request, [[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"], sig];
//    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSLog(@"get url=%@", myurl);
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    
//    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        
//        if(success) success(responseObject);
//        
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        
//        if(failure) failure(error);
//        
//    }];
}

@end
