//
//  appSettings.m

//

#import "appSettings.h"
#import "Functions.h"

@implementation colorScheme

+ (NSDictionary *) getSchemes {
    static NSDictionary *schemes;
    if (!schemes) schemes = @{
                              @"navigationBarColor": @(0xf90015),
                              @"statusBarColor" : @(0xc40010),
                              @"backgroundViewColor" : @(0xf6f2f2),
                              @"backgroundCellColor" : @(0xffffff),
                              @"menuTextColor": @(0x000000),
                              @"labelTextColor1": @(0x6a6a6a),
                              @"placeholderColor" : @(0x8a8a8a),
                              @"yellowColor" : @(0xffb300),
                              };
    
    return schemes;
}


+ (UIColor *)colorWithSchemeName:(NSString *)schemeName {
    NSDictionary *schemes = [self getSchemes];
    NSNumber *colorNum = [schemes valueForKey:schemeName];
    return [Functions colorWithRGBHex:[colorNum longValue]];
}

@end

@implementation appSettings

@end
