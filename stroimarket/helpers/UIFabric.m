
#define DEFAULT_FONT @"HelveticaNeue"
#define ADOPT_FONTS 1

#import "UIFabric.h"
#import "Functions.h"

#import "appSettings.h"

@implementation UIFabric
+(UILabel *) labelWithText:(NSString *)text andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andSuperView:(UIView *)parent {
    
    UILabel *lab = [UILabel new];
    [parent addSubview:lab];
    lab.text = text;
    lab.textColor = color;
    if (!fontName) fontName = DEFAULT_FONT;
    if (ADOPT_FONTS) fontSize = fontSize * [Functions koefY];
    if (modif) lab.font = [UIFont fontWithName:[NSString stringWithFormat:@"%@-%@", fontName, modif] size:fontSize]; else lab.font = [UIFont fontWithName:fontName size:fontSize];
    return lab;
}

+ (UITextView *)textViewWithText:(NSString *)text andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andSuperView:(UIView *)parent {
    
    UITextView *textView = [UITextView new];
    textView.text = text;
    if (!fontName) fontName = DEFAULT_FONT;
    if (ADOPT_FONTS) fontSize = fontSize * [Functions koefY];
    textView.textColor = color;
    if (modif) textView.font = [UIFont fontWithName:[NSString stringWithFormat:@"%@-%@", fontName, modif] size:fontSize]; else textView.font = [UIFont fontWithName:fontName size:fontSize];
    [parent addSubview:textView];
    return textView;
}

+ (UIWebView *)webView:(id)target andSuperView:(UIView *)parent {
    
    UIWebView *webView = [UIWebView new];
    webView.delegate = target;
    webView.backgroundColor = [UIColor whiteColor];
    [parent addSubview:webView];
    
    return webView;
}

+ (UILabel *) labelWithText:(NSString *)text andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontSize:(float)fontSize andSuperView:(UIView *)parent {
    
    UILabel *lab = [UILabel new];
    [parent addSubview:lab];
    lab.text = text;
    lab.textColor = color;
    if (!fontName) fontName = DEFAULT_FONT;
    if (ADOPT_FONTS) fontSize = fontSize * [Functions koefY];
    lab.font = [UIFont fontWithName:fontName size:fontSize];
    return lab;
}

+ (UIScrollView *) scrollView:(UIView *)parent {
    
    UIScrollView *scroll = [UIScrollView new];
    [parent addSubview:scroll];
    
    return scroll;
}

+ (GMSMapView *)mapView:(UIView *)parent {
    GMSMapView *map = [GMSMapView new];
    [parent addSubview:map];
    map.myLocationEnabled = YES;
    return map;
}

+ (UIPageControl *)pageControl:(UIView *)parent {
    
    UIPageControl *page = [UIPageControl new];
    [parent addSubview:page];
    
    return page;
}

+ (UISearchBar *) searchBarWithPlaceholder:(NSString *)text andBackgroundColor:(UIColor *)color andInnerBackgroundColor:(UIColor *)inner andTextColor:(UIColor *)textColor andSuperView:(UIView *)parent {
    
    UISearchBar *search = [UISearchBar new];
    [parent addSubview:search];
    
    search.backgroundImage = [[UIImage alloc] init];
    search.backgroundColor = color;
    
    search.placeholder = text;
    
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setBackgroundColor:inner];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
                                                                                                 NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                 NSFontAttributeName : [UIFont systemFontOfSize:15]
                                                                                                 }];
    
    
//    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:textColor];
    
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{NSForegroundColorAttributeName:textColor}];
    
//    for (UIView *v in search.subviews)
//    {
//        if([Environment isVersion7OrHigher]) //checks UIDevice#systemVersion
//        {
//            for(id subview in v.subviews)
//            {
//                if ([v isKindOfClass:[UITextField class]])
//                {
//                    ((UITextField *)v).textColor = textColor;
//                }
//            }
//        }
//        
//        else
//        {
//            if ([v isKindOfClass:[UITextField class]])
//            {
//                ((UITextField *)v).textColor = color;
//            }
//        }
//    }
    
    return search;
}

+ (UISwitch *)SwitchOnTintColor:(UIColor *)onTintColor andThumbTintColor:(UIColor *)thumbTintColor andSuperView:(UIView *)parent andTarget:(id)target andAction:(SEL)action {
    
    UISwitch *switch1 = [UISwitch new];
    if(onTintColor) {
        switch1.onTintColor = onTintColor;
    }
    if(thumbTintColor) {
        switch1.thumbTintColor = thumbTintColor;
    }
    [switch1 addTarget:target action:action forControlEvents:UIControlEventValueChanged];
    
    [parent addSubview:switch1];
    
    return switch1;
}

+ (UISegmentedControl *) SegmentedControlWithButtons:(NSArray *)buttonsName andBackgroundColor:(UIColor *)background andTintColor:(UIColor *)tint andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andTarget:(id)target andAction:(SEL)action andSuperView:(UIView *)parent {
    
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:buttonsName];
    [parent addSubview:segment];
    segment.tintColor = tint;
    segment.selectedSegmentIndex = 0;
    segment.backgroundColor = background;
    if (!fontName) fontName = DEFAULT_FONT;
    if (ADOPT_FONTS) fontSize = fontSize * [Functions koefY];
    UIFont *font;
    if (modif) font = [UIFont fontWithName:[NSString stringWithFormat:@"%@-%@", fontName, modif] size:fontSize]; else font = [UIFont fontWithName:fontName size:fontSize];
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    if(color != nil) {
        NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName];
        [segment setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
        [segment setTitleTextAttributes:highlightedAttributes forState:UIControlStateNormal];
    }
    
    [segment addTarget:target action:action forControlEvents:UIControlEventValueChanged];
    
    return segment;
}

+ (UIView *)viewWithBackgroundColor:(UIColor *)color andSuperView:(UIView *)parent {
    
    UIView *v = [UIView new];
    [parent addSubview:v];
    v.backgroundColor = color;

    return v;
}

+(UIImageView *) imageViewWithImageName:(NSString *)image andContentMode:(UIViewContentMode)contentMode iconMode:(bool)iconMode  andSuperView:(UIView *)parent {
    
    UIImageView *im = [[UIImageView alloc] init];
    [parent addSubview:im];
    if (image) [im setImage:[UIImage imageNamed:image]];
    im.contentMode = contentMode;
    
    if (iconMode) im.image = [im.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    return im;
}


+ (UIButton *)buttonWithText:(NSString *)text andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontSize:(float)fontSize andBackgroundColor:(UIColor *)bcolor andImage:(NSString *)image andSuperView:(UIView *)parent {
    return [UIFabric buttonWithText:text andTextColor:color andFontName:fontName andFontModificator:nil andFontSize:fontSize andBackgroundColor:bcolor andImage:image andSuperView:parent];
}

+ (UIButton *)buttonWithText:(NSString *)text andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andBackgroundColor:(UIColor *)bcolor andImage:(NSString *)image andSuperView:(UIView *)parent {
    UIButton *btn = [UIButton new];
    [parent addSubview:btn];
    if (text) {
        [btn setTitle:text forState:UIControlStateNormal];
        [btn setTitleColor:color forState:UIControlStateNormal];
        if (!fontName) fontName = DEFAULT_FONT;
        if (ADOPT_FONTS) fontSize = fontSize * [Functions koefY];
        
        if (modif) btn.titleLabel.font = [UIFont fontWithName:[NSString stringWithFormat:@"%@-%@", fontName, modif] size:fontSize]; else btn.titleLabel.font = [UIFont fontWithName:fontName size:fontSize];
    }
    btn.backgroundColor = bcolor;
    if (image) {
        [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    }
    
    return btn;
    
}

+ (UIButton *)buttonWithImage:(NSString *)image andTarget:(id)target andSelector:(SEL)selector andSuperView:(UIView *)parent {
    UIButton *btn = [UIButton new];
    [parent addSubview:btn];
    if (image) {
        [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    }
    
    [btn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

+ (UIButton *)buttonWithUIImage:(UIImage *)image andTarget:(id)target andSelector:(SEL)selector andSuperView:(UIView *)parent {
    UIButton *btn = [UIButton new];
    [parent addSubview:btn];
    if (image) {
        [btn setImage:image forState:UIControlStateNormal];
    }
    
    [btn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    return btn;
    
}

+ (SHSPhoneTextField *)shsPhoneTextField:(NSString *)text andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontSize:(float)fontSize andSuperView:(UIView *)parent {
    
    SHSPhoneTextField *phone = [SHSPhoneTextField new];
    phone.text = text;
    if(color) {
        phone.textColor = color;
    }
    if (!fontName) fontName = DEFAULT_FONT;
    if (ADOPT_FONTS) fontSize = fontSize * [Functions koefY];
    phone.font = [UIFont fontWithName:fontName size:fontSize];
    [parent addSubview:phone];
    
    return phone;
}

+ (UITextField *)textFieldWithPlaceholder:(NSString *)text andPlaceholderColor:(UIColor *)placeholderColor andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andIsPassword:(bool)isPassword  andSuperView:(UIView *)parent {
    
    UITextField *inp = [UITextField new];
    [parent addSubview:inp];
    inp.placeholder = text;
    if (!fontName) fontName = DEFAULT_FONT;
    if (ADOPT_FONTS) fontSize = fontSize * [Functions koefY];
    if (modif) inp.font = [UIFont fontWithName:[NSString stringWithFormat:@"%@-%@", fontName, modif] size:fontSize]; else inp.font = [UIFont fontWithName:fontName size:fontSize];
    if (isPassword) inp.secureTextEntry = true;
    else inp.secureTextEntry = false;
    
    if(placeholderColor) {
        inp.attributedPlaceholder = [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName:placeholderColor}];
    }
    
    return inp;
 
}

+ (UIButton *) wcRoundButtonWithText:(NSString *)text andBackgroundImage:(NSString *)bImage andOnBackgroundImage:(NSString *)bOnImage orImage:(NSString *)image andSuperView:(UIView *)parent {
    UIButton *btn = [UIButton new];
    [parent addSubview:btn];
    float fontSize = 29;
    if (text) {
        [btn setTitle:text forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        NSString *fontName = DEFAULT_FONT;
        if (ADOPT_FONTS) fontSize = fontSize * [Functions koefY];
        
        btn.titleLabel.font = [UIFont fontWithName:[NSString stringWithFormat:@"%@-%@", fontName, @"Medium"] size:fontSize];
        if (bImage) [btn setBackgroundImage:[UIImage imageNamed:bImage] forState:UIControlStateNormal];
        if (bOnImage) [btn setBackgroundImage:[UIImage imageNamed:bOnImage] forState:UIControlStateHighlighted];
    }
    btn.backgroundColor = [UIColor clearColor];
    if (image) {
        [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
        if (bOnImage) [btn setImage:[UIImage imageNamed:bOnImage] forState:UIControlStateHighlighted];
    }
    
    //btn.layer.cornerRadius = 20;
    btn.clipsToBounds = YES;
    //btn.layer.borderWidth = 1;
    //btn.layer.borderColor = [[Functions colorWithRGBHex:0xeaeaea] CGColor];
 
    return btn;
    
}


@end
