//
//  Functions.m

//

#import "Functions.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

@implementation Functions

+(float)koefY {
    return SCREEN_MAX_LENGTH/667.0;
}

+(float)koefX {
    return SCREEN_MIN_LENGTH/375.0;
}


+ (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2 {
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    
    return [data1 isEqual:data2];
}

+ (UIColor *)colorWithRGBHex:(UInt32)hex
{
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:1.0f];
}

+(int)GetNetReachability
{
    NSLog(@"netstatus=%i",[[[NSUserDefaults standardUserDefaults] valueForKey:@"netstatus"] intValue]);
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"netstatus"] intValue]) return 1;
    else return 0;
}

+ (BOOL)IsIPhone4 {
    
    if(IS_IPHONE_4_OR_LESS) {
        return YES;
    }
    else {
        return NO;
    }
}

+ (BOOL)IsIPhone5 {
    
    if(IS_IPHONE_5) {
        return YES;
    }
    else {
        return NO;
    }
}

+ (BOOL)IsIPhone6 {
    if(IS_IPHONE_6) {
        return YES;
    }
    else {
        return NO;
    }
}


+ (UIBarButtonItem*)setMenuButton:(id)target action:(SEL)action
{
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_menu"];
    [backBtn setImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
     backBtn.frame = CGRectMake(0, 0, 44, 44);
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(40, 0, 44, 44)];
    backButtonView.bounds = CGRectOffset(backButtonView.bounds, 15, 0);
    [backButtonView addSubview:backBtn];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    
    return backButton;
}

+ (UIBarButtonItem*)setMoreButton:(id)target action:(SEL)action {
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_basket"];
    [backBtn setImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 30, 44);
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 44)];
    backButtonView.bounds = CGRectOffset(backButtonView.bounds, 0, 0);
    [backButtonView addSubview:backBtn];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    
    return backButton;
}

+ (UIBarButtonItem*)setBackButton:(id)target action:(SEL)action {
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"ic_arrow_back"];
    [backBtn setImage:backBtnImage forState:UIControlStateNormal];
    [backBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    backButtonView.bounds = CGRectOffset(backButtonView.bounds, 12, 0);
    [backButtonView addSubview:backBtn];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    
    return backButton;
}

+ (void)setupNavigationController:(UINavigationController *)navContr andTitle:(NSString *)title andItem:(UINavigationItem *) navItem withLeftBtn:(UIBarButtonItem*)leftBtn andRightBtn:(UIBarButtonItem*)rightBtn {
    
//    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
//    
//    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//        statusBar.backgroundColor = [colorScheme colorWithSchemeName:@"statusBarColor"];
//    }
    
    navContr.navigationBar.hidden = false;
    navContr.navigationBar.backItem.title = @"";
    navContr.navigationBar.barTintColor = [colorScheme colorWithSchemeName:@"navigationBarColor"];
    navContr.navigationBar.backgroundColor = [colorScheme colorWithSchemeName:@"navigationBarColor"];
    navContr.navigationBar.translucent = NO;
    navContr.navigationBar.tintColor = [UIColor whiteColor];
//    navContr.navigationBar.alpha = 0.1f;
    
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(60, -10, SCREEN_WIDTH - 120, 60)];
    titleLab.textAlignment = NSTextAlignmentLeft;
    titleLab.textColor = [UIColor whiteColor];
    [navContr.navigationBar addSubview:titleLab];
    
    UIFont *arialFont = [UIFont fontWithName:@"Roboto-Regular" size:19.0*[Functions koefY]];
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: arialFont forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:title attributes: arialDict];
    
    titleLab.attributedText = aAttrString;
    
    navItem.leftBarButtonItem = leftBtn;
    navItem.rightBarButtonItem = rightBtn;
}

+ (void)setupNavigationController:(UINavigationController *)navContr andSearchTitle:(NSString *)title andTarget:(id)target andItem:(UINavigationItem *)navItem withLeftBtn:(UIBarButtonItem*)leftBtn andRightBtn:(UIBarButtonItem*)rightBtn {
    
    //    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    //
    //    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
    //        statusBar.backgroundColor = [colorScheme colorWithSchemeName:@"statusBarColor"];
    //    }
    
    navContr.navigationBar.hidden = false;
//    navContr.navigationBar.alpha = 0.5f;
    navContr.navigationBar.backItem.title = @"";
//    navContr.navigationBar.barTintColor = [colorScheme colorWithSchemeName:@"navigationBarColor"];
    navContr.navigationBar.barTintColor = [UIColor colorWithRed:0.98 green:0 blue:0.08 alpha:0.5];
    navContr.navigationBar.backgroundColor = [UIColor colorWithRed:0.98 green:0 blue:0.08 alpha:0.5];//[colorScheme colorWithSchemeName:@"navigationBarColor"];
    navContr.navigationBar.translucent = NO;
    navContr.navigationBar.tintColor = [UIColor whiteColor];
    
    //Search
    UISearchBar *searchPlayer = [[UISearchBar alloc] initWithFrame:CGRectMake(30, 2, SCREEN_WIDTH - 80 - 30, 30)];
    searchPlayer.placeholder = title;
    searchPlayer.showsCancelButton = NO;
    searchPlayer.delegate = (id)target;
    
    searchPlayer.searchBarStyle = UISearchBarStyleMinimal;
    
    searchPlayer.barTintColor = [UIColor clearColor];
    [searchPlayer setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [searchPlayer sizeToFit];
    
    
    //Search Text field customization
    [UITextField appearanceWhenContainedIn:[UISearchBar class], nil].backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    [UITextField appearanceWhenContainedIn:[UISearchBar class], nil].textColor = [UIColor whiteColor];
//    [UITextField appearanceWhenContainedIn:[UISearchBar class], nil].font = THEME_FONT_BOLD(15.0);
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    id barButtonAppearanceInSearchBar = [UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil];
//    [barButtonAppearanceInSearchBar setTitleTextAttributes:@{NSFontAttributeName:THEME_FONT(15.0),
//                                                             NSForegroundColorAttributeName : [UIColor whiteColor]
//                                                             } forState:UIControlStateNormal];
    [barButtonAppearanceInSearchBar setTitle:@"Cancel"];
    
    [searchPlayer setValue:[UIColor whiteColor] forKeyPath:@"_searchField._placeholderLabel.textColor"];
    
    UITextField *searchBarTextField = [searchPlayer valueForKey:@"_searchField"];
    
    // Magnifying glass icon.
    UIImageView *leftImageView = (UIImageView *)searchBarTextField.leftView;
//    leftImageView.image = [LeftImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    leftImageView.tintColor = [UIColor whiteColor];
    
    // Clear button
    UIButton *clearButton = [searchBarTextField valueForKey:@"_clearButton"];
    [clearButton setImage:[clearButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    clearButton.tintColor = [UIColor whiteColor];
    
    navItem.titleView = searchPlayer;
    
    navItem.leftBarButtonItem = leftBtn;
    navItem.rightBarButtonItem = rightBtn;
}

+ (void)setupNavigationController:(UINavigationController *)navContr andSegmentButtons:(NSArray *)buttons andItem:(UINavigationItem *)navItem withSegmentTarget:(id)target andAction:(SEL)action {
    
    navContr.navigationBar.hidden = false;
    navContr.navigationBar.backItem.title = @"";
    navContr.navigationBar.topItem.title = @"";
    navContr.navigationBar.barTintColor = [Functions colorWithRGBHex:0xffc905];
    navContr.navigationBar.backgroundColor = [Functions colorWithRGBHex:0xffc905];
    navContr.navigationBar.translucent = NO;
    navContr.navigationBar.tintColor = [UIColor whiteColor];
    
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:buttons];
    [navContr.navigationBar addSubview:segment];
    
    float width = 250*[Functions koefX];//segment.frame.size.width;
    segment.frame = CGRectMake((navContr.navigationBar.frame.size.width - width)/2, 5, width, segment.frame.size.height);
    segment.tintColor = [UIColor whiteColor];
    segment.selectedSegmentIndex = 0;
//    segment.backgroundColor = background;
//    if (!fontName) fontName = DEFAULT_FONT;
//    if (ADOPT_FONTS) fontSize = fontSize * [Functions koefY];
    UIFont *font = [UIFont fontWithName:@"SFUIText-Regular" size:18*[Functions koefY]];
    [segment setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil] forState:UIControlStateNormal];
    
//    if(color != nil) {
//        NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName];
//        [segment setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
//        [segment setTitleTextAttributes:highlightedAttributes forState:UIControlStateNormal];
//    }
    
    [segment addTarget:target action:action forControlEvents:UIControlEventValueChanged];
    
}

+ (void)setupNavigationController:(UINavigationController *)navContr andView:(UIView *)view {
    
    navContr.navigationBar.hidden = false;
    navContr.navigationBar.backItem.title = @"";
    navContr.navigationBar.topItem.title = @"";
    navContr.navigationBar.barTintColor = [Functions colorWithRGBHex:0xffc905];
    navContr.navigationBar.backgroundColor = [Functions colorWithRGBHex:0xffc905];
    navContr.navigationBar.translucent = NO;
    navContr.navigationBar.tintColor = [UIColor whiteColor];
    
    [navContr.navigationBar addSubview:view];
    
}

+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContext(size);
    
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    [image drawInRect:rect];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

+ (void)addKeyboardToolbarForTextField:(UITextField *)sender width:(float)width target:(id)target doneTitle:(NSString *)doneTitle doneAction:(SEL)doneAction {
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, width, 40*[Functions koefY])];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:doneTitle style:UIBarButtonItemStyleDone target:target action:doneAction],
                           nil];
    numberToolbar.tintColor=[UIColor blackColor];
    [numberToolbar sizeToFit];
    sender.inputAccessoryView = numberToolbar;
}

+ (BOOL)NSStringIsValidEmail:(NSString *)checkString {
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
