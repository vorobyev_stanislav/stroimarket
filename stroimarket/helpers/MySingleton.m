#import "MySingleton.h"


@implementation MySingleton

@synthesize voteTimer;
@synthesize reload;
@synthesize menu;
@synthesize kostil;
@synthesize profile;
@synthesize current;
@synthesize catalogReload;
@synthesize edit;

static MySingleton * sharedMySingleton = NULL;

+(MySingleton *)sharedMySingleton {
    if (!sharedMySingleton || sharedMySingleton == NULL) {
		sharedMySingleton = [MySingleton new];
	}
	return sharedMySingleton;
}

- (void)dealloc {
    
}

@end
