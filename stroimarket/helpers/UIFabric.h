

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define MAS_SHORTHAND_GLOBALS 1
#import "Masonry.h"
@import GoogleMaps;
#import "SHSPhoneTextField.h"

@interface UIFabric : NSObject

+(UILabel *) labelWithText:(NSString *)text andTextColor:(UIColor *)color andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andSuperView:(UIView *)parent;

+(UILabel *) labelWithText:(NSString *)text andTextColor:(UIColor *)color andFontName:(NSString *)fontName andFontSize:(float)fontSize andSuperView:(UIView *)parent;

+ (UIWebView *)webView:(id)target andSuperView:(UIView *)parent;

+ (UIScrollView *) scrollView:(UIView *)parent;

+ (UIPageControl *)pageControl:(UIView *)parent;

+ (GMSMapView *)mapView:(UIView *)parent;

+ (SHSPhoneTextField *)shsPhoneTextField:(NSString *)text andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontSize:(float)fontSize andSuperView:(UIView *)parent;

+ (UITextView *)textViewWithText:(NSString *)text andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andSuperView:(UIView *)parent;

+ (UIView *)viewWithBackgroundColor:(UIColor *)color andSuperView:(UIView *)parent;

+ (UISearchBar *) searchBarWithPlaceholder:(NSString *)text andBackgroundColor:(UIColor *)color andInnerBackgroundColor:(UIColor *)inner andTextColor:(UIColor *)textColor andSuperView:(UIView *)parent;

+ (UISwitch *)SwitchOnTintColor:(UIColor *)onTintColor andThumbTintColor:(UIColor *)thumbTintColor andSuperView:(UIView *)parent andTarget:(id)target andAction:(SEL)action;

+(UIImageView *) imageViewWithImageName:(NSString *)image andContentMode:(UIViewContentMode)contentMode iconMode:(bool)iconMode andSuperView:(UIView *)parent;

+ (UIButton *)buttonWithText:(NSString *)text andTextColor:(UIColor *)color andFontName:(NSString *)fontName andFontSize:(float)fontSize andBackgroundColor:(UIColor *)bcolor andImage:(NSString *)image andSuperView:(UIView *)parent;

+ (UIButton *)buttonWithText:(NSString *)text andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andBackgroundColor:(UIColor *)bcolor andImage:(NSString *)image andSuperView:(UIView *)parent;

+ (UIButton *)buttonWithImage:(NSString *)image andTarget:(id)target andSelector:(SEL)selector andSuperView:(UIView *)parent;

+ (UIButton *)buttonWithUIImage:(UIImage *)image andTarget:(id)target andSelector:(SEL)selector andSuperView:(UIView *)parent;

+ (UITextField *)textFieldWithPlaceholder:(NSString *)text andPlaceholderColor:(UIColor *)placeholderColor  andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andIsPassword:(bool)isPassword  andSuperView:(UIView *)parent;

+ (UISegmentedControl *) SegmentedControlWithButtons:(NSArray *)buttonsName andBackgroundColor:(UIColor *)background andTintColor:(UIColor *)tint andTextColor:(UIColor *)color  andFontName:(NSString *)fontName andFontModificator:(NSString *)modif andFontSize:(float)fontSize andTarget:(id)target andAction:(SEL)action andSuperView:(UIView *)parent;

@end
