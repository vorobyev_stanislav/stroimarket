//
//  appSettings.h

//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "localizationService.h"

@interface colorScheme : NSObject
+ (UIColor *)colorWithSchemeName:(NSString *)schemeName;

@end

@interface appSettings : NSObject

@end
