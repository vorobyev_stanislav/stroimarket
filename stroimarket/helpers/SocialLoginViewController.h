//
//  SocialLoginViewController.h
//  FFY
//
//  Created by apple on 26.01.16.
//  Copyright © 2016 Vorobyev Stanislav. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "STPopup.h"

@protocol SocialLoginViewControllerDelegate

- (void)VKLoginFinishWithAccessToken:(NSDictionary *)params;
- (void)VKLoginFinishWithError:(NSDictionary *)error;

- (void)FBLoginFinishWithAccessToken:(NSDictionary *)params;
- (void)FBLoginFinishWithError:(NSDictionary *)error;

- (void)OKLoginFinishWithAccessToken:(NSDictionary *)params;
- (void)OKLoginFinishWithError:(NSDictionary *)error;

@end

typedef enum SocialNetType : NSUInteger {
    SOCIALNET_VK = 1,
    SOCIALNET_FB = 2,
    SOCIALNET_OK = 3
} SocialNetType;

@interface SocialLoginViewController : UIViewController

///Делегат контроллера
@property (assign, nonatomic) id<SocialLoginViewControllerDelegate> SocialDelegate;

///Тип социальной сети
@property (nonatomic) SocialNetType type;

///webview
@property (weak, nonatomic) IBOutlet UIWebView *webBrowser;

///clientId для ВК
@property (nonatomic) NSString *VKClientId;

///настройки доступа к ВК
@property (nonatomic) NSString *VKScope;

///redirectUri для FB
@property (nonatomic) NSString *FBredirectUri;

///clientId для FB
@property (nonatomic) NSString *FBClientId;

///настройки доступа к FB
@property (nonatomic) NSString *FBScope;


///redirect_uri для Ok
@property (nonatomic) NSString *OKredirectUri;

///clientId для Ok
@property (nonatomic) NSString *OKClientId;

///настройки доступа к Ok
@property (nonatomic) NSString *OKScope;

+ (void)getVKProfile:(NSInteger)userId completed:(void(^)(NSString *firstName, NSString *lastName))completed;

///FB get request - получение id пользователя
+ (void)getFBRequest:(NSString*)request params:(NSDictionary*)params success:(void (^)(id))success failure:(void (^)(id))failure;

///OK get request - получение uid пользователя
+ (void)getOKRequest:(NSString*)request sessionSecretKey:(NSString*)key success:(void (^)(id))success failure:(void (^)(id))failure;
@end
