//
//  Functions.h

//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static NSString* const mydomain=@"http://stroymarket-sk.ru/api/";
static NSString* const domain=@"http://stroymarket-sk.ru";

@interface Functions : NSObject
+ (UIColor *)colorWithRGBHex:(UInt32)hex;
+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius;
+ (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2;
+ (int)GetNetReachability;
+ (BOOL)IsIPhone4;
+ (BOOL)IsIPhone5;
+ (BOOL)IsIPhone6;
+ (UIBarButtonItem*)setMenuButton:(id)target action:(SEL)action;
+ (UIBarButtonItem*)setBackButton:(id)target action:(SEL)action;
+(void)setupNavigationController:(UINavigationController *)navContr andTitle:(NSString *) title andItem:(UINavigationItem *) navItem withLeftBtn:(UIBarButtonItem*)leftBtn andRightBtn:(UIBarButtonItem*)rightBtn;
+ (void)setupNavigationController:(UINavigationController *)navContr andSegmentButtons:(NSArray *)buttons andItem:(UINavigationItem *)navItem withSegmentTarget:(id)target andAction:(SEL)action;
+ (void)setupNavigationController:(UINavigationController *)navContr andView:(UIView *)view;
+ (void)setupNavigationController:(UINavigationController *)navContr andSearchTitle:(NSString *)title andTarget:(id)target andItem:(UINavigationItem *)navItem withLeftBtn:(UIBarButtonItem*)leftBtn andRightBtn:(UIBarButtonItem*)rightBtn;
+(float)koefY; //designed for iphone 6
+(float)koefX; //designed for iphone 6
+ (UIBarButtonItem*)setMoreButton:(id)target action:(SEL)action;
+ (void)addKeyboardToolbarForTextField:(UITextField *)sender width:(float)width target:(id)target doneTitle:(NSString *)doneTitle doneAction:(SEL)doneAction;
+ (BOOL)NSStringIsValidEmail:(NSString *)checkString;

@end
