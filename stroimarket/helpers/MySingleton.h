

@interface MySingleton : NSObject {
    
    NSTimer *voteTimer;
    NSInteger reload;
    NSInteger menu;
    NSInteger kostil;
    profile *profile;
    NSInteger current;
    NSInteger catalogReload;
    BOOL edit;
}
@property (nonatomic) NSTimer *voteTimer;
@property (nonatomic) NSInteger reload;
@property (nonatomic) NSInteger menu;
@property (nonatomic) NSInteger kostil;
@property (nonatomic) profile *profile;
@property (nonatomic) NSInteger current;
@property (nonatomic) NSInteger catalogReload;
@property (nonatomic) BOOL edit;

+(MySingleton *)sharedMySingleton;

@end
