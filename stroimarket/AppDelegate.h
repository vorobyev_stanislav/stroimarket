//
//  AppDelegate.h
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;
#import "notificationService.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;

@property SlideNavigationController *firstViewController;
@property UIViewController *mainController;
@property UIViewController *catalogController;
@property UIViewController *selectedGoodsController;
@property UIViewController *myOrdersController;
@property UIViewController *bonusesController;
@property UIViewController *notificationsController;
@property UIViewController *shopsController;
@property UIViewController *reportController;
@property UIViewController *aboutController;
@property (strong, nonatomic) notificationService *notificationService;

@end

