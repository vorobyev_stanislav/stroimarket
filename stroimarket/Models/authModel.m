//
//  authModel.m
//  stroimarket
//
//  Created by apple on 27.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "authModel.h"

@implementation profile

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[profile class]];
    
    [mapping mapKeyPath:@"city" toProperty:@"city"];
    [mapping mapKeyPath:@"last_name" toProperty:@"last_name"];
    [mapping mapKeyPath:@"first_name" toProperty:@"first_name"];
    [mapping mapKeyPath:@"patr_name" toProperty:@"patr_name"];
    
    [mapping mapKeyPath:@"lastname" toProperty:@"last_name"];
    [mapping mapKeyPath:@"name" toProperty:@"first_name"];
    [mapping mapKeyPath:@"patrname" toProperty:@"patr_name"];
    
    [mapping mapKeyPath:@"birthday" toProperty:@"birthday"];
    [mapping mapKeyPath:@"email" toProperty:@"email"];
    [mapping mapKeyPath:@"id" toProperty:@"userId"];
    [mapping mapKeyPath:@"bonus" toProperty:@"bonus"];
    
    return mapping;
}

@end

@implementation data

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[data class]];
    
    [mapping mapKeyPath:@"send_time" toProperty:@"send_time"];
    [mapping mapKeyPath:@"token" toProperty:@"token"];
    [mapping hasOne:[profile class] forKeyPath:@"profile" forProperty:@"profile" withObjectMapping:[profile objectMapping]];
    
    return mapping;
}

@end

@implementation authModel

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[authModel class]];
    
    [mapping mapKeyPath:@"status" toProperty:@"status"];
    [mapping hasOne:[data class] forKeyPath:@"data" forProperty:@"data" withObjectMapping:[data objectMapping]];
    [mapping mapKeyPath:@"errors" toProperty:@"errors"];
    
    return mapping;
}

@end

@implementation auth2Model

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[auth2Model class]];
    
    [mapping mapKeyPath:@"status" toProperty:@"status"];
    [mapping hasOne:[profile class] forKeyPath:@"data" forProperty:@"profile" withObjectMapping:[profile objectMapping]];
    [mapping mapKeyPath:@"errors" toProperty:@"errors"];
    
    return mapping;
}

@end
