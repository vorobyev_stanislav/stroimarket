//
//  notificationModel.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "notificationModel.h"

@implementation notificationModel

+ (NSArray *)indexedProperties {
    return @[@"notificationId"];
}

+ (NSString *)primaryKey {
    return @"notificationId";
}

+ (NSArray *)ignoredProperties {
    return @[];
}

@end
