//
//  cellModel.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface stockModel : NSObject

@property NSInteger stockId;
@property NSString *name;
@property NSString *date_active_from;
@property NSString *date_active_to;
@property NSString *text;
@property NSString *image;

+ (EKObjectMapping *)objectMapping;

@end

@interface newsModel : NSObject

@property NSInteger newsId;
@property NSString *name;
@property NSString *date_create;
@property NSString *preview;
@property NSString *text;

+ (EKObjectMapping *)objectMapping;

@end

@interface productDayModel : NSObject

@property NSInteger productId;
@property NSString *name;
@property NSString *desc;
@property NSString *image;
@property NSString *price;

+ (EKObjectMapping *)objectMapping;

@end

@interface cellModel : NSObject

@property NSString *name;
@property NSString *value;
@property NSInteger type;

@property BOOL status;
@property NSArray<newsModel *> *data;
@property NSArray<stockModel *> *stocks;

+ (EKObjectMapping *)objectMapping;

@end

@interface slider : NSObject

@property NSString *image;

+ (EKObjectMapping *)objectMapping;

@end

@interface cell2Model : NSObject

@property BOOL status;
@property NSArray<stockModel *> *data;

+ (EKObjectMapping *)objectMapping;

@end

@interface mainModel : NSObject

@property NSArray<stockModel *> *promo;
@property NSArray<newsModel *> *news;
@property NSArray<slider *> *slides;
@property NSArray<productDayModel *> *productday;

+ (EKObjectMapping *)objectMapping;

@end

@interface cell3Model : NSObject

@property BOOL status;
@property mainModel *data;

+ (EKObjectMapping *)objectMapping;

@end
