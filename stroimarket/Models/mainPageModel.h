//
//  mainPageModel.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface object : NSObject

@property NSInteger objectId;
@property NSString *avatar;
@property NSString *name;
@property NSString *text;

@end

@interface mainPageModel : NSObject

@property NSString *title;
@property NSString *titleAll;
@property NSString *type;
@property NSArray<object *> *objects;

@end
