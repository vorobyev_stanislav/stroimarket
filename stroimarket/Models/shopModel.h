//
//  shopModel.h
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface shopModel : NSObject

@property NSInteger shopId;
@property NSString *address;
@property NSString *phone;
@property float latitude;
@property float longitude;

@end
