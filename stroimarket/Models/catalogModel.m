//
//  catalogModel.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "catalogModel.h"

@implementation goods

+ (NSArray *)indexedProperties {
    return @[@"goodsId", @"parent"];
}

+ (NSString *)primaryKey {
    return @"goodsId";
}

+ (NSArray *)ignoredProperties {
    return @[];
}

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[goods class]];
    
    [mapping mapKeyPath:@"id" toProperty:@"goodsId"];
    [mapping mapKeyPath:@"name" toProperty:@"name"];
    [mapping mapKeyPath:@"description" toProperty:@"desc"];
    [mapping mapKeyPath:@"image" toProperty:@"image"];
    [mapping mapKeyPath:@"min_price" toProperty:@"price"];
    [mapping mapKeyPath:@"price" toProperty:@"price"];
    
    return mapping;
}

@end

@implementation collectionImages

+ (NSArray *)indexedProperties {
    return @[@"parent"];
}

+ (NSArray *)ignoredProperties {
    return @[];
}

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[collectionImages class]];
    
    [mapping mapKeyPath:@"image" toProperty:@"image"];
    
    return mapping;
}


@end

@implementation dataObj

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[dataObj class]];
    
    [mapping hasMany:[collectionImages class] forKeyPath:@"collection_images" forProperty:@"collectionImages" withObjectMapping:[collectionImages objectMapping]];
    [mapping hasMany:[goods class] forKeyPath:@"items" forProperty:@"items" withObjectMapping:[goods objectMapping]];
    
    return mapping;
}

@end

@implementation catalogModel2

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[catalogModel2 class]];
    
    [mapping mapKeyPath:@"status" toProperty:@"status"];
    [mapping mapKeyPath:@"errors" toProperty:@"errors"];
    [mapping hasOne:[dataObj class] forKeyPath:@"data" forProperty:@"data" withObjectMapping:[dataObj objectMapping]];
    
    return mapping;
}

@end

@implementation catalogModel

+ (NSArray *)indexedProperties {
    return @[@"catalogId", @"parent"];
}

+ (NSString *)primaryKey {
    return @"catalogId";
}

+ (NSArray *)ignoredProperties {
    return @[@"status", @"data", @"errors"];
}

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[catalogModel class]];
    
    [mapping mapKeyPath:@"status" toProperty:@"status"];
    [mapping hasMany:[goods class] forKeyPath:@"data" forProperty:@"data" withObjectMapping:[goods objectMapping]];
    
    return mapping;
}

@end
