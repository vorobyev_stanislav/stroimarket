//
//  orderModel.m
//  stroimarket
//
//  Created by apple on 22.05.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "orderModel.h"

@implementation selectedGoods

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[selectedGoods class]];
    
    [mapping mapKeyPath:@"id" toProperty:@"goodsId"];
    [mapping mapKeyPath:@"currency" toProperty:@"currency"];
    [mapping mapKeyPath:@"name" toProperty:@"name"];
    [mapping mapKeyPath:@"price" toProperty:@"price"];
    [mapping mapKeyPath:@"product_id" toProperty:@"productId"];
    [mapping mapKeyPath:@"quantity" toProperty:@"quantity"];
    [mapping mapKeyPath:@"collection_name" toProperty:@"collection"];
    [mapping mapKeyPath:@"product_image" toProperty:@"image"];
    
    return mapping;
}

@end

@implementation order

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[order class]];
    
    [mapping mapKeyPath:@"id" toProperty:@"orderId"];
    [mapping mapKeyPath:@"date_order" toProperty:@"date_order"];
    [mapping mapKeyPath:@"date_update_order" toProperty:@"date_update_order"];
    [mapping mapKeyPath:@"date_status" toProperty:@"date_status"];
    [mapping mapKeyPath:@"sum_paid" toProperty:@"sum_paid"];
    
    return mapping;
}

@end

@implementation orderModel

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[orderModel class]];
    
    [mapping mapKeyPath:@"status" toProperty:@"status"];
    [mapping hasMany:[selectedGoods class] forKeyPath:@"data" forProperty:@"data" withObjectMapping:[selectedGoods objectMapping]];
    [mapping mapKeyPath:@"errors" toProperty:@"errors"];
    
    return mapping;
}

@end

@implementation order2Model

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[order2Model class]];
    
    [mapping mapKeyPath:@"status" toProperty:@"status"];
    [mapping hasMany:[order class] forKeyPath:@"data" forProperty:@"data" withObjectMapping:[order objectMapping]];
    [mapping mapKeyPath:@"errors" toProperty:@"errors"];
    
    return mapping;
}

@end
