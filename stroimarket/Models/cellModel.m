//
//  cellModel.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "cellModel.h"

@implementation stockModel

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[stockModel class]];
    
    [mapping mapKeyPath:@"id" toProperty:@"stockId"];
    [mapping mapKeyPath:@"name" toProperty:@"name"];
    [mapping mapKeyPath:@"text" toProperty:@"text"];
    [mapping mapKeyPath:@"date_active_from" toProperty:@"date_active_from"];
    [mapping mapKeyPath:@"date_active_to" toProperty:@"date_active_to"];
    [mapping mapKeyPath:@"image" toProperty:@"image"];
    
    return mapping;
}

@end


@implementation newsModel

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[newsModel class]];
    
    [mapping mapKeyPath:@"id" toProperty:@"newsId"];
    [mapping mapKeyPath:@"name" toProperty:@"name"];
    [mapping mapKeyPath:@"text" toProperty:@"text"];
    [mapping mapKeyPath:@"date_create" toProperty:@"date_create"];
    [mapping mapKeyPath:@"image" toProperty:@"preview"];
    
    return mapping;
}

@end

@implementation slider

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[slider class]];
    
    [mapping mapKeyPath:@"image" toProperty:@"image"];
    
    return mapping;
}

@end

@implementation cellModel

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[cellModel class]];
    
    [mapping mapKeyPath:@"status" toProperty:@"status"];
    [mapping hasMany:[newsModel class] forKeyPath:@"data" forProperty:@"data" withObjectMapping:[newsModel objectMapping]];
    
    return mapping;
}

@end


@implementation cell2Model

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[cell2Model class]];
    
    [mapping mapKeyPath:@"status" toProperty:@"status"];
    [mapping hasMany:[stockModel class] forKeyPath:@"data" forProperty:@"data" withObjectMapping:[stockModel objectMapping]];
    
    return mapping;
}

@end

@implementation productDayModel

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[productDayModel class]];
    
    [mapping mapKeyPath:@"id" toProperty:@"productId"];
    [mapping mapKeyPath:@"description" toProperty:@"desc"];
    [mapping mapKeyPath:@"image" toProperty:@"image"];
    [mapping mapKeyPath:@"name" toProperty:@"name"];
    [mapping mapKeyPath:@"price" toProperty:@"price"];
    
    return mapping;
}

@end

@implementation mainModel

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[mainModel class]];
    
    [mapping hasMany:[stockModel class] forKeyPath:@"promo" forProperty:@"promo" withObjectMapping:[stockModel objectMapping]];
    [mapping hasMany:[newsModel class] forKeyPath:@"news" forProperty:@"news" withObjectMapping:[newsModel objectMapping]];
    [mapping hasMany:[slider class] forKeyPath:@"slides" forProperty:@"slides" withObjectMapping:[slider objectMapping]];
    [mapping hasMany:[productDayModel class] forKeyPath:@"productday" forProperty:@"productday" withObjectMapping:[productDayModel objectMapping]];
    
    return mapping;
}

@end

@implementation cell3Model

+ (EKObjectMapping *)objectMapping {
    EKObjectMapping * mapping = [[EKObjectMapping alloc] initWithObjectClass:[cell3Model class]];
    
    [mapping mapKeyPath:@"status" toProperty:@"status"];
    [mapping hasOne:[mainModel class] forKeyPath:@"data" forProperty:@"data" withObjectMapping:[mainModel objectMapping]];
    
    return mapping;
}

@end
