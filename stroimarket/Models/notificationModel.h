//
//  notificationModel.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface notificationModel : RLMObject

@property NSInteger notificationId;
@property NSString *notification;

@end
