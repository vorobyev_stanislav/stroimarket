//
//  catalogModel.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface goods : RLMObject

@property NSInteger goodsId;
@property NSString *name;
@property NSString *type;
@property NSString *price;
@property NSString *desc;
@property NSString *image;
@property NSInteger parent;

+ (EKObjectMapping *)objectMapping;

@end

@interface collectionImages : RLMObject

@property NSString *image;
@property NSInteger parent;

+ (EKObjectMapping *)objectMapping;

@end

@interface dataObj : NSObject

@property NSArray<collectionImages *> *collectionImages;
@property NSArray<goods *> *items;

+ (EKObjectMapping *)objectMapping;

@end

@interface catalogModel2 : NSObject

@property BOOL status;
@property dataObj *data;
@property NSArray *errors;

+ (EKObjectMapping *)objectMapping;

@end

@interface catalogModel : RLMObject

@property NSInteger catalogId;
@property NSString *name;
@property NSString *code;
@property NSString *image;
@property NSInteger parent;
@property NSString *price;

@property BOOL status;
@property NSArray *data;
@property NSArray *errors;

+ (EKObjectMapping *)objectMapping;

@end
