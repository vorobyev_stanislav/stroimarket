//
//  authModel.h
//  stroimarket
//
//  Created by apple on 27.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface profile : NSObject

@property NSInteger userId;
@property NSString *city;
@property NSString *last_name;
@property NSString *first_name;
@property NSString *patr_name;
@property NSString *birthday;
@property NSString *email;
@property NSString *phone;
@property NSInteger bonus;

+ (EKObjectMapping *)objectMapping;

@end

@interface data : NSObject

@property NSString *send_time;
@property NSString *token;
@property profile *profile;

+ (EKObjectMapping *)objectMapping;

@end

@interface authModel : NSObject

@property BOOL status;
@property data *data;
@property NSArray *errors;

+ (EKObjectMapping *)objectMapping;

@end

@interface auth2Model : NSObject

@property BOOL status;
@property profile *profile;
@property NSArray *errors;

+ (EKObjectMapping *)objectMapping;

@end
