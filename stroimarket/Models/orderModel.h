//
//  orderModel.h
//  stroimarket
//
//  Created by apple on 22.05.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface selectedGoods : NSObject

@property NSInteger goodsId;
@property NSString *currency;
@property NSString *name;
@property NSString *collection;
@property NSString *price;
@property NSString *image;
@property NSInteger productId;
@property float quantity;

+ (EKObjectMapping *)objectMapping;

@end

@interface order : NSObject

@property NSInteger orderId;
@property NSString *date_order;
@property NSString *date_update_order;
@property NSString *date_status;
@property float sum_paid;

+ (EKObjectMapping *)objectMapping;

@end

@interface orderModel : NSObject

@property BOOL status;
@property NSArray *data;
@property NSArray *errors;

+ (EKObjectMapping *)objectMapping;

@end

@interface order2Model : NSObject

@property BOOL status;
@property NSArray *data;
@property NSArray *errors;

+ (EKObjectMapping *)objectMapping;

@end
