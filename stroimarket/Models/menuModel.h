//
//  menuModel.h
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface menuModel : NSObject

@property NSString *avatar;
@property NSString *name;

@end
