//
//  networkService.m
//  stroimarket
//
//  Created by apple on 27.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "networkService.h"
@import FirebaseMessaging;

@implementation networkService

- (void)loginWithPhone:(NSString *)phone andEmail:(NSString *)email completed:(void (^)(BOOL))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@login?phone_number=%@&email=%@", mydomain, phone, email];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        authModel *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[authModel objectMapping]];
        
        if(completed) {
            completed(resp.status);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed(NO);
        }
        
    }];

}

- (void)validateCode:(NSString *)code withPhone:(NSString *)phone completed:(void (^)(BOOL, NSString *, profile *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@validate?phone_number=%@&kod=%@", mydomain, phone, code];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        authModel *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[authModel objectMapping]];
        
        if(resp.data.profile) {
            [[NSUserDefaults standardUserDefaults] setValue:resp.data.token forKey:@"authToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            [[NSUserDefaults standardUserDefaults] setValue:resp.data.token forKey:@"regAuthToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        if(resp.data.profile) {
            [[FIRMessaging messaging] subscribeToTopic:[NSString stringWithFormat:@"/topics/%li", (long)resp.data.profile.userId]];
            NSLog(@"/topics/%li", (long)resp.data.profile.userId);
        }
        
        if(completed) {
            
            completed(resp.status, resp.data.token, resp.data.profile);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed(NO, nil, nil);
        }
        
    }];
    
}

- (void)registerUser:(void (^)(BOOL))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@registerprofile?token=%@&city=%@&last_name=%@&first_name=%@&patr_name=%@&birthday=%@&email=%@&phone=%@&dev_token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"regAuthToken"], [MySingleton sharedMySingleton].profile.city, [MySingleton sharedMySingleton].profile.last_name, [MySingleton sharedMySingleton].profile.first_name, [MySingleton sharedMySingleton].profile.patr_name, [MySingleton sharedMySingleton].profile.birthday, [MySingleton sharedMySingleton].profile.email, [MySingleton sharedMySingleton].profile.phone, [[NSUserDefaults standardUserDefaults] valueForKey:@"fcmToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        authModel *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[authModel objectMapping]];
        
        if(resp.status) {
            [[NSUserDefaults standardUserDefaults] setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"regAuthToken"] forKey:@"authToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        if(resp.data.profile) {
            [[FIRMessaging messaging] subscribeToTopic:[NSString stringWithFormat:@"/topics/%li", (long)resp.data.profile.userId]];
            NSLog(@"/topics/%li", (long)resp.data.profile.userId);
        }
        
        if(completed) {
            completed(resp.status);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed(NO);
        }
        
    }];
    
}

- (void)getProfile:(void (^)(profile *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@getprofile?token=%@&dev_token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"], [[NSUserDefaults standardUserDefaults] valueForKey:@"fcmToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        auth2Model *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[auth2Model objectMapping]];
        
        if(resp.profile) {
            [[FIRMessaging messaging] subscribeToTopic:[NSString stringWithFormat:@"/topics/%li", (long)resp.profile.userId]];
            NSLog(@"/topics/%li", (long)resp.profile.userId);
        }
        
        [[NSUserDefaults standardUserDefaults] setValue:@(resp.profile.bonus) forKey:@"bonus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if(completed) {
            completed(resp.profile);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed([profile new]);
        }
        
    }];
    
}

- (void)setProfile:(profile *)profile completed:(void(^)(BOOL))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@setprofile?token=%@&last_name=%@&first_name=%@&patr_name=%@&birthday=%@&email=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"], profile.last_name, profile.first_name, profile.patr_name, profile.birthday, profile.email];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        auth2Model *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[auth2Model objectMapping]];
        
        if(completed) {
            completed(resp.status);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed(NO);
        }
        
    }];
    
}

- (void)setTokens:(void(^)())completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@settokens?token=%@&dev_token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"], [[NSUserDefaults standardUserDefaults] valueForKey:@"fcmToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
//        auth2Model *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[auth2Model objectMapping]];
//        
//        if(completed) {
//            completed(resp.status);
//        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
//        if(completed) {
//            completed(NO);
//        }
        
    }];
    
}

- (void)resendCode:(NSString *)phone completed:(void(^)(BOOL, NSString *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@sendsms?phone=%@", mydomain, phone];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        auth2Model *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[auth2Model objectMapping]];

        if(completed) {
            completed(resp.status, resp.errors[0]);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed(NO, @"Ошибка соединения!");
        }
        
    }];
    
}

- (void)sendMessage:(NSString *)message completed:(void(^)(BOOL))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@feedback?token=%@&message=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"], message];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        auth2Model *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[auth2Model objectMapping]];
        
        if(completed) {
            completed(resp.status);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed(NO);
        }
        
    }];
    
}

- (void)getNews:(void(^)(NSArray *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@news?token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        cellModel *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[cellModel objectMapping]];
        
        if(resp.status) {
            if(completed) {
                completed(resp.data);
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed([NSArray new]);
        }
        
    }];
    
}

- (void)getStock:(void(^)(NSArray *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@promo?token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        cell2Model *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[cell2Model objectMapping]];
        
        if(resp.status) {
            if(completed) {
                completed(resp.data);
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed([NSArray new]);
        }
        
    }];
    
}

- (void)getMain:(void(^)(NSArray *, NSArray *, NSArray *, NSArray *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@mainscreen?token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        cell3Model *resp = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[cell3Model objectMapping]];
        NSLog(@"%i %@", resp.status, resp.data.slides);
        if(resp.status) {
            if(completed) {
                completed(resp.data.slides, resp.data.promo, resp.data.news, resp.data.productday);
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        if(completed) {
            completed([NSArray new], [NSArray new], [NSArray new], [NSArray new]);
        }
        
    }];
    
}

@end
