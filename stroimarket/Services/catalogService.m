//
//  catalogService.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "catalogService.h"

@implementation catalogService

- (void)parseCatalog:(NSDictionary *)catalog {
    
    catalogModel *cc = [catalogModel new];
    cc.catalogId = [[catalog valueForKey:@"id"] integerValue];
    cc.name = [catalog valueForKey:@"name"];
    cc.image = [catalog valueForKey:@"image"];
    cc.code = [catalog valueForKey:@"code"];
    cc.price = [catalog valueForKey:@"min_price"];
    if([catalog valueForKey:@"parent"] != [NSNull null]) {
        cc.parent = [[catalog valueForKey:@"parent"] integerValue];
    } else {
        cc.parent = 0;
    }
    if(cc.parent == 0 && [cc.image isEqual:[NSNull null]]) {
        return;
    }
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [catalogModel createOrUpdateInRealm:[RLMRealm defaultRealm] withValue:cc];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    NSDictionary *childe = [catalog valueForKey:@"children"];
    if([[catalog valueForKey:@"children"] isKindOfClass:[NSDictionary class]]) {
        NSArray *kies = [childe allKeys];
        for(int i=0;i<kies.count;i++) {
            NSDictionary *subcat = [childe valueForKey:kies[i]];
            [self parseCatalog:subcat];
        }
    }
}

- (void)getCatalogs:(void(^)(NSArray<catalogModel *> *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@catalog?token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        NSMutableArray *catalogs = [NSMutableArray new];
        if([[responseObject valueForKey:@"status"] boolValue]) {
            
            [[RLMRealm defaultRealm] beginWriteTransaction];
            RLMResults *res = [catalogModel allObjects];
            for(catalogModel *cc in res) {
                [[RLMRealm defaultRealm] deleteObject:cc];
            }
            [[RLMRealm defaultRealm] commitWriteTransaction];
            
            NSDictionary *data = [responseObject valueForKey:@"data"];
            NSArray *kies = [data allKeys];
            for(int i=0;i<kies.count;i++) {
                NSDictionary *cat = [data valueForKey:kies[i]];
                [self parseCatalog:cat];
            }
            
            [self getCatalogFromDB:0 inArray:catalogs];
        }
        
        if(completed) {
            completed(catalogs);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        
        NSMutableArray *catalogs = [NSMutableArray new];
        [self getCatalogFromDB:0 inArray:catalogs];
        
        if(completed) {
            completed([NSArray new]);
        }
        
    }];
}

- (void)getSubCatalog:(NSInteger)catalogId completed:(void(^)(NSArray<catalogModel *> *))completed {
    
    NSMutableArray *catalogs = [NSMutableArray new];
    [self getCatalogFromDB:catalogId inArray:catalogs];
    
    if(completed) {
        completed(catalogs);
    }
}

- (void)getCatalogFromDB:(NSInteger)catalogId inArray:(NSMutableArray *)catalogs {
    RLMResults *res = [catalogModel objectsWhere:[NSString stringWithFormat:@"parent = %li", (long)catalogId]];
    for(catalogModel *cc in res) {
        catalogModel *newCC = [catalogModel new];
        newCC.catalogId = cc.catalogId;
        newCC.name = cc.name;
        newCC.code = cc.code;
        newCC.image = cc.image;
        newCC.parent = cc.parent;
        newCC.price = cc.price;
        [catalogs addObject:newCC];
    }
}

- (void)getCatalogFromDB:(NSInteger)catalogId completed:(void(^)(NSMutableArray *))completed {
    RLMResults *res = [catalogModel objectsWhere:[NSString stringWithFormat:@"parent = %li", (long)catalogId]];
    NSMutableArray *catalogs = [NSMutableArray new];
    for(catalogModel *cc in res) {
        catalogModel *newCC = [catalogModel new];
        newCC.catalogId = cc.catalogId;
        newCC.name = cc.name;
        newCC.code = cc.code;
        newCC.image = cc.image;
        newCC.parent = cc.parent;
        [catalogs addObject:newCC];
    }
    
    if(completed) {
        completed(catalogs);
    }
}

- (void)getProductFromDB:(NSInteger)catalogId completed:(void(^)(NSArray<goods *> *, NSArray<collectionImages *> *))completed {
    RLMResults *res = [goods objectsWhere:[NSString stringWithFormat:@"parent = %li", (long)catalogId]];
    NSMutableArray *catalogs = [NSMutableArray new];
    for(goods *cc in res) {
        goods *newCC = [goods new];
        newCC.goodsId = cc.goodsId;
        newCC.name = cc.name;
        newCC.image = cc.image;
        newCC.parent = cc.parent;
        newCC.price = cc.price;
        newCC.desc = cc.desc;
        [catalogs addObject:newCC];
    }
    
    NSMutableArray *images = [NSMutableArray new];
    res = [collectionImages objectsWhere:[NSString stringWithFormat:@"parent=%li", (long)catalogId]];
    for(collectionImages *cc in res) {
        collectionImages *newCC = [collectionImages new];
        newCC.image = cc.image;
        newCC.parent = cc.parent;
        [images addObject:newCC];
    }
    
    if(completed) {
        completed(catalogs, images);
    }
}

- (void)getGoodsForCatalog:(NSInteger)catalogId completed:(void(^)(NSArray<goods *> *, NSArray<collectionImages *> *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@products?token=%@&collection_id=%li", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"], (long)catalogId];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        
        catalogModel2 *catalog = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[catalogModel2 objectMapping]];
        
        if(catalog.status) {
            [[RLMRealm defaultRealm] beginWriteTransaction];
            RLMResults *res = [goods objectsWhere:[NSString stringWithFormat:@"parent=%li", (long)catalogId]];
            for(goods *cc in res) {
                [[RLMRealm defaultRealm] deleteObject:cc];
            }
            [[RLMRealm defaultRealm] commitWriteTransaction];
            [[RLMRealm defaultRealm] beginWriteTransaction];
            for(int i=0;i<catalog.data.items.count;i++) {
                goods *cc = catalog.data.items[i];
                cc.parent = catalogId;
                [goods createOrUpdateInRealm:[RLMRealm defaultRealm] withValue:cc];
            }
            [[RLMRealm defaultRealm] commitWriteTransaction];
            
            [[RLMRealm defaultRealm] beginWriteTransaction];
            res = [collectionImages objectsWhere:[NSString stringWithFormat:@"parent=%li", (long)catalogId]];
            for(collectionImages *cc in res) {
                [[RLMRealm defaultRealm] deleteObject:cc];
            }
            [[RLMRealm defaultRealm] commitWriteTransaction];
            
            [[RLMRealm defaultRealm] beginWriteTransaction];
            for(int i=0;i<catalog.data.collectionImages.count;i++) {
                collectionImages *cc = catalog.data.collectionImages[i];
                cc.parent = catalogId;
                [collectionImages createInRealm:[RLMRealm defaultRealm] withValue:cc];
            }
            [[RLMRealm defaultRealm] commitWriteTransaction];
        }
        
        if(completed) {
            [self getProductFromDB:catalogId completed:^(NSArray *goods, NSArray *images) {
                completed(goods, images);
            }];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        
        if(completed) {
            [self getProductFromDB:catalogId completed:^(NSArray *goods, NSArray *images) {
                completed(goods, images);
            }];
        }
        
    }];
}

@end
