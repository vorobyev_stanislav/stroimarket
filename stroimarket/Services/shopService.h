//
//  shopService.h
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "shopModel.h"

@interface shopService : NSObject

- (void)getShops:(void(^)(NSArray<shopModel *> *))completed;

@end
