//
//  notificationService.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "notificationModel.h"

@interface notificationService : NSObject

- (void)getNotifications:(void(^)(NSArray<notificationModel *> *))completed;
- (void)addNotification:(NSString *)text completed:(void(^)())completed;

@end
