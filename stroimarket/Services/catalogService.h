//
//  catalogService.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "catalogModel.h"

@interface catalogService : NSObject

- (void)getCatalogs:(void(^)(NSArray<catalogModel *> *))completed;
- (void)getGoodsForCatalog:(NSInteger)catalogId completed:(void(^)(NSArray<goods *> *, NSArray<collectionImages *> *))completed;
- (void)getSubCatalog:(NSInteger)catalogId completed:(void(^)(NSArray<catalogModel *> *))completed;
- (void)getProductFromDB:(NSInteger)catalogId completed:(void(^)(NSArray<goods *> *, NSArray<collectionImages *> *))completed;
- (void)getCatalogFromDB:(NSInteger)catalogId completed:(void(^)(NSMutableArray *))completed;

@end
