//
//  notificationService.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "notificationService.h"

@implementation notificationService

- (void)addNotification:(NSString *)text completed:(void(^)())completed {
    notificationModel *cc = [notificationModel new];
    cc.notificationId = [[NSDate date] timeIntervalSince1970];
    cc.notification = text;
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [notificationModel createOrUpdateInRealm:[RLMRealm defaultRealm] withValue:cc];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    
    if(completed) {
        completed();
    }
}

- (void)getNotifications:(void(^)(NSArray<notificationModel *> *))completed {
    
    NSMutableArray *notifications = [NSMutableArray new];
    
    RLMResults *res = [[notificationModel allObjects] sortedResultsUsingKeyPath:@"notificationId" ascending:NO];
    
    for(notificationModel *cc in res) {
        notificationModel *newCC = [notificationModel new];
        newCC.notificationId = cc.notificationId;
        newCC.notification = cc.notification;
        [notifications addObject:newCC];
    }
    
    if(completed) {
        completed(notifications);
    }
}

@end
