//
//  networkService.h
//  stroimarket
//
//  Created by apple on 27.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "authModel.h"
#import "cellModel.h"

@interface networkService : NSObject

- (void)loginWithPhone:(NSString *)phone andEmail:(NSString *)email completed:(void (^)(BOOL))completed;
- (void)validateCode:(NSString *)code withPhone:(NSString *)phone completed:(void (^)(BOOL, NSString *, profile *))completed;
- (void)registerUser:(void (^)(BOOL))completed;
- (void)getProfile:(void (^)(profile *))completed;
- (void)setProfile:(profile *)profile completed:(void(^)(BOOL))completed;
- (void)sendMessage:(NSString *)message completed:(void(^)(BOOL))completed;
- (void)getNews:(void(^)(NSArray *))completed;
- (void)getStock:(void(^)(NSArray *))completed;
- (void)getMain:(void(^)(NSArray *, NSArray *, NSArray *, NSArray *))completed;
- (void)setTokens:(void(^)())completed;
- (void)resendCode:(NSString *)phone completed:(void(^)(BOOL, NSString *))completed;

@end
