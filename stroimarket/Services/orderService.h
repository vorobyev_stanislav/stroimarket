//
//  orderService.h
//  stroimarket
//
//  Created by apple on 22.05.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "orderModel.h"

@interface orderService : NSObject

- (void)addProductToBasket:(NSInteger)productId quantity:(NSInteger)quantity completed:(void(^)(NSInteger))completed;
- (void)getBasket:(void(^)(NSArray<selectedGoods *> *))completed;
- (void)deleteProductFromBasket:(NSInteger)productId completed:(void(^)(BOOL))completed;
- (void)makeOrder:(void(^)(BOOL))completed;
- (void)getOrders:(void(^)(NSArray<order *> *))completed;

@end
