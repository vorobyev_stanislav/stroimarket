//
//  orderService.m
//  stroimarket
//
//  Created by apple on 22.05.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "orderService.h"

@implementation orderService

- (void)addProductToBasket:(NSInteger)productId quantity:(NSInteger)quantity completed:(void(^)(NSInteger))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@addtobasket?token=%@&product_id=%li&quantity=%li", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"], productId, quantity];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        /*NSMutableArray *catalogs = [NSMutableArray new];
        if([[responseObject valueForKey:@"status"] boolValue]) {
            
            [[RLMRealm defaultRealm] beginWriteTransaction];
            RLMResults *res = [catalogModel allObjects];
            for(catalogModel *cc in res) {
                [[RLMRealm defaultRealm] deleteObject:cc];
            }
            [[RLMRealm defaultRealm] commitWriteTransaction];
            
            NSDictionary *data = [responseObject valueForKey:@"data"];
            NSArray *kies = [data allKeys];
            for(int i=0;i<kies.count;i++) {
                NSDictionary *cat = [data valueForKey:kies[i]];
                [self parseCatalog:cat];
            }
            
            [self getCatalogFromDB:0 inArray:catalogs];
        }*/
        
        if(completed) {
            
            completed(1);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        
        //NSMutableArray *catalogs = [NSMutableArray new];
        //[self getCatalogFromDB:0 inArray:catalogs];
        
        if(completed) {
            completed(0);
        }
        
    }];
}

- (void)deleteProductFromBasket:(NSInteger)productId completed:(void(^)(BOOL))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@removefrombasket?token=%@&item_id=%li", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"], productId];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        //NSLog(@"%@", responseObject);
        orderModel *basket = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[orderModel objectMapping]];
        
        if(completed) {
            completed(basket.status);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        
        //NSMutableArray *catalogs = [NSMutableArray new];
        //[self getCatalogFromDB:0 inArray:catalogs];
        
        if(completed) {
            completed(NO);
        }
        
    }];
}

- (void)getBasket:(void(^)(NSArray<selectedGoods *> *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@getbasket?token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        orderModel *basket = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[orderModel objectMapping]];
        
        NSMutableArray *goods = [NSMutableArray new];
        if(basket.status) {
            goods = [basket.data mutableCopy];
        }
        
        if(completed) {
            completed(goods);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        
        //NSMutableArray *catalogs = [NSMutableArray new];
        //[self getCatalogFromDB:0 inArray:catalogs];
        
        if(completed) {
            completed([NSArray new]);
        }
        
    }];
}

- (void)makeOrder:(void(^)(BOOL))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@order?token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        orderModel *basket = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[orderModel objectMapping]];
        
        if(completed) {
            completed(basket.status);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        
        //NSMutableArray *catalogs = [NSMutableArray new];
        //[self getCatalogFromDB:0 inArray:catalogs];
        
        if(completed) {
            completed(NO);
        }
        
    }];
}

- (void)getOrders:(void(^)(NSArray<order *> *))completed {
    
    NSString *myurl = [NSString stringWithFormat:@"%@getorders?token=%@", mydomain, [[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"]];
    myurl = [myurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"get url=%@", myurl);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html", @"application/json"]];
    
    [manager GET:myurl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"%@", responseObject);
        order2Model *basket = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[order2Model objectMapping]];
        
        if(completed) {
            completed(basket.data);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"wwwww=%@", error);
        
        //NSMutableArray *catalogs = [NSMutableArray new];
        //[self getCatalogFromDB:0 inArray:catalogs];
        
        if(completed) {
            completed([NSArray new]);
        }
        
    }];
}

@end
