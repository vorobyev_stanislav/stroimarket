//
//  shopService.m
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "shopService.h"

@implementation shopService

- (void)getShops:(void(^)(NSArray<shopModel *> *))completed {
    
    NSMutableArray *shops = [NSMutableArray new];
    
    shopModel *shop = [shopModel new];
    shop.address = @"г. Ставрополь, ул. Доваторцев, 61";
    shop.phone = @"тел (8652) 55-41-28, 55-41-29";
    shop.latitude = 45.004957;
    shop.longitude = 41.925927;
    [shops addObject:shop];
    
    shop = [shopModel new];
    shop.address = @"г. Ставрополь, ул. Черниговская 20";
    shop.phone = @"тел (8652) 57-04-50";
    shop.latitude = 44.989343;
    shop.longitude = 41.914717;
    [shops addObject:shop];
    
    shop = [shopModel new];
    shop.address = @"г. Черкесск, ул. Октябрьская,309-Б";
    shop.phone = @"тел (8782) 27-31-71";
    shop.latitude = 44.2001064;
    shop.longitude = 42.0544985;
    [shops addObject:shop];
    
    shop = [shopModel new];
    shop.address = @"г. Пятигорск, ул. Ермолова, 22";
    shop.phone = @"тел (8793) 97-60-12";
    shop.latitude = 44.0461715;
    shop.longitude = 43.0132915;
    [shops addObject:shop];
    
    shop = [shopModel new];
    shop.address = @"г. Краснодар, ул. Кубанская Набережная 45";
    shop.phone = @"тел (861) 238-62-27";
    shop.latitude = 45.034146;
    shop.longitude = 38.958002;
    [shops addObject:shop];
    
    if(completed) {
        completed(shops);
    }
}

@end
