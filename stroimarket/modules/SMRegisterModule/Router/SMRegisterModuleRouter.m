//
//  SMRegisterModuleSMRegisterModuleRouter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterModuleRouter.h"
#import "SMMainModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMRegisterModuleRouter

#pragma mark - Методы SMRegisterModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openMainModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to main"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMMainModuleModuleInput> moduleInput) {
        [moduleInput configureModule];
        return nil;
    }];
}

@end
