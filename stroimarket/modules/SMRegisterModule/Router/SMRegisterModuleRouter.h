//
//  SMRegisterModuleSMRegisterModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMRegisterModuleRouter : NSObject <SMRegisterModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
