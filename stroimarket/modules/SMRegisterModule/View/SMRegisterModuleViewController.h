//
//  SMRegisterModuleSMRegisterModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMRegisterModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMRegisterModuleViewOutput;

@interface SMRegisterModuleViewController : UIViewController <SMRegisterModuleViewInput>

@property (nonatomic, strong) id<SMRegisterModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;

@end
