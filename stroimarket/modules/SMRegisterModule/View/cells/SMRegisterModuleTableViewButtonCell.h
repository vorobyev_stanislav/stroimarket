//
//  SMRegisterModuleTableViewButtonCell.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMRegisterModuleCellPresenter.h"

@interface SMRegisterModuleTableViewButtonCell : RETableViewCell <SMRegisterModuleCellInput>

@property (strong, readwrite, nonatomic) SMRegisterModuleButtonCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIButton *regButton;

@end
