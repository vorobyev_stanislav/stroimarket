//
//  SMRegisterModuleSMRegisterModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMRegisterModuleCellPresenter.h"

@interface SMRegisterModuleTableViewCell : RETableViewCell <SMRegisterModuleCellInput>

@property (strong, readwrite, nonatomic) SMRegisterModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet UITextField *textField;
//@property (weak, nonatomic) IBOutlet SHSPhoneTextField *phone;
@property (weak, nonatomic) IBOutlet UILabel *name;

- (void)setValue:(NSString *)value;

@end
