//
//  SMRegisterModuleTableViewButtonCell.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMRegisterModuleTableViewButtonCell.h"

@implementation SMRegisterModuleTableViewButtonCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 100*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

#pragma mark - Методы обработки событий от визуальных элементов

- (void)regButtonClick {
    [self.item registerUser];
}

#pragma mark - Методы SMRegisterModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    
    self.regButton = [UIFabric buttonWithText:@"ДАЛЕЕ" andTextColor:[colorScheme colorWithSchemeName:@"backgroundCellColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self];
    self.regButton.layer.cornerRadius = 2;
    [self.regButton addTarget:self action:@selector(regButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.regButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.centerY.equalTo(self);
        make.centerX.equalTo(self);
    }];
    
}

@end
