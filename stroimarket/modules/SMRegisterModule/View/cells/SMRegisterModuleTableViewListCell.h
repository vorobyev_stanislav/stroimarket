//
//  SMRegisterModuleTableViewListCell.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMRegisterModuleCellPresenter.h"

@interface SMRegisterModuleTableViewListCell : RETableViewCell <SMRegisterModuleCellInput>

@property (strong, readwrite, nonatomic) SMRegisterModuleListCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet UILabel *textField;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
