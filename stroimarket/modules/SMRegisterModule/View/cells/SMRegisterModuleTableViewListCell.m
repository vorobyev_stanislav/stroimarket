//
//  SMRegisterModuleTableViewListCell.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMRegisterModuleTableViewListCell.h"
#import <ActionSheetStringPicker.h>

@implementation SMRegisterModuleTableViewListCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 60*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.name.text = self.item.model.name;
    self.textField.text = self.item.model.value;
}

#pragma mark - Методы обработки событий от визуальных элементов

//- (void)showCalendar {
//    [self.item showCalendar:self.textField.text];
//}

- (void)citySelect:(ActionSheetStringPicker *)sender {

}

- (void)cityCancel {
    
}

- (void)showCalendar {
    
//    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:@"Выберите город" rows:@[@"Ставрополь"] initialSelection:0 target:self successAction:@selector(citySelect:) cancelAction:@selector(cityCancel) origin:self];
//    [picker setCancelButton:[[UIBarButtonItem alloc] initWithTitle:@"Отмена"  style:UIBarButtonItemStylePlain target:nil action:nil]];
//    [picker setDoneButton:[[UIBarButtonItem alloc] initWithTitle:@"Готово"  style:UIBarButtonItemStylePlain target:nil action:nil]];
//    [picker showActionSheetPicker];
    
    ActionSheetStringPicker *picker = [ActionSheetStringPicker showPickerWithTitle:@"Выберите город"
                                            rows:@[@"Ставрополь"]
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           self.textField.text = selectedValue;
                                           [MySingleton sharedMySingleton].profile.city = selectedValue;
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:self];
    [picker setCancelButton:[[UIBarButtonItem alloc] initWithTitle:@"Отмена"  style:UIBarButtonItemStylePlain target:nil action:nil]];
    [picker setDoneButton:[[UIBarButtonItem alloc] initWithTitle:@"Готово"  style:UIBarButtonItemStylePlain target:nil action:nil]];
//    [picker showActionSheetPicker];
}

- (void)citySelect {
    
}

#pragma mark - Методы SMRegisterModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    
    self.border = [UIFabric viewWithBackgroundColor:[UIColor colorWithRed:218.0f/255.0f green:218.0f/255.0f blue:218.0f/255.0f alpha:0.0f] andSuperView:self];
    
    [self.border mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(1);
        make.bottom.equalTo(self).with.offset(0);
        make.centerX.equalTo(self);
    }];
    
    self.textField = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto-Regular" andFontSize:19 andSuperView:self];
    self.textField.alpha = 0.87f;
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCalendar)];
    gesture.numberOfTapsRequired = 1;
    self.textField.userInteractionEnabled = YES;
    [self.textField addGestureRecognizer:gesture];
    
    [self.textField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.bottom.equalTo(self.border.mas_top).with.offset(0);
        make.centerX.equalTo(self);
    }];
    
    self.arrow = [UIFabric imageViewWithImageName:@"ic_arrow_drop_down" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];
    
    [self.arrow mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.textField);
        make.right.equalTo(self.textField);
    }];
    
    self.name = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:14 andSuperView:self];
    self.name.alpha = 0.54f;
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.centerX.equalTo(self);
        make.bottom.equalTo(self.textField.mas_top).with.offset(5);
    }];
}

@end
