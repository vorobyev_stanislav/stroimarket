//
//  SMRegisterModuleTableViewSocialCell.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMRegisterModuleTableViewSocialCell.h"

@implementation SMRegisterModuleTableViewSocialCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 135*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

#pragma mark - Методы обработки событий от визуальных элементов

- (void)vkClick {
    [self.item vkClick];
}

- (void)fbClick {
    [self.item fbClick];
}

#pragma mark - Методы SMRegisterModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    
    self.backView = [UIFabric viewWithBackgroundColor:[UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f] andSuperView:self];

    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.bottom.equalTo(self).with.offset(0);
        make.top.equalTo(self).with.offset(0);
        make.centerX.equalTo(self);
    }];
    
    self.header = [UIFabric labelWithText:@"Зарегистрироваться через соцсети" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto-Regular" andFontSize:16 andSuperView:self.backView];
    self.header.textAlignment = NSTextAlignmentCenter;
    
    [self.header mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.top.equalTo(self.backView).with.offset(10);
        make.centerX.equalTo(self);
    }];
    
    self.vk = [UIFabric buttonWithImage:@"ic_vk" andTarget:self andSelector:@selector(vkClick) andSuperView:self.backView];
    
    [self.vk mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(65*[Functions koefX]);
        make.height.mas_equalTo(65*[Functions koefX]);
        make.centerY.equalTo(self.backView).with.offset(20*[Functions koefX]);
        make.centerX.equalTo(self.backView).with.offset(-(65.0f/2 + 20)*[Functions koefX]);
    }];
    
    self.fb = [UIFabric buttonWithImage:@"ic_fb" andTarget:self andSelector:@selector(fbClick) andSuperView:self.backView];
    
    [self.fb mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(65*[Functions koefX]);
        make.height.mas_equalTo(65*[Functions koefX]);
        make.centerY.equalTo(self.backView).with.offset(20*[Functions koefX]);
        make.centerX.equalTo(self.backView).with.offset((65.0f/2 + 20)*[Functions koefX]);
    }];
}

@end
