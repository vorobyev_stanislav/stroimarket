//
//  SMRegisterModuleTableViewPhoneCell.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMRegisterModuleTableViewPhoneCell.h"

@implementation SMRegisterModuleTableViewPhoneCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 60*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.name.text = self.item.model.name;
    self.textField.text = [MySingleton sharedMySingleton].profile.phone;
}

#pragma mark - Методы обработки событий от визуальных элементов

- (void)hideKB {
    [self.textField resignFirstResponder];
}

- (IBAction)EnterWord:(UITextField *)sender {
    NSLog(@"type=%li text=%@", (long)self.item.model.type, sender.text);
    if(self.item.model.type == 0) {
        [MySingleton sharedMySingleton].profile.last_name = sender.text;
    } else if(self.item.model.type == 1) {
        [MySingleton sharedMySingleton].profile.first_name = sender.text;
    } else if(self.item.model.type == 2) {
        [MySingleton sharedMySingleton].profile.patr_name = sender.text;
    } else if(self.item.model.type == 4) {
        [MySingleton sharedMySingleton].profile.email = sender.text;
    } else if(self.item.model.type == 5) {
        [MySingleton sharedMySingleton].profile.phone = sender.text;
    }
}

#pragma mark - keyboard delegate

- (void)keyboardWillShow:(NSNotification *)note {
    
    NSDictionary *userInfo = note.userInfo;
    [self.item showKB:self.item.model.type andUserInfo:userInfo];
    
}

- (void)keyboardWillHide:(NSNotification *)note {
    NSDictionary *userInfo = note.userInfo;
    [self.item hideKB:userInfo];
}

#pragma mark - UITextField delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    [MySingleton sharedMySingleton].current = self.item.model.type;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (range.location == textField.text.length && [string isEqualToString:@" "]) {
        textField.text = [textField.text stringByAppendingString:@"\u00a0"];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}

#pragma mark - Методы SMRegisterModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    
    self.border = [UIFabric viewWithBackgroundColor:[UIColor colorWithRed:218.0f/255.0f green:218.0f/255.0f blue:218.0f/255.0f alpha:1.0f] andSuperView:self];
    
    [self.border mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(1);
        make.bottom.equalTo(self).with.offset(0);
        make.centerX.equalTo(self);
    }];
    
    self.textField = [UIFabric shsPhoneTextField:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto-Regular" andFontSize:19 andSuperView:self];
    self.textField.alpha = 0.87f;
    self.textField.delegate = (id)self;
    [Functions addKeyboardToolbarForTextField:self.textField width:self.frame.size.width target:self doneTitle:@"Готово" doneAction:@selector(hideKB)];
    [self.textField.formatter setDefaultOutputPattern:@"+#(###)###-##-##"];
    
    [self.textField addTarget:self action:@selector(EnterWord:) forControlEvents:UIControlEventEditingChanged];
    
    [self.textField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.bottom.equalTo(self.border.mas_top).with.offset(0);
        make.centerX.equalTo(self);
    }];
    
    self.name = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:14 andSuperView:self];
    self.name.alpha = 0.54f;
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.centerX.equalTo(self);
        make.bottom.equalTo(self.textField.mas_top).with.offset(5);
    }];
}

@end
