//
//  SMRegisterModuleTableViewPhoneCell.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMRegisterModuleCellPresenter.h"

@interface SMRegisterModuleTableViewPhoneCell : RETableViewCell <SMRegisterModuleCellInput>

@property (strong, readwrite, nonatomic) SMRegisterModulePhoneCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
