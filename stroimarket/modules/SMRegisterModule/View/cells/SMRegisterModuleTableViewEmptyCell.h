//
//  SMRegisterModuleTableViewEmptyCell.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMRegisterModuleCellPresenter.h"

@interface SMRegisterModuleTableViewEmptyCell : RETableViewCell <SMRegisterModuleCellInput>

@property (strong, readwrite, nonatomic) SMRegisterModuleEmptyCellPresenter *item;

@end
