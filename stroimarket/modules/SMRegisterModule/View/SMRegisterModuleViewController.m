//
//  SMRegisterModuleSMRegisterModuleViewController.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterModuleViewController.h"
#import "Functions.h"
#import "SMRegisterModuleTableViewCell.h"
#import "SMRegisterModuleViewOutput.h"
#import "SocialLoginViewController.h"

static NSString * const VKClientId = @"6071877";
static NSString * const FBClientId = @"431784453856923";

@implementation SMRegisterModuleViewController

SocialLoginViewController *popupController;

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Регистрация" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:nil];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [super updateViewConstraints];
}

#pragma mark - Social delegate

- (void)VKLoginFinishWithAccessToken:(NSDictionary *)params {
    
    [[NSUserDefaults standardUserDefaults] setValue:[params objectForKey:@"access_token"] forKey:@"accessToken"];
    [[NSUserDefaults standardUserDefaults] setValue:[params objectForKey:@"user_id"] forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] setValue:@"vkauth" forKey:@"SNType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"VK: access_token=%@", params);
    
    [SocialLoginViewController getVKProfile:[[params valueForKey:@"user_id"] intValue] completed:^(NSString *firstName, NSString *lastName) {
        
        [self.output setFirstName:firstName lastName:lastName];
        [popupController dismissViewControllerAnimated:YES completion:nil];
    }];
    
}

- (void)VKLoginFinishWithError:(NSDictionary *)error {
    
    NSLog(@"VK: error=%@", error);
    [popupController dismissViewControllerAnimated:YES completion:nil];
//    if (self.popupViewController != nil) {
//        [self dismissPopupViewControllerAnimated:YES completion:nil];
//    }
}

- (void)FBLoginFinishWithAccessToken:(NSDictionary *)params
{
    
    [[NSUserDefaults standardUserDefaults] setValue:[params objectForKey:@"access_token"] forKey:@"accessToken"];
    [[NSUserDefaults standardUserDefaults] setValue:@"fbauth" forKey:@"SNType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"FB: params=%@", params);
    [SocialLoginViewController getFBRequest:@"me" params:[NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] valueForKey:@"accessToken"], @"access_token", nil] success:^(NSDictionary *params){
        
        [[NSUserDefaults standardUserDefaults] setValue:[params objectForKey:@"id"] forKey:@"userId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [popupController dismissViewControllerAnimated:YES completion:nil];
//        [self SocialLogin];
        
    } failure:^(NSDictionary *params){
        
        [self FBLoginFinishWithError:params];
    }];
    
    
}

- (void)FBLoginFinishWithError:(NSDictionary *)error
{
    
    NSLog(@"FB: error=%@", error);
    [popupController dismissViewControllerAnimated:YES completion:nil];
//    if (self.popupViewController != nil) {
//        [self dismissPopupViewControllerAnimated:YES completion:nil];
//    }
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    
}

- (void)vkClick {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    popupController = [storyboard instantiateViewControllerWithIdentifier:@"SocialLoginViewController"];
    
    popupController.type = SOCIALNET_VK;
    popupController.VKClientId = VKClientId;
    popupController.VKScope = @"friends";
    popupController.SocialDelegate = (id)self;
    
    [self presentModalViewController:popupController animated:YES];
//    [self presentPopupViewController:popupController animated:YES completion:nil];
}

- (void)fbClick {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    popupController = [storyboard instantiateViewControllerWithIdentifier:@"SocialLoginViewController"];
    
    popupController.type = SOCIALNET_FB;
    popupController.FBredirectUri = @"https://stroymarket-sk.ru/auth/auth/";
    popupController.FBScope = @"public_profile";
    popupController.FBClientId = FBClientId;
    popupController.SocialDelegate = (id)self;
    
    [self presentModalViewController:popupController animated:YES];
//    [self presentPopupViewController:popupController animated:YES completion:nil];
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    UITableView *tv = [[UITableView alloc] init];
    self.tableView = tv;
    [self.view addSubview:tv];
    tv.backgroundColor = [UIColor clearColor];
    tv.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableManager = [[RETableViewManager alloc] initWithTableView:self.tableView];
    RETableViewSection *dumbSection = [RETableViewSection sectionWithHeaderTitle:@""];
    [self.tableManager addSection:dumbSection];
    self.tableManager[@"SMRegisterModuleCellPresenter"] = @"SMRegisterModuleTableViewCell";
    self.tableManager[@"SMRegisterModuleEmptyCellPresenter"] = @"SMRegisterModuleTableViewEmptyCell";
    self.tableManager[@"SMRegisterModuleDateCellPresenter"] = @"SMRegisterModuleTableViewDateCell";
    self.tableManager[@"SMRegisterModulePhoneCellPresenter"] = @"SMRegisterModuleTableViewPhoneCell";
    self.tableManager[@"SMRegisterModuleListCellPresenter"] = @"SMRegisterModuleTableViewListCell";
    self.tableManager[@"SMRegisterModuleSocialCellPresenter"] = @"SMRegisterModuleTableViewSocialCell";
    self.tableManager[@"SMRegisterModuleButtonCellPresenter"] = @"SMRegisterModuleTableViewButtonCell";
    [self.output setTableViewManager:self.tableManager];
}
#pragma mark - Методы SMRegisterModuleViewInput

- (void)hideKB:(NSDictionary *)userInfo {
    
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];

    CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];

    CGRect newFrame = self.view.frame;
    newFrame.origin.y = self.navigationController.navigationBar.frame.size.height + 20;

    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{

        self.view.frame = newFrame;
    } completion:nil];
}

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo {
    
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];
    
    //    float cellY = 20*[Functions koefX] + type*60*[Functions koefX];
    //    NSLog(@"cellY=%f kbY=%f", cellY, keyboardFrameEnd.origin.y);
    
    type = [MySingleton sharedMySingleton].current;
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:type inSection:0]];
    CGRect cellFrame = [cell convertRect:cell.frame toView:self.tableView];
    
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:type inSection:0]];
    
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    
    NSLog(@"type %li frame %f %f %f %f", (long)type, keyboardFrameEnd.size.height, keyboardFrameEnd.origin.y, cellFrame.origin.y, cellFrame.size.height);
    float yy = 0;
    yy = cellFrame.origin.y + cellFrame.size.height - keyboardFrameEnd.origin.y;
    
    //    if(self.editingField == 0) {
    //        yy = self.downView.frame.origin.y + self.whenceTF.frame.origin.y + self.whenceTF.frame.size.height - keyboardFrameEnd.origin.y;
    //    } else {
    //        yy = self.downView.frame.origin.y + self.whereTF.frame.origin.y + self.whereTF.frame.size.height - keyboardFrameEnd.origin.y;
    //    }
    //
    
    //
    CGRect newFrame = self.view.frame;
    yy = 60*[Functions koefX]*([MySingleton sharedMySingleton].current + 1) - (self.view.frame.size.height - keyboardFrameEnd.size.height) - self.navigationController.navigationBar.frame.size.height;
    
    if(yy > 0) {
//        yy = 0;
        newFrame.origin.y = -yy - 5;// + self.navigationController.navigationBar.frame.size.height;
    }
    
    NSLog(@"yy=%f", yy);
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        self.view.frame = newFrame;
    } completion:nil];
    
//    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
//
//    CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];
//
////    float cellY = 20*[Functions koefX] + type*60*[Functions koefX];
////    NSLog(@"cellY=%f kbY=%f", cellY, keyboardFrameEnd.origin.y);
//    
//    type = [MySingleton sharedMySingleton].current;
//    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:type inSection:0]];
//    CGRect cellFrame = [cell convertRect:cell.frame toView:self.tableView];
//
//    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:type inSection:0]];
//    
//    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
//    
//    NSLog(@"type %li frame %f %f %f %f", (long)type, keyboardFrameEnd.size.height, keyboardFrameEnd.origin.y, cellFrame.origin.y, cellFrame.size.height);
//    float yy = 0;
//    yy = cellFrame.origin.y + cellFrame.size.height - keyboardFrameEnd.origin.y;
//    
////    if(self.editingField == 0) {
////        yy = self.downView.frame.origin.y + self.whenceTF.frame.origin.y + self.whenceTF.frame.size.height - keyboardFrameEnd.origin.y;
////    } else {
////        yy = self.downView.frame.origin.y + self.whereTF.frame.origin.y + self.whereTF.frame.size.height - keyboardFrameEnd.origin.y;
////    }
////
//    CGRect newFrame = self.view.frame;
//    newFrame.origin.y = -yy - 5;
//
//    NSLog(@"yy=%f", yy);
//    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
//        self.view.frame = newFrame;
//    } completion:nil];
}

- (void)badPhone {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Введите правильный номер телефона!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

- (void)badEmail {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Введите правильный адрес почты!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

- (void)badData {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Введите все данные!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

- (void)registerError {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Ошибка регистрации!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
