//
//  SMRegisterModuleSMRegisterModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterModuleViewOutput.h"
#import "SMRegisterModuleInteractorOutput.h"
#import "SMRegisterModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMRegisterModuleViewInput;
@protocol SMRegisterModuleInteractorInput;
@protocol SMRegisterModuleRouterInput;

@interface SMRegisterModulePresenter : NSObject <SMRegisterModuleModuleInput, SMRegisterModuleViewOutput, SMRegisterModuleInteractorOutput>

@property (nonatomic, weak) id<SMRegisterModuleViewInput> view;
@property (nonatomic, strong) id<SMRegisterModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMRegisterModuleRouterInput> router;

@end
