//
//  SMRegisterModuleSMRegisterModuleCellPresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterModuleCellPresenter.h"

#import "Functions.h"

@implementation SMRegisterModuleEmptyCellPresenter

@end

@implementation SMRegisterModuleDateCellPresenter

- (void)showCalendar:(NSString *)date {
    [self.interactor showCalendar:date];
}

@end

@implementation SMRegisterModulePhoneCellPresenter

- (void)hideKB:(NSDictionary *)userInfo {
    [self.interactor hideKB:userInfo];
}

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo {
    [self.interactor showKB:type andUserInfo:userInfo];
}

@end

@implementation SMRegisterModuleListCellPresenter

@end

@implementation SMRegisterModuleSocialCellPresenter

- (void)vkClick {
    [self.interactor vkClick];
}

- (void)fbClick {
    [self.interactor fbClick];
}

@end

@implementation SMRegisterModuleButtonCellPresenter

- (void)registerUser {
    [self.interactor registerUser];
}

@end

@implementation SMRegisterModuleCellPresenter

- (void)hideKB:(NSDictionary *)userInfo {
    [self.interactor hideKB:userInfo];
}

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo {
    [self.interactor showKB:type andUserInfo:userInfo];
}

@end
