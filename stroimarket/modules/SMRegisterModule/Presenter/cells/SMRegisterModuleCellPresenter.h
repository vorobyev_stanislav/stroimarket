//
//  SMRegisterModuleSMRegisterModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "cellModel.h"
#import "SMRegisterModuleInteractorInput.h"

@protocol SMRegisterModuleCellInput <NSObject>

@end

@interface SMRegisterModuleEmptyCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMRegisterModuleCellInput> cell;

@end

@interface SMRegisterModuleDateCellPresenter : RETableViewItem


@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMRegisterModuleCellInput> cell;
@property (nonatomic, strong) id<SMRegisterModuleInteractorInput> interactor;

- (void)showCalendar:(NSString *)date;

@end

@interface SMRegisterModulePhoneCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMRegisterModuleCellInput> cell;
@property (nonatomic, strong) id<SMRegisterModuleInteractorInput> interactor;

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo;
- (void)hideKB:(NSDictionary *)userInfo;

@end

@interface SMRegisterModuleListCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMRegisterModuleCellInput> cell;

@end

@interface SMRegisterModuleSocialCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMRegisterModuleCellInput> cell;
@property (nonatomic, strong) id<SMRegisterModuleInteractorInput> interactor;

- (void)vkClick;
- (void)fbClick;

@end

@interface SMRegisterModuleButtonCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMRegisterModuleCellInput> cell;
@property (nonatomic, strong) id<SMRegisterModuleInteractorInput> interactor;

- (void)registerUser;

@end

@interface SMRegisterModuleCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMRegisterModuleCellInput> cell;
@property (nonatomic, strong) id<SMRegisterModuleInteractorInput> interactor;

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo;
- (void)hideKB:(NSDictionary *)userInfo;

@end
