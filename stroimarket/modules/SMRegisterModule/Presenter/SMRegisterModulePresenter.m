//
//  SMRegisterModuleSMRegisterModulePresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterModulePresenter.h"

#import "SMRegisterModuleViewInput.h"
#import "SMRegisterModuleInteractorInput.h"
#import "SMRegisterModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMRegisterModulePresenter

#pragma mark - Методы SMRegisterModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMRegisterModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMRegisterModuleViewOutput

- (void)setFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    [self.interactor setFirstName:firstName lastName:lastName];
}

- (void) viewWillApear {
    [self.interactor makeCells];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMRegisterModuleInteractorOutput

- (void)vkClick {
    [self.view vkClick];
}

- (void)fbClick {
    [self.view fbClick];
}

- (void)badEmail {
    [self.view badEmail];
}

- (void)badData {
    [self.view badData];
}

- (void)badPhone {
    [self.view badPhone];
}

- (void)registerError {
    [self.view registerError];
}

- (void)registerSuccess {
    [self.router openMainModuleWithParams:@{}];
}

- (void)hideKB:(NSDictionary *)userInfo {
    [self.view hideKB:userInfo];
}

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo {
    [self.view showKB:type andUserInfo:userInfo];
}

- (void)registerUser {
    [self.router openMainModuleWithParams:@{}];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
