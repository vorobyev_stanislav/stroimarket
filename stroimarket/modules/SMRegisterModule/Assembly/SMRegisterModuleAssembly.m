//
//  SMRegisterModuleSMRegisterModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterModuleAssembly.h"

#import "SMRegisterModuleViewController.h"
#import "SMRegisterModuleInteractor.h"
#import "SMRegisterModulePresenter.h"
#import "SMRegisterModuleRouter.h"
#import "networkService.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMRegisterModuleAssembly

- (SMRegisterModuleViewController *)viewSMRegisterModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMRegisterModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMRegisterModuleModule]];
                          }];
}

- (SMRegisterModuleInteractor *)interactorSMRegisterModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMRegisterModuleModule]];
                              [definition injectProperty:@selector(networkService)
                                                    with:[self networkServiceSMRegisterModule]];
                          }];
}

- (SMRegisterModulePresenter *)presenterSMRegisterModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMRegisterModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMRegisterModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMRegisterModuleModule]];

                          }];
}

- (SMRegisterModuleRouter *)routerSMRegisterModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMRegisterModuleModule]];
                          }];
}

- (networkService *)networkServiceSMRegisterModule {
    return [TyphoonDefinition withClass:[networkService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
