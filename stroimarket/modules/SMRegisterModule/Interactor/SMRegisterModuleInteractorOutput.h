//
//  SMRegisterModuleSMRegisterModuleInteractorOutput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMRegisterModuleInteractorOutput <NSObject>

- (void)progressShow;
- (void)progressDismiss;
- (void)registerUser;
- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo;
- (void)hideKB:(NSDictionary *)userInfo;
- (void)badData;
- (void)badEmail;
- (void)badPhone;
- (void)registerSuccess;
- (void)registerError;
- (void)vkClick;
- (void)fbClick;

@end
