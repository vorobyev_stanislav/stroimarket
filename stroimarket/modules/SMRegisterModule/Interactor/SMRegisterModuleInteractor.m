//
//  SMRegisterModuleSMRegisterModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterModuleInteractor.h"
#import "SMRegisterModuleTableViewCell.h"
#import "SMRegisterModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMRegisterModuleCellPresenter.h"
#import "SMRegisterModuleTableViewPhoneCell.h"

@interface SMRegisterModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMRegisterModuleInteractor

#pragma mark - Методы SMRegisterModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)makeCells {
    [self.manager.sections[0] removeAllItems];
    
    SMRegisterModuleEmptyCellPresenter *pres1 = [SMRegisterModuleEmptyCellPresenter item];
    pres1.selectionHandler = ^(SMRegisterModuleEmptyCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres1];
    
    SMRegisterModuleCellPresenter *presFam = [SMRegisterModuleCellPresenter item];
    cellModel *model = [cellModel new];
    presFam.interactor = (id)self;
    model.name = @"Фамилия";
    model.type = 0;
//    model.value = profile.last_name;
    presFam.model = model;
    presFam.selectionHandler = ^(SMRegisterModuleCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)presFam];
    
    SMRegisterModuleCellPresenter *pres2 = [SMRegisterModuleCellPresenter item];
    model = [cellModel new];
    pres2.interactor = (id)self;
    model.name = @"Имя";
    model.type = 1;
//    model.value = profile.first_name;
    pres2.model = model;
    pres2.selectionHandler = ^(SMRegisterModuleCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMRegisterModuleCellPresenter item];
    model = [cellModel new];
    pres2.interactor = (id)self;
    model.name = @"Отчество";
    model.type = 2;
//    model.value = profile.patr_name;
    pres2.model = model;
    pres2.selectionHandler = ^(SMRegisterModuleCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    SMRegisterModuleDateCellPresenter *pres3 = [SMRegisterModuleDateCellPresenter item];
    model = [cellModel new];
    model.name = @"Дата рождения";
    model.type = 3;
//    model.value = @"ddddddd";
    pres3.model = model;
    pres3.interactor = (id)self;
    pres3.selectionHandler = ^(SMRegisterModuleDateCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres3];
    
    pres2 = [SMRegisterModuleCellPresenter item];
    model = [cellModel new];
    pres2.interactor = (id)self;
    model.name = @"E-mail";
    model.type = 4;
//    model.value = profile.email;
    pres2.model = model;
    pres2.selectionHandler = ^(SMRegisterModuleCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    SMRegisterModulePhoneCellPresenter *pres4 = [SMRegisterModulePhoneCellPresenter item];
    model = [cellModel new];
    pres4.interactor = (id)self;
    model.name = @"Мобильный телефон";
    model.type = 5;
//    model.value = profile.phone;
    pres4.model = model;
    pres4.selectionHandler = ^(SMRegisterModulePhoneCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres4];
    
    SMRegisterModuleListCellPresenter *pres5 = [SMRegisterModuleListCellPresenter item];
    model = [cellModel new];
    model.name = @"Выберите город";
    model.type = 6;
    pres5.model = model;
    pres5.selectionHandler = ^(SMRegisterModulePhoneCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres5];
    
    pres1 = [SMRegisterModuleEmptyCellPresenter item];
    pres1.selectionHandler = ^(SMRegisterModuleEmptyCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres1];
    
    SMRegisterModuleSocialCellPresenter *pres6 = [SMRegisterModuleSocialCellPresenter item];
    pres6.interactor = (id)self;
    pres6.selectionHandler = ^(SMRegisterModuleSocialCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres6];
    
    SMRegisterModuleButtonCellPresenter *pres7 = [SMRegisterModuleButtonCellPresenter item];
    pres7.interactor = (id)self;
    pres7.selectionHandler = ^(SMRegisterModuleSocialCellPresenter *newValue){
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres7];
    
    [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
}

- (void)registerUser {
    
    SMRegisterModuleTableViewPhoneCell *cell = [self.manager.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
    [MySingleton sharedMySingleton].profile.phone = cell.textField.phoneNumber;
    NSLog(@"city %@", [MySingleton sharedMySingleton].profile.city);
    NSLog(@"last %@", [MySingleton sharedMySingleton].profile.last_name);
    NSLog(@"first %@", [MySingleton sharedMySingleton].profile.first_name);
    NSLog(@"patr %@", [MySingleton sharedMySingleton].profile.patr_name);
    NSLog(@"birthday %@", [MySingleton sharedMySingleton].profile.birthday);
    NSLog(@"email %@", [MySingleton sharedMySingleton].profile.email);
    NSLog(@"phone %@", [MySingleton sharedMySingleton].profile.phone);
    if(![[MySingleton sharedMySingleton].profile.city length] || ![[MySingleton sharedMySingleton].profile.last_name length] || ![[MySingleton sharedMySingleton].profile.first_name length] || ![[MySingleton sharedMySingleton].profile.patr_name length] || ![[MySingleton sharedMySingleton].profile.birthday length] || ![[MySingleton sharedMySingleton].profile.email length] || ![[MySingleton sharedMySingleton].profile.phone length]) {
        [self.output badData];
    } else {
        if([[MySingleton sharedMySingleton].profile.phone length] != 11) {
            [self.output badPhone];
            return;
        }
        if(![Functions NSStringIsValidEmail:[MySingleton sharedMySingleton].profile.email]) {
            [self.output badEmail];
            return;
        }
        
        [self.networkService registerUser:^(BOOL status) {
            if(status) {
                [self.output registerSuccess];
            } else {
                [self.output registerError];
            }
        }];
    }
}

- (void)showCalendar:(NSString *)date {
    
}

- (void)hideKB:(NSDictionary *)userInfo {
    [self.output hideKB:userInfo];
}

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo {
    NSLog(@"- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo %li", type);
    [self.output showKB:type andUserInfo:userInfo];
}

- (void)vkClick {
    [self.output vkClick];
}

- (void)fbClick {
    [self.output fbClick];
}

- (void)setFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    
    NSLog(@"f=%@ l=%@", firstName, lastName);
    
//    profile *model = [profile new];
//    model.first_name = firstName;
//    model.last_name = lastName;
    [MySingleton sharedMySingleton].profile.first_name = firstName;
    [MySingleton sharedMySingleton].profile.last_name = lastName;
    [self makeCells];
    
//    SMRegisterModuleTableViewCell *cell = [self.manager.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//    cell.textField.text = lastName;
//    [cell setValue:lastName];
//    
//    cell = [self.manager.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
//    cell.textField.text = firstName;
//    [cell setValue:firstName];
}

@end
