//
//  SMRegisterModuleSMRegisterModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterModuleInteractorInput.h"
#import "networkService.h"

@protocol SMRegisterModuleInteractorOutput;

@interface SMRegisterModuleInteractor : NSObject <SMRegisterModuleInteractorInput>

@property (nonatomic, weak) id<SMRegisterModuleInteractorOutput> output;
@property (nonatomic, strong) networkService *networkService;

@end
