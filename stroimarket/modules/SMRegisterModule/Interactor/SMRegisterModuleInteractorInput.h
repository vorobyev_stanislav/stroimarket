//
//  SMRegisterModuleSMRegisterModuleInteractorInput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;
@protocol SMRegisterModuleInteractorInput <NSObject>

- (void)setTableViewManager:(RETableViewManager*)manager;
- (void)makeCells;
- (void)showCalendar:(NSString *)date;
- (void)registerUser;
- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo;
- (void)hideKB:(NSDictionary *)userInfo;
- (void)vkClick;
- (void)fbClick;
- (void)setFirstName:(NSString *)firstName lastName:(NSString *)lastName;

@end
