//
//  SMRegisterPhoneModuleSMRegisterPhoneModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMRegisterPhoneModuleModel.h"

@protocol SMRegisterPhoneModuleCellInput <NSObject>

@end

@interface SMRegisterPhoneModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMRegisterPhoneModuleModel *model;
@property (nonatomic, strong) id<SMRegisterPhoneModuleCellInput> cell;
@end
