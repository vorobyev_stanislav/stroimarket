//
//  SMRegisterPhoneModuleSMRegisterPhoneModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterPhoneModuleViewOutput.h"
#import "SMRegisterPhoneModuleInteractorOutput.h"
#import "SMRegisterPhoneModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMRegisterPhoneModuleViewInput;
@protocol SMRegisterPhoneModuleInteractorInput;
@protocol SMRegisterPhoneModuleRouterInput;

@interface SMRegisterPhoneModulePresenter : NSObject <SMRegisterPhoneModuleModuleInput, SMRegisterPhoneModuleViewOutput, SMRegisterPhoneModuleInteractorOutput>

@property (nonatomic, weak) id<SMRegisterPhoneModuleViewInput> view;
@property (nonatomic, strong) id<SMRegisterPhoneModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMRegisterPhoneModuleRouterInput> router;

@end
