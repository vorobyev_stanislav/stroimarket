//
//  SMRegisterPhoneModuleSMRegisterPhoneModulePresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterPhoneModulePresenter.h"

#import "SMRegisterPhoneModuleViewInput.h"
#import "SMRegisterPhoneModuleInteractorInput.h"
#import "SMRegisterPhoneModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMRegisterPhoneModulePresenter

#pragma mark - Методы SMRegisterPhoneModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMRegisterPhoneModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMRegisterPhoneModuleViewOutput

- (void)openEnterCode:(NSString *)phone {
    [self.router openRegisterCodeModuleWithParams:@{@"phone" : phone}];
}

- (void) viewWillApear {
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMRegisterPhoneModuleInteractorOutput
-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
