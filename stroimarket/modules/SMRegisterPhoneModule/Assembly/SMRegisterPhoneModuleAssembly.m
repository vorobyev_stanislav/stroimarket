//
//  SMRegisterPhoneModuleSMRegisterPhoneModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterPhoneModuleAssembly.h"

#import "SMRegisterPhoneModuleViewController.h"
#import "SMRegisterPhoneModuleInteractor.h"
#import "SMRegisterPhoneModulePresenter.h"
#import "SMRegisterPhoneModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMRegisterPhoneModuleAssembly

- (SMRegisterPhoneModuleViewController *)viewSMRegisterPhoneModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterPhoneModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMRegisterPhoneModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMRegisterPhoneModuleModule]];
                          }];
}

- (SMRegisterPhoneModuleInteractor *)interactorSMRegisterPhoneModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterPhoneModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMRegisterPhoneModuleModule]];
                          }];
}

- (SMRegisterPhoneModulePresenter *)presenterSMRegisterPhoneModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterPhoneModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMRegisterPhoneModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMRegisterPhoneModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMRegisterPhoneModuleModule]];

                          }];
}

- (SMRegisterPhoneModuleRouter *)routerSMRegisterPhoneModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterPhoneModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMRegisterPhoneModuleModule]];
                          }];
}

@end
