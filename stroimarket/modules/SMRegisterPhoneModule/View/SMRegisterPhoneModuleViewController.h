//
//  SMRegisterPhoneModuleSMRegisterPhoneModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMRegisterPhoneModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMRegisterPhoneModuleViewOutput;

@interface SMRegisterPhoneModuleViewController : UIViewController <SMRegisterPhoneModuleViewInput>

@property (nonatomic, strong) id<SMRegisterPhoneModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *phone;
@property (weak, nonatomic) IBOutlet UIButton *send;

@end
