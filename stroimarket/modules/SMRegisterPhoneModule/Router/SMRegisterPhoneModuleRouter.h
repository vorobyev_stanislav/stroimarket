//
//  SMRegisterPhoneModuleSMRegisterPhoneModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterPhoneModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMRegisterPhoneModuleRouter : NSObject <SMRegisterPhoneModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
