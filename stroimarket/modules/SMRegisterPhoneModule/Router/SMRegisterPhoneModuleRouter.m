//
//  SMRegisterPhoneModuleSMRegisterPhoneModuleRouter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterPhoneModuleRouter.h"
#import "SMRegisterCodeModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMRegisterPhoneModuleRouter

#pragma mark - Методы SMRegisterPhoneModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openRegisterCodeModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to code"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMRegisterCodeModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

@end
