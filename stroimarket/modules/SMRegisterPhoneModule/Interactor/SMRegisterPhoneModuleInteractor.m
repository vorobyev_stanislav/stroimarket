//
//  SMRegisterPhoneModuleSMRegisterPhoneModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterPhoneModuleInteractor.h"

#import "SMRegisterPhoneModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMRegisterPhoneModuleCellPresenter.h"

@interface SMRegisterPhoneModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMRegisterPhoneModuleInteractor

#pragma mark - Методы SMRegisterPhoneModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}
@end
