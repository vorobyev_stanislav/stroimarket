//
//  SMRegisterPhoneModuleSMRegisterPhoneModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterPhoneModuleInteractorInput.h"


@protocol SMRegisterPhoneModuleInteractorOutput;

@interface SMRegisterPhoneModuleInteractor : NSObject <SMRegisterPhoneModuleInteractorInput>

@property (nonatomic, weak) id<SMRegisterPhoneModuleInteractorOutput> output;

@end
