//
//  SMEnterErrorModuleSMEnterErrorModuleRouter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterErrorModuleRouter.h"
#import "SMRegisterModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMEnterErrorModuleRouter

#pragma mark - Методы SMEnterErrorModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openRegisterPhoneModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to register module"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMRegisterModuleModuleInput> moduleInput) {
        [moduleInput configureModule];
        return nil;
    }];
}

@end
