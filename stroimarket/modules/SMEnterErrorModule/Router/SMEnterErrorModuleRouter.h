//
//  SMEnterErrorModuleSMEnterErrorModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterErrorModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMEnterErrorModuleRouter : NSObject <SMEnterErrorModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
