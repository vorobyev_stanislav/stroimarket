//
//  SMEnterErrorModuleSMEnterErrorModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterErrorModuleAssembly.h"

#import "SMEnterErrorModuleViewController.h"
#import "SMEnterErrorModuleInteractor.h"
#import "SMEnterErrorModulePresenter.h"
#import "SMEnterErrorModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMEnterErrorModuleAssembly

- (SMEnterErrorModuleViewController *)viewSMEnterErrorModuleModule {
    return [TyphoonDefinition withClass:[SMEnterErrorModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMEnterErrorModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMEnterErrorModuleModule]];
                          }];
}

- (SMEnterErrorModuleInteractor *)interactorSMEnterErrorModuleModule {
    return [TyphoonDefinition withClass:[SMEnterErrorModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMEnterErrorModuleModule]];
                          }];
}

- (SMEnterErrorModulePresenter *)presenterSMEnterErrorModuleModule {
    return [TyphoonDefinition withClass:[SMEnterErrorModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMEnterErrorModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMEnterErrorModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMEnterErrorModuleModule]];

                          }];
}

- (SMEnterErrorModuleRouter *)routerSMEnterErrorModuleModule {
    return [TyphoonDefinition withClass:[SMEnterErrorModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMEnterErrorModuleModule]];
                          }];
}

@end
