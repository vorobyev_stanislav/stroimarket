//
//  SMEnterErrorModuleSMEnterErrorModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMEnterErrorModuleModel.h"

@protocol SMEnterErrorModuleCellInput <NSObject>

@end

@interface SMEnterErrorModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMEnterErrorModuleModel *model;
@property (nonatomic, strong) id<SMEnterErrorModuleCellInput> cell;
@end
