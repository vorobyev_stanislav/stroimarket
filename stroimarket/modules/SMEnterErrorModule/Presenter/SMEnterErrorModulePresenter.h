//
//  SMEnterErrorModuleSMEnterErrorModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterErrorModuleViewOutput.h"
#import "SMEnterErrorModuleInteractorOutput.h"
#import "SMEnterErrorModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMEnterErrorModuleViewInput;
@protocol SMEnterErrorModuleInteractorInput;
@protocol SMEnterErrorModuleRouterInput;

@interface SMEnterErrorModulePresenter : NSObject <SMEnterErrorModuleModuleInput, SMEnterErrorModuleViewOutput, SMEnterErrorModuleInteractorOutput>

@property (nonatomic, weak) id<SMEnterErrorModuleViewInput> view;
@property (nonatomic, strong) id<SMEnterErrorModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMEnterErrorModuleRouterInput> router;

@end
