//
//  SMEnterErrorModuleSMEnterErrorModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMEnterErrorModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMEnterErrorModuleViewOutput;

@interface SMEnterErrorModuleViewController : UIViewController <SMEnterErrorModuleViewInput>

@property (nonatomic, strong) id<SMEnterErrorModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIImageView *warning;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UIButton *regButton;

@end
