//
//  SMEnterErrorModuleSMEnterErrorModuleViewController.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterErrorModuleViewController.h"
#import "Functions.h"
#import "SMEnterErrorModuleTableViewCell.h"
#import "SMEnterErrorModuleViewOutput.h"

@implementation SMEnterErrorModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Вход" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:nil];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {

    [self.warning mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(105*[Functions koefX]);
        make.height.mas_equalTo(105*[Functions koefX]);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).with.offset(30*[Functions koefX]);
    }];
    
    [self.label1 mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.warning.mas_bottom).with.offset(20*[Functions koefX]);
    }];
    
    [self.label2 mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.label1.mas_bottom).with.offset(20*[Functions koefX]);
    }];
    
    [self.regButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.top.equalTo(self.label2.mas_bottom).with.offset(45*[Functions koefX]);
        make.centerX.equalTo(self.view);
    }];
    
    [super updateViewConstraints];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    
}

- (void)regButtonClick {
    [self.output openRegister];
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.warning = [UIFabric imageViewWithImageName:@"ic_warning" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self.view];
    
    self.label1 = [UIFabric labelWithText:@"Вы являетесь незарегистрированным пользователем." andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.view];
    self.label1.alpha = 0.87f;
    self.label1.numberOfLines = 0;
    self.label1.textAlignment = NSTextAlignmentCenter;
    
    self.label2 = [UIFabric labelWithText:@"Пожалуйста зарегистрируйтесь и совершайте бронирование товаров." andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:19 andSuperView:self.view];
    self.label2.alpha = 0.87f;
    self.label2.numberOfLines = 0;
    self.label2.textAlignment = NSTextAlignmentCenter;
    
    self.regButton = [UIFabric buttonWithText:@"РЕГИСТРАЦИЯ" andTextColor:[colorScheme colorWithSchemeName:@"backgroundCellColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    self.regButton.layer.cornerRadius = 2;
    [self.regButton addTarget:self action:@selector(regButtonClick) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - Методы SMEnterErrorModuleViewInput

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
