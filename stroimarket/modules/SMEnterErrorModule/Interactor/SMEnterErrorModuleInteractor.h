//
//  SMEnterErrorModuleSMEnterErrorModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterErrorModuleInteractorInput.h"


@protocol SMEnterErrorModuleInteractorOutput;

@interface SMEnterErrorModuleInteractor : NSObject <SMEnterErrorModuleInteractorInput>

@property (nonatomic, weak) id<SMEnterErrorModuleInteractorOutput> output;

@end
