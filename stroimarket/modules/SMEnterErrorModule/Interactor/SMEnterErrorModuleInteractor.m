//
//  SMEnterErrorModuleSMEnterErrorModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterErrorModuleInteractor.h"

#import "SMEnterErrorModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMEnterErrorModuleCellPresenter.h"

@interface SMEnterErrorModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMEnterErrorModuleInteractor

#pragma mark - Методы SMEnterErrorModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}
@end
