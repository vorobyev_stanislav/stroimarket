//
//  SMProfileModuleSMProfileModuleInteractorOutput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMProfileModuleInteractorOutput <NSObject>

- (void)progressShow;
- (void)progressDismiss;
- (void)editEnable;
- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo;
- (void)hideKB:(NSDictionary *)userInfo;

@end
