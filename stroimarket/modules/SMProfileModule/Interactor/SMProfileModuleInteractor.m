//
//  SMProfileModuleSMProfileModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMProfileModuleInteractor.h"

#import "SMProfileModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMProfileModuleCellPresenter.h"
#import "SMProfileModuleTableViewTextCell.h"
#import "SMProfileModuleTableViewDateCell.h"

@interface SMProfileModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMProfileModuleInteractor

#pragma mark - Методы SMProfileModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)makeProfile {
    [self.output progressShow];
    
    if([MySingleton sharedMySingleton].edit) {
        [self.output editEnable];
    }
    [self.networkService getProfile:^(profile *profile) {
        [self.manager.sections[0] removeAllItems];
        
        SMProfileModuleCellPresenter *pres1 = [SMProfileModuleCellPresenter item];
        pres1.interactor = (id)self;
        pres1.selectionHandler = ^(SMProfileModuleCellPresenter *newValue){
            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres1];
        
        //    SMProfileModuleEmptyCellPresenter *pres = [SMProfileModuleEmptyCellPresenter item];
        //    [self.manager.sections[0] addItem:(id)pres];
        
        SMProfileModuleTextCellPresenter *pres2 = [SMProfileModuleTextCellPresenter item];
        cellModel *model = [cellModel new];
        model.name = @"Фамилия";
        model.value = profile.last_name;
        model.type = 0;
        pres2.model = model;
        pres2.interactor = (id)self;
        pres2.selectionHandler = ^(SMProfileModuleTextCellPresenter *newValue){
            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres2];
        
        pres2 = [SMProfileModuleTextCellPresenter item];
        model = [cellModel new];
        model.name = @"Имя";
        model.value = profile.first_name;
        model.type = 1;
        pres2.model = model;
        pres2.interactor = (id)self;
        pres2.selectionHandler = ^(SMProfileModuleTextCellPresenter *newValue){
            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres2];
        
        pres2 = [SMProfileModuleTextCellPresenter item];
        model = [cellModel new];
        model.name = @"Отчество";
        model.value = profile.patr_name;
        model.type = 2;
        pres2.model = model;
        pres2.interactor = (id)self;
        pres2.selectionHandler = ^(SMProfileModuleTextCellPresenter *newValue){
            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres2];
        
        SMProfileModuleDateCellPresenter *pres3 = [SMProfileModuleDateCellPresenter item];
        model = [cellModel new];
        model.name = @"Дата рождения";
        model.value = profile.birthday;
        model.type = 3;
        pres3.model = model;
        pres3.interactor = (id)self;
        pres3.selectionHandler = ^(SMProfileModuleDateCellPresenter *newValue){
            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres3];
        
        pres2 = [SMProfileModuleTextCellPresenter item];
        model = [cellModel new];
        model.name = @"E-mail";
        model.value = profile.email;
        model.type = 4;
        pres2.model = model;
        pres2.interactor = (id)self;
        pres2.selectionHandler = ^(SMProfileModuleTextCellPresenter *newValue){
            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres2];
        
        SMProfileModulePhoneCellPresenter *pres4 = [SMProfileModulePhoneCellPresenter item];
        model = [cellModel new];
        model.name = @"Мобильный телефон";
        model.value = profile.phone;
        model.type = 5;
        pres4.model = model;
        pres4.selectionHandler = ^(SMProfileModulePhoneCellPresenter *newValue){
            [newValue deselectRowAnimated:YES];
        };
        //[self.manager.sections[0] addItem:(id)pres4];
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
        [self.output progressDismiss];
    }];
}

- (void)saveProfile {
    profile *prof = [profile new];
    SMProfileModuleTableViewTextCell *cellText = [self.manager.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    prof.last_name = cellText.textField.text;
    
    cellText = [self.manager.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    prof.first_name = cellText.textField.text;
    
    cellText = [self.manager.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    prof.patr_name = cellText.textField.text;
    
    cellText = [self.manager.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    prof.email = cellText.textField.text;
    
    SMProfileModuleTableViewDateCell *cellDate = [self.manager.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"Y-m-d"];
    NSDate *dd = [df dateFromString:cellDate.textField.text];
    [df setDateFormat:@"dd.mm.Y"];
    prof.birthday = [df stringFromDate:dd];//cellDate.textField.text;
    
    [self.networkService setProfile:prof completed:^(BOOL status) {
        if(status) {
            [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Профиль сохранен!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
        }
    }];
}

- (void)hideKB:(NSDictionary *)userInfo {
    [self.output hideKB:userInfo];
}

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo {
    [self.output showKB:type andUserInfo:userInfo];
}

@end
