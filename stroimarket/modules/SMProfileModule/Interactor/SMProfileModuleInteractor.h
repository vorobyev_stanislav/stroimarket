//
//  SMProfileModuleSMProfileModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMProfileModuleInteractorInput.h"
#import "networkService.h"

@protocol SMProfileModuleInteractorOutput;

@interface SMProfileModuleInteractor : NSObject <SMProfileModuleInteractorInput>

@property (nonatomic, weak) id<SMProfileModuleInteractorOutput> output;
@property (strong, nonatomic) networkService *networkService;

@end
