//
//  SMProfileModuleSMProfileModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMProfileModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMProfileModuleRouter : NSObject <SMProfileModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
