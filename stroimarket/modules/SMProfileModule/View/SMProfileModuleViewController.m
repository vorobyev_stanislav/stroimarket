//
//  SMProfileModuleSMProfileModuleViewController.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMProfileModuleViewController.h"
#import "Functions.h"
#import "SMProfileModuleTableViewCell.h"
#import "SMProfileModuleViewOutput.h"

@implementation SMProfileModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    
//    [self.sendButton mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo([UIScreen mainScreen].bounds.size.width);
//        make.height.mas_equalTo(40*[Functions koefX]);
//        make.bottom.equalTo(self.view).with.offset(40*[Functions koefX]);
//        make.centerX.equalTo(self.view);
//    }];
//    
//    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
////        make.edges.equalTo(self.view);
//        make.left.equalTo(self.view).with.offset(0);
//        make.right.equalTo(self.view).with.offset(0);
//        make.bottom.equalTo(self.view).with.offset(0);
//        make.top.equalTo(self.view).with.offset(-64);
//    }];
    self.tableView.frame = CGRectMake(0, -self.navigationController.navigationBar.frame.size.height - 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    self.sendButton.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 60*[Functions koefX]);
    
    [super updateViewConstraints];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (IBAction)action:(id)sender {
    [[SlideNavigationController sharedInstance] openMenu:1 withCompletion:nil];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

- (void)saveProfile {
    [self.output saveProfile];
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    UITableView *tv = [[UITableView alloc] init];
    self.tableView = tv;
    [self.view addSubview:tv];
    tv.backgroundColor = [UIColor whiteColor];
    tv.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableManager = [[RETableViewManager alloc] initWithTableView:self.tableView];
    RETableViewSection *dumbSection = [RETableViewSection sectionWithHeaderTitle:@""];
    [self.tableManager addSection:dumbSection];
    self.tableManager[@"SMProfileModuleCellPresenter"] = @"SMProfileModuleTableViewCell";
    self.tableManager[@"SMProfileModuleTextCellPresenter"] = @"SMProfileModuleTableViewTextCell";
    self.tableManager[@"SMProfileModuleDateCellPresenter"] = @"SMProfileModuleTableViewDateCell";
    self.tableManager[@"SMProfileModulePhoneCellPresenter"] = @"SMProfileModuleTableViewPhoneCell";
    self.tableManager[@"SMProfileModuleEmptyCellPresenter"] = @"SMProfileModuleTableViewEmptyCell";
    [self.output setTableViewManager:self.tableManager];
    
    self.sendButton = [UIFabric buttonWithText:@"СОХРАНИТЬ" andTextColor:[colorScheme colorWithSchemeName:@"backgroundCellColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    self.sendButton.layer.cornerRadius = 2;
    [self.sendButton addTarget:self action:@selector(saveProfile) forControlEvents:UIControlEventTouchUpInside];
    
}
#pragma mark - Методы SMProfileModuleViewInput

- (void)hideKB:(NSDictionary *)userInfo {
    
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];
    
    CGRect newFrame = self.view.frame;
    newFrame.origin.y = 0;//self.navigationController.navigationBar.frame.size.height + 20;
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        
        self.view.frame = newFrame;
    } completion:nil];
}

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo {
    
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];
    
    //    float cellY = 20*[Functions koefX] + type*60*[Functions koefX];
    //    NSLog(@"cellY=%f kbY=%f", cellY, keyboardFrameEnd.origin.y);
    
    type = [MySingleton sharedMySingleton].current;
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:type inSection:0]];
    CGRect cellFrame = [cell convertRect:cell.frame toView:self.tableView];
    
    CGRect rectInTableView = [self.tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:type inSection:0]];
    
    CGRect rectInSuperview = [self.tableView convertRect:rectInTableView toView:[self.tableView superview]];
    
    NSLog(@"type %li frame %f %f %f %f", (long)type, keyboardFrameEnd.size.height, keyboardFrameEnd.origin.y, cellFrame.origin.y, cellFrame.size.height);
    float yy = 0;
    yy = cellFrame.origin.y + cellFrame.size.height - keyboardFrameEnd.origin.y;
    
    //    if(self.editingField == 0) {
    //        yy = self.downView.frame.origin.y + self.whenceTF.frame.origin.y + self.whenceTF.frame.size.height - keyboardFrameEnd.origin.y;
    //    } else {
    //        yy = self.downView.frame.origin.y + self.whereTF.frame.origin.y + self.whereTF.frame.size.height - keyboardFrameEnd.origin.y;
    //    }
    //
    
    //
    CGRect newFrame = self.view.frame;
    yy = 175*[Functions koefX] + 24*[Functions koefX] + 60*[Functions koefX]*([MySingleton sharedMySingleton].current + 1) - (self.view.frame.size.height - keyboardFrameEnd.size.height);
    
    if(yy < 0) {
        yy = 0;
    }
    newFrame.origin.y = -yy;// - 5 + self.navigationController.navigationBar.frame.size.height;
    
    NSLog(@"yy=%f", yy);
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        self.view.frame = newFrame;
    } completion:nil];
}

- (void)editEnable {
    
    if(self.tableView.frame.size.height >= self.view.frame.size.height) {
        CGRect newFrame = self.sendButton.frame;
        newFrame.origin.y = newFrame.origin.y - 60*[Functions koefX];
        CGRect newFrame2 = self.tableView.frame;
        newFrame2.size.height = newFrame2.size.height - 60*[Functions koefX];
        [UIView animateWithDuration:0.3 animations:^{
            self.sendButton.frame = newFrame;
            self.tableView.frame = newFrame2;
        }];
    }
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
