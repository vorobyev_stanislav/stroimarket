//
//  SMProfileModuleTableViewEmptyCell.m
//  stroimarket
//
//  Created by apple on 09.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMProfileModuleTableViewEmptyCell.h"

@implementation SMProfileModuleTableViewEmptyCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 20*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMProfileModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
}

@end
