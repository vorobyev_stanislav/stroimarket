//
//  SMProfileModuleSMProfileModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMProfileModuleCellPresenter.h"

@interface SMProfileModuleTableViewCell : RETableViewCell <SMProfileModuleCellInput>

@property (strong, readwrite, nonatomic) SMProfileModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UIImageView *edit;
@property (weak, nonatomic) IBOutlet UIImageView *bonuses;
@property (weak, nonatomic) IBOutlet UILabel *count;

@end
