//
//  SMProfileModuleTableViewPhoneCell.m
//  stroimarket
//
//  Created by apple on 09.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMProfileModuleTableViewPhoneCell.h"

@implementation SMProfileModuleTableViewPhoneCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 60*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.name.text = self.item.model.name;
    self.textField.text = self.item.model.value;
    
    self.textField.enabled = [MySingleton sharedMySingleton].edit;
}

#pragma mark - Методы обработки событий от визуальных элементов

- (void)hideKB {
    [self.textField resignFirstResponder];
}

#pragma mark - Методы SMRegisterModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    
    self.border = [UIFabric viewWithBackgroundColor:[UIColor colorWithRed:218.0f/255.0f green:218.0f/255.0f blue:218.0f/255.0f alpha:1.0f] andSuperView:self];
    
    [self.border mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(1);
        make.bottom.equalTo(self).with.offset(0);
        make.centerX.equalTo(self);
    }];
    
    self.textField = [UIFabric shsPhoneTextField:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto-Regular" andFontSize:19 andSuperView:self];
    self.textField.alpha = 0.87f;
    [Functions addKeyboardToolbarForTextField:self.textField width:self.frame.size.width target:self doneTitle:@"Готово" doneAction:@selector(hideKB)];
    [self.textField.formatter setDefaultOutputPattern:@"+#(###)###-##-##"];
    
    [self.textField mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.bottom.equalTo(self.border.mas_top).with.offset(0);
        make.centerX.equalTo(self);
    }];
    
    self.name = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:12 andSuperView:self];
    self.name.alpha = 0.54f;
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.centerX.equalTo(self);
        make.bottom.equalTo(self.textField.mas_top).with.offset(5);
    }];
}

@end
