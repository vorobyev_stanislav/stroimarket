//
//  SMProfileModuleTableViewPhoneCell.h
//  stroimarket
//
//  Created by apple on 09.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMProfileModuleCellPresenter.h"

@interface SMProfileModuleTableViewPhoneCell : RETableViewCell <SMProfileModuleCellInput>

@property (strong, readwrite, nonatomic) SMProfileModulePhoneCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
