//
//  SMProfileModuleTableViewEmptyCell.h
//  stroimarket
//
//  Created by apple on 09.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMProfileModuleCellPresenter.h"

@interface SMProfileModuleTableViewEmptyCell : RETableViewCell <SMProfileModuleCellInput>

@property (strong, readwrite, nonatomic) SMProfileModuleEmptyCellPresenter *item;

@end
