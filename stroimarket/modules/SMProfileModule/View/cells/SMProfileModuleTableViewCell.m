//
//  SMProfileModuleSMProfileModuleTableViewCell.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMProfileModuleTableViewCell.h"
#import "Functions.h"

@implementation SMProfileModuleTableViewCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 175*[Functions koefX] + 24*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

#pragma mark - Методы обработки событий от визуальных элементов

- (void)editProfile {
    NSLog(@"edit profile");
    [self.item editProfile];
}

#pragma mark - Методы SMProfileModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    self.background = [UIFabric imageViewWithImageName:@"ic_header_empty" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];
    self.background.clipsToBounds = YES;

    [self.background mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).with.offset(0);
        make.left.equalTo(self).with.offset(0);
        make.right.equalTo(self).with.offset(0);
        make.bottom.equalTo(self).with.offset(-24*[Functions koefX]);
    }];
    
    self.bonuses = [UIFabric imageViewWithImageName:@"ic_menu_bonuses_white" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];
    
    [self.bonuses mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(18*[Functions koefX]);
        make.bottom.equalTo(self).with.offset(-10 - 24*[Functions koefX]);
    }];
    
    NSInteger bonus = [[[NSUserDefaults standardUserDefaults] valueForKey:@"bonus"] integerValue];
    self.count = [UIFabric labelWithText:[NSString stringWithFormat:@"%li бонусов", bonus] andTextColor:[UIColor whiteColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andSuperView:self];
    self.count.alpha = 0.87f;
    
    [self.count mas_remakeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(self).with.offset(70*[Functions koefX]);
        make.bottom.equalTo(self).with.offset(-10 - 24*[Functions koefX]);
    }];
    
    self.edit = [UIFabric imageViewWithImageName:@"ic_button_edit" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editProfile)];
    gesture.numberOfTapsRequired = 1;
    self.edit.userInteractionEnabled = YES;
    [self.edit addGestureRecognizer:gesture];
    
    [self.edit mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(48*[Functions koefX]);
        make.height.mas_equalTo(48*[Functions koefX]);
        make.right.equalTo(self).with.offset(-18*[Functions koefX]);
        make.bottom.equalTo(self).with.offset(0);
    }];
}

@end
