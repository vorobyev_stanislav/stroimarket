//
//  SMProfileModuleTableViewTextCell.h
//  stroimarket
//
//  Created by apple on 09.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMProfileModuleCellPresenter.h"

@interface SMProfileModuleTableViewTextCell : RETableViewCell <SMProfileModuleCellInput>

@property (strong, readwrite, nonatomic) SMProfileModuleTextCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
