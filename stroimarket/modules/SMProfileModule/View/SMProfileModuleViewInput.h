//
//  SMProfileModuleSMProfileModuleViewInput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMProfileModuleViewInput <NSObject>

/**
 @author apple

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)progressShow;
- (void)progressDismiss;
- (void)editEnable;
- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo;
- (void)hideKB:(NSDictionary *)userInfo;

@end
