//
//  SMProfileModuleSMProfileModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMProfileModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMProfileModuleViewOutput;

@interface SMProfileModuleViewController : UIViewController <SMProfileModuleViewInput>

@property (nonatomic, strong) id<SMProfileModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end
