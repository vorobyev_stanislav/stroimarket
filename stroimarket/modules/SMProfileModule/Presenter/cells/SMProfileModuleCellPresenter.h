//
//  SMProfileModuleSMProfileModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "cellModel.h"
#import "SMProfileModuleInteractorInput.h"

@protocol SMProfileModuleCellInput <NSObject>

@end

@interface SMProfileModuleTextCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMProfileModuleCellInput> cell;
@property (nonatomic, strong) id<SMProfileModuleInteractorInput> interactor;

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo;
- (void)hideKB:(NSDictionary *)userInfo;

@end

@interface SMProfileModuleEmptyCellPresenter : RETableViewItem

@property (nonatomic, strong) id<SMProfileModuleCellInput> cell;

@end

@interface SMProfileModuleDateCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMProfileModuleCellInput> cell;
@property (strong, nonatomic) id<SMProfileModuleInteractorInput> interactor;

@end

@interface SMProfileModulePhoneCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) cellModel *model;
@property (nonatomic, strong) id<SMProfileModuleCellInput> cell;

@end

@interface SMProfileModuleCellPresenter : RETableViewItem

//@property (strong, readwrite, nonatomic) SMProfileModuleModel *model;
@property (nonatomic, strong) id<SMProfileModuleCellInput> cell;
@property (nonatomic, strong) id<SMProfileModuleInteractorInput> interactor;

- (void)editProfile;

@end
