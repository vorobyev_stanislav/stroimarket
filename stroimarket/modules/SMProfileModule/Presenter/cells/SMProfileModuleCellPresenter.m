//
//  SMProfileModuleSMProfileModuleCellPresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMProfileModuleCellPresenter.h"

#import "Functions.h"

@implementation SMProfileModuleTextCellPresenter

- (void)hideKB:(NSDictionary *)userInfo {
    [self.interactor hideKB:userInfo];
}

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo {
    [self.interactor showKB:type andUserInfo:userInfo];
}

@end

@implementation SMProfileModuleEmptyCellPresenter

@end

@implementation SMProfileModuleDateCellPresenter

@end

@implementation SMProfileModulePhoneCellPresenter

@end

@implementation SMProfileModuleCellPresenter

- (void)editProfile {
    [MySingleton sharedMySingleton].edit = YES;
    [self.interactor makeProfile];
}

@end
