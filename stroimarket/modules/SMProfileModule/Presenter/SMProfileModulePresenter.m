//
//  SMProfileModuleSMProfileModulePresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMProfileModulePresenter.h"

#import "SMProfileModuleViewInput.h"
#import "SMProfileModuleInteractorInput.h"
#import "SMProfileModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMProfileModulePresenter

#pragma mark - Методы SMProfileModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMProfileModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMProfileModuleViewOutput

- (void)saveProfile {
    [self.interactor saveProfile];
}

- (void) viewWillApear {
    [self.interactor makeProfile];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMProfileModuleInteractorOutput

- (void)hideKB:(NSDictionary *)userInfo {
    [self.view hideKB:userInfo];
}

- (void)showKB:(NSInteger)type andUserInfo:(NSDictionary *)userInfo {
    [self.view showKB:type andUserInfo:userInfo];
}

- (void)editEnable {
    [self.view editEnable];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
