//
//  SMProfileModuleSMProfileModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMProfileModuleViewOutput.h"
#import "SMProfileModuleInteractorOutput.h"
#import "SMProfileModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMProfileModuleViewInput;
@protocol SMProfileModuleInteractorInput;
@protocol SMProfileModuleRouterInput;

@interface SMProfileModulePresenter : NSObject <SMProfileModuleModuleInput, SMProfileModuleViewOutput, SMProfileModuleInteractorOutput>

@property (nonatomic, weak) id<SMProfileModuleViewInput> view;
@property (nonatomic, strong) id<SMProfileModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMProfileModuleRouterInput> router;

@end
