//
//  SMProfileModuleSMProfileModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMProfileModuleAssembly.h"

#import "SMProfileModuleViewController.h"
#import "SMProfileModuleInteractor.h"
#import "SMProfileModulePresenter.h"
#import "SMProfileModuleRouter.h"
#import "networkService.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMProfileModuleAssembly

- (SMProfileModuleViewController *)viewSMProfileModuleModule {
    return [TyphoonDefinition withClass:[SMProfileModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMProfileModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMProfileModuleModule]];
                          }];
}

- (SMProfileModuleInteractor *)interactorSMProfileModuleModule {
    return [TyphoonDefinition withClass:[SMProfileModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMProfileModuleModule]];
                              [definition injectProperty:@selector(networkService)
                                                    with:[self networkServiceSMProfileModule]];
                          }];
}

- (SMProfileModulePresenter *)presenterSMProfileModuleModule {
    return [TyphoonDefinition withClass:[SMProfileModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMProfileModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMProfileModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMProfileModuleModule]];

                          }];
}

- (SMProfileModuleRouter *)routerSMProfileModuleModule {
    return [TyphoonDefinition withClass:[SMProfileModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMProfileModuleModule]];
                          }];
}

- (networkService *)networkServiceSMProfileModule {
    return [TyphoonDefinition withClass:[networkService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
