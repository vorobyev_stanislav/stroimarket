//
//  SMRegisterCodeModuleSMRegisterCodeModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterCodeModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMRegisterCodeModuleRouter : NSObject <SMRegisterCodeModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
