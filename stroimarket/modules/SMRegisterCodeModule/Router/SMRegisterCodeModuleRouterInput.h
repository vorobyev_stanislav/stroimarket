//
//  SMRegisterCodeModuleSMRegisterCodeModuleRouterInput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMRegisterCodeModuleRouterInput <NSObject>

- (void)goBack;
- (void)openRegisterModuleWithParams:(NSDictionary *)params;

@end
