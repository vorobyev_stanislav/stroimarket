//
//  SMRegisterCodeModuleSMRegisterCodeModuleRouter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterCodeModuleRouter.h"
#import "SMRegisterModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMRegisterCodeModuleRouter

#pragma mark - Методы SMRegisterCodeModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openRegisterModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to register"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMRegisterModuleModuleInput> moduleInput) {
        [moduleInput configureModule];
        return nil;
    }];
}

@end
