//
//  SMRegisterCodeModuleSMRegisterCodeModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterCodeModuleAssembly.h"

#import "SMRegisterCodeModuleViewController.h"
#import "SMRegisterCodeModuleInteractor.h"
#import "SMRegisterCodeModulePresenter.h"
#import "SMRegisterCodeModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMRegisterCodeModuleAssembly

- (SMRegisterCodeModuleViewController *)viewSMRegisterCodeModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterCodeModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMRegisterCodeModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMRegisterCodeModuleModule]];
                          }];
}

- (SMRegisterCodeModuleInteractor *)interactorSMRegisterCodeModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterCodeModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMRegisterCodeModuleModule]];
                          }];
}

- (SMRegisterCodeModulePresenter *)presenterSMRegisterCodeModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterCodeModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMRegisterCodeModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMRegisterCodeModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMRegisterCodeModuleModule]];

                          }];
}

- (SMRegisterCodeModuleRouter *)routerSMRegisterCodeModuleModule {
    return [TyphoonDefinition withClass:[SMRegisterCodeModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMRegisterCodeModuleModule]];
                          }];
}

@end
