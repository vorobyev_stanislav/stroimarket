//
//  SMRegisterCodeModuleSMRegisterCodeModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterCodeModuleInteractor.h"

#import "SMRegisterCodeModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMRegisterCodeModuleCellPresenter.h"

@interface SMRegisterCodeModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMRegisterCodeModuleInteractor

#pragma mark - Методы SMRegisterCodeModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}
@end
