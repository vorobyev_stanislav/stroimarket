//
//  SMRegisterCodeModuleSMRegisterCodeModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterCodeModuleInteractorInput.h"


@protocol SMRegisterCodeModuleInteractorOutput;

@interface SMRegisterCodeModuleInteractor : NSObject <SMRegisterCodeModuleInteractorInput>

@property (nonatomic, weak) id<SMRegisterCodeModuleInteractorOutput> output;

@end
