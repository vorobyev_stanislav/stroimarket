//
//  SMRegisterCodeModuleSMRegisterCodeModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMRegisterCodeModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMRegisterCodeModuleViewOutput;

@interface SMRegisterCodeModuleViewController : UIViewController <SMRegisterCodeModuleViewInput>

@property (nonatomic, strong) id<SMRegisterCodeModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *code;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *send;
@property (weak, nonatomic) IBOutlet UIButton *resend;

@end
