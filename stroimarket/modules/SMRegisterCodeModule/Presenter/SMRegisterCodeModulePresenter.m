//
//  SMRegisterCodeModuleSMRegisterCodeModulePresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterCodeModulePresenter.h"

#import "SMRegisterCodeModuleViewInput.h"
#import "SMRegisterCodeModuleInteractorInput.h"
#import "SMRegisterCodeModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMRegisterCodeModulePresenter

#pragma mark - Методы SMRegisterCodeModuleModuleInput

- (void)configureModule:(NSDictionary *)params {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
    self.phone = [params valueForKey:@"phone"];
}

#pragma mark - Методы SMRegisterCodeModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMRegisterCodeModuleViewOutput

- (void)sendCode:(NSString *)code {
    [self.router openRegisterModuleWithParams:@{}];
}

- (void) viewWillApear {
    [self.view setPhoneLabel:self.phone];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMRegisterCodeModuleInteractorOutput
-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
