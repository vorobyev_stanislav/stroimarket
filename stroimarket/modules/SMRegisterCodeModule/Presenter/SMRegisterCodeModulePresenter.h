//
//  SMRegisterCodeModuleSMRegisterCodeModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMRegisterCodeModuleViewOutput.h"
#import "SMRegisterCodeModuleInteractorOutput.h"
#import "SMRegisterCodeModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMRegisterCodeModuleViewInput;
@protocol SMRegisterCodeModuleInteractorInput;
@protocol SMRegisterCodeModuleRouterInput;

@interface SMRegisterCodeModulePresenter : NSObject <SMRegisterCodeModuleModuleInput, SMRegisterCodeModuleViewOutput, SMRegisterCodeModuleInteractorOutput>

@property (nonatomic, weak) id<SMRegisterCodeModuleViewInput> view;
@property (nonatomic, strong) id<SMRegisterCodeModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMRegisterCodeModuleRouterInput> router;

@property NSString *phone;

@end
