//
//  SMRegisterCodeModuleSMRegisterCodeModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMRegisterCodeModuleModel.h"

@protocol SMRegisterCodeModuleCellInput <NSObject>

@end

@interface SMRegisterCodeModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMRegisterCodeModuleModel *model;
@property (nonatomic, strong) id<SMRegisterCodeModuleCellInput> cell;
@end
