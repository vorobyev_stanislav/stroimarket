//
//  SMSplashModuleSMSplashModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMSplashModuleCellPresenter.h"

@interface SMSplashModuleTableViewCell : RETableViewCell <SMSplashModuleCellInput>

@property (strong, readwrite, nonatomic) SMSplashModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
