//
//  SMSplashModuleSMSplashModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMSplashModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMSplashModuleViewOutput;

@interface SMSplashModuleViewController : UIViewController <SMSplashModuleViewInput>

@property (nonatomic, strong) id<SMSplashModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;
@end
