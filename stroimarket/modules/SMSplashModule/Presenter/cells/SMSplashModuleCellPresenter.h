//
//  SMSplashModuleSMSplashModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMSplashModuleModel.h"

@protocol SMSplashModuleCellInput <NSObject>

@end

@interface SMSplashModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMSplashModuleModel *model;
@property (nonatomic, strong) id<SMSplashModuleCellInput> cell;
@end
