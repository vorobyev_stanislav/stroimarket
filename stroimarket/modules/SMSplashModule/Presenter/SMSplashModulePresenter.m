//
//  SMSplashModuleSMSplashModulePresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSplashModulePresenter.h"

#import "SMSplashModuleViewInput.h"
#import "SMSplashModuleInteractorInput.h"
#import "SMSplashModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMSplashModulePresenter

#pragma mark - Методы SMSplashModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMSplashModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMSplashModuleViewOutput

- (void) viewWillApear {
    
//    [[NSUserDefaults standardUserDefaults] setValue:@"53cad52e92a666c8311a37fd66f65e49" forKey:@"authToken"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    [self.router openRegisterPhoneModuleWithParams:@{}];
//    return;
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"authToken"] length]) {
        [self.interactor setToken];
        [self.router openMainModuleWithParams:@{}];
    } else {
        [self.router openEnterPhoneModuleWithParams:@{}];
    }
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMSplashModuleInteractorOutput
-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
