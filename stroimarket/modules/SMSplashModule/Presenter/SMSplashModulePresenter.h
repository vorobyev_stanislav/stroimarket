//
//  SMSplashModuleSMSplashModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSplashModuleViewOutput.h"
#import "SMSplashModuleInteractorOutput.h"
#import "SMSplashModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMSplashModuleViewInput;
@protocol SMSplashModuleInteractorInput;
@protocol SMSplashModuleRouterInput;

@interface SMSplashModulePresenter : NSObject <SMSplashModuleModuleInput, SMSplashModuleViewOutput, SMSplashModuleInteractorOutput>

@property (nonatomic, weak) id<SMSplashModuleViewInput> view;
@property (nonatomic, strong) id<SMSplashModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMSplashModuleRouterInput> router;

@end
