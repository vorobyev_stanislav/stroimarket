//
//  SMSplashModuleSMSplashModuleRouter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSplashModuleRouter.h"
#import "SMMainModuleModuleInput.h"
#import "SMEnterPhoneModuleModuleInput.h"
#import "SMRegisterModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMSplashModuleRouter

#pragma mark - Методы SMSplashModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openMainModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to main"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMMainModuleModuleInput> moduleInput) {
        [moduleInput configureModule];
        return nil;
    }];
}

- (void)openEnterPhoneModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to enter"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMEnterPhoneModuleModuleInput> moduleInput) {
        [moduleInput configureModule];
        return nil;
    }];
}

- (void)openRegisterPhoneModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to register"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMRegisterModuleModuleInput> moduleInput) {
        [moduleInput configureModule];
        return nil;
    }];
}

@end
