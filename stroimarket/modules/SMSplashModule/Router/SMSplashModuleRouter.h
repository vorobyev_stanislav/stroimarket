//
//  SMSplashModuleSMSplashModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSplashModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMSplashModuleRouter : NSObject <SMSplashModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
