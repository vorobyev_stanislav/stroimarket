//
//  SMSplashModuleSMSplashModuleRouterInput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMSplashModuleRouterInput <NSObject>

- (void)goBack;
- (void)openEnterPhoneModuleWithParams:(NSDictionary *)params;
- (void)openMainModuleWithParams:(NSDictionary *)params;
- (void)openRegisterPhoneModuleWithParams:(NSDictionary *)params;

@end
