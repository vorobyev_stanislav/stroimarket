//
//  SMSplashModuleSMSplashModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSplashModuleAssembly.h"
#import "networkService.h"
#import "SMSplashModuleViewController.h"
#import "SMSplashModuleInteractor.h"
#import "SMSplashModulePresenter.h"
#import "SMSplashModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMSplashModuleAssembly

- (SMSplashModuleViewController *)viewSMSplashModuleModule {
    return [TyphoonDefinition withClass:[SMSplashModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSplashModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMSplashModuleModule]];
                          }];
}

- (SMSplashModuleInteractor *)interactorSMSplashModuleModule {
    return [TyphoonDefinition withClass:[SMSplashModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSplashModuleModule]];
                              [definition injectProperty:@selector(networkService)
                                                    with:[self networkServiceSMSplashModule]];
                          }];
}

- (SMSplashModulePresenter *)presenterSMSplashModuleModule {
    return [TyphoonDefinition withClass:[SMSplashModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMSplashModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMSplashModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMSplashModuleModule]];

                          }];
}

- (SMSplashModuleRouter *)routerSMSplashModuleModule {
    return [TyphoonDefinition withClass:[SMSplashModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMSplashModuleModule]];
                          }];
}

- (networkService *)networkServiceSMSplashModule {
    return [TyphoonDefinition withClass:[networkService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
