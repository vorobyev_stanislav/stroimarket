//
//  SMSplashModuleSMSplashModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSplashModuleInteractorInput.h"
#import "networkService.h"

@protocol SMSplashModuleInteractorOutput;

@interface SMSplashModuleInteractor : NSObject <SMSplashModuleInteractorInput>

@property (nonatomic, weak) id<SMSplashModuleInteractorOutput> output;
@property (strong, nonatomic) networkService *networkService;

@end
