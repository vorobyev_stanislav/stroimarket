//
//  SMSplashModuleSMSplashModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSplashModuleInteractor.h"

#import "SMSplashModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMSplashModuleCellPresenter.h"

@interface SMSplashModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMSplashModuleInteractor

#pragma mark - Методы SMSplashModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)setToken {
    [self.networkService setTokens:^() {
        
    }];
}

@end
