//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleViewOutput.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;

@protocol SMSubCatalog2ModuleViewOutput <NSObject>

/**
 @author apple

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)backBtnTap;
- (void)setTableViewManager:(RETableViewManager*)manager;
- (void)viewWillApear;
- (void)selectedCollection:(NSInteger)collectionId;

@end
