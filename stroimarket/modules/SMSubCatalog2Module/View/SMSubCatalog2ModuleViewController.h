//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleViewController.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMSubCatalog2ModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMSubCatalog2ModuleViewOutput;

@interface SMSubCatalog2ModuleViewController : UIViewController <SMSubCatalog2ModuleViewInput>

@property (nonatomic, strong) id<SMSubCatalog2ModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property NSArray *collections;

@end
