//
//  SMSubCatalog2ModuleCollectionViewCell.h
//  stroimarket
//
//  Created by apple on 22.05.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "catalogModel.h"

@interface SMSubCatalog2ModuleCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UILabel *price;

- (void)setCollection:(catalogModel *)collection;

@end
