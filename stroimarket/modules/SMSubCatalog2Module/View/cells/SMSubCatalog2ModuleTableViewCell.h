//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMSubCatalog2ModuleCellPresenter.h"

@interface SMSubCatalog2ModuleTableViewCell : RETableViewCell <SMSubCatalog2ModuleCellInput>

@property (strong, readwrite, nonatomic) SMSubCatalog2ModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *collection;

@end
