//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleViewController.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog2ModuleViewController.h"
#import "Functions.h"
#import "SMSubCatalog2ModuleCollectionViewCell.h"
#import "SMSubCatalog2ModuleViewOutput.h"

@implementation SMSubCatalog2ModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [Functions setupNavigationController:self.navigationController andSearchTitle:@"Я ищу" andTarget:self andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [super updateViewConstraints];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.view.backgroundColor = [colorScheme colorWithSchemeName:@"backgroundViewColor"];

    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    UICollectionView *cv = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    self.collectionView = cv;
    [self.collectionView setDataSource:(id)self];
    [self.collectionView setDelegate:(id)self];
    [self.view addSubview:cv];
    cv.backgroundColor = [UIColor clearColor];
    
    [self.collectionView registerClass:[SMSubCatalog2ModuleCollectionViewCell class] forCellWithReuseIdentifier:@"collection"];
}

#pragma mark - UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.collections.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SMSubCatalog2ModuleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collection" forIndexPath:indexPath];
    [cell setCollection:self.collections[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    catalogModel *cc = self.collections[indexPath.row];
    [self.output selectedCollection:cc.catalogId];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(ceil((self.view.frame.size.width - 4*18*[Functions koefX])/2), ceil(250*[Functions koefX]));
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 18*[Functions koefX], 0, 18*[Functions koefX]);
}

#pragma mark - Методы SMCatalogDetailModuleViewInput

- (void)collectionReceived:(NSArray *)collection {
    self.collections = collection;
    [self.collectionView reloadData];
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
