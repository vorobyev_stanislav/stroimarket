//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleAssembly.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog2ModuleAssembly.h"
#import "catalogService.h"
#import "SMSubCatalog2ModuleViewController.h"
#import "SMSubCatalog2ModuleInteractor.h"
#import "SMSubCatalog2ModulePresenter.h"
#import "SMSubCatalog2ModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMSubCatalog2ModuleAssembly

- (SMSubCatalog2ModuleViewController *)viewSMSubCatalog2ModuleModule {
    return [TyphoonDefinition withClass:[SMSubCatalog2ModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSubCatalog2ModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMSubCatalog2ModuleModule]];
                          }];
}

- (SMSubCatalog2ModuleInteractor *)interactorSMSubCatalog2ModuleModule {
    return [TyphoonDefinition withClass:[SMSubCatalog2ModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSubCatalog2ModuleModule]];
                              [definition injectProperty:@selector(catalogService)
                                                    with:[self catalogServiceSMSubCatalog2Module]];
                          }];
}

- (SMSubCatalog2ModulePresenter *)presenterSMSubCatalog2ModuleModule {
    return [TyphoonDefinition withClass:[SMSubCatalog2ModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMSubCatalog2ModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMSubCatalog2ModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMSubCatalog2ModuleModule]];

                          }];
}

- (SMSubCatalog2ModuleRouter *)routerSMSubCatalog2ModuleModule {
    return [TyphoonDefinition withClass:[SMSubCatalog2ModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMSubCatalog2ModuleModule]];
                          }];
}

- (catalogService *)catalogServiceSMSubCatalog2Module {
    return [TyphoonDefinition withClass:[catalogService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
