//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleAssembly.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Typhoon/Typhoon.h>
//#import <RamblerTyphoonUtils/AssemblyCollector.h>
//#import "BaseModuleAssembly.h"
/**
 @author apple

 SMSubCatalog2Module module
 */
@interface SMSubCatalog2ModuleAssembly : TyphoonAssembly// <RamblerInitialAssembly>

@end
