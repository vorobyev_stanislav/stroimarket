//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleInteractorInput.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;
@protocol SMSubCatalog2ModuleInteractorInput <NSObject>

- (void)setTableViewManager:(RETableViewManager*)manager;
- (void)makeCatalog:(NSInteger)catalogId;

@end
