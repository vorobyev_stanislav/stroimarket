//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleInteractor.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog2ModuleInteractor.h"

#import "SMSubCatalog2ModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMSubCatalog2ModuleCellPresenter.h"

@interface SMSubCatalog2ModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMSubCatalog2ModuleInteractor

#pragma mark - Методы SMSubCatalog2ModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)makeCatalog:(NSInteger)catalogId {
    [self.catalogService getSubCatalog:catalogId completed:^(NSArray<catalogModel *> *catalogs) {
        [self.output collectionReceived:catalogs];
    }];
}

@end
