//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleInteractor.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog2ModuleInteractorInput.h"
#import "catalogService.h"

@protocol SMSubCatalog2ModuleInteractorOutput;

@interface SMSubCatalog2ModuleInteractor : NSObject <SMSubCatalog2ModuleInteractorInput>

@property (nonatomic, weak) id<SMSubCatalog2ModuleInteractorOutput> output;
@property (nonatomic, strong) catalogService *catalogService;

@end
