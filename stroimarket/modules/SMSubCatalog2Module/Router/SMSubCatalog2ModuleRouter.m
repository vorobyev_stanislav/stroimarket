//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleRouter.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog2ModuleRouter.h"
#import "SMCatalogDetailModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMSubCatalog2ModuleRouter

#pragma mark - Методы SMSubCatalog2ModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openCatalogDetailModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to collection detail"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMCatalogDetailModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

@end
