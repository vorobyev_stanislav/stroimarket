//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleRouterInput.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMSubCatalog2ModuleRouterInput <NSObject>

- (void)goBack;
- (void)openCatalogDetailModuleWithParams:(NSDictionary *)params;

@end
