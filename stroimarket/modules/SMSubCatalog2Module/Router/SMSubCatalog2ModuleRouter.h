//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleRouter.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog2ModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMSubCatalog2ModuleRouter : NSObject <SMSubCatalog2ModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
