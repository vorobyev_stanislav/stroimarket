//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleModuleInput.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@protocol SMSubCatalog2ModuleModuleInput <RamblerViperModuleInput>

/**
 @author apple

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule:(NSDictionary *)params;

@end
