//
//  SMSubCatalog2ModuleSMSubCatalog2ModulePresenter.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog2ModuleViewOutput.h"
#import "SMSubCatalog2ModuleInteractorOutput.h"
#import "SMSubCatalog2ModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMSubCatalog2ModuleViewInput;
@protocol SMSubCatalog2ModuleInteractorInput;
@protocol SMSubCatalog2ModuleRouterInput;

@interface SMSubCatalog2ModulePresenter : NSObject <SMSubCatalog2ModuleModuleInput, SMSubCatalog2ModuleViewOutput, SMSubCatalog2ModuleInteractorOutput>

@property (nonatomic, weak) id<SMSubCatalog2ModuleViewInput> view;
@property (nonatomic, strong) id<SMSubCatalog2ModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMSubCatalog2ModuleRouterInput> router;

@property NSInteger catalogId;

@end
