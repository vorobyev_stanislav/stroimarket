//
//  SMSubCatalog2ModuleSMSubCatalog2ModulePresenter.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog2ModulePresenter.h"

#import "SMSubCatalog2ModuleViewInput.h"
#import "SMSubCatalog2ModuleInteractorInput.h"
#import "SMSubCatalog2ModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMSubCatalog2ModulePresenter

#pragma mark - Методы SMSubCatalog2ModuleModuleInput

- (void)configureModule:(NSDictionary *)params {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
    self.catalogId = [[params valueForKey:@"collectionId"] integerValue];
}

#pragma mark - Методы SMSubCatalog2ModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMSubCatalog2ModuleViewOutput

- (void)selectedCollection:(NSInteger)collectionId {
    [self.router openCatalogDetailModuleWithParams:@{@"collectionId" : @(collectionId)}];
}

- (void) viewWillApear {
    [self.interactor makeCatalog:self.catalogId];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMSubCatalog2ModuleInteractorOutput

- (void)collectionReceived:(NSArray *)collection {
    [self.view collectionReceived:collection];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
