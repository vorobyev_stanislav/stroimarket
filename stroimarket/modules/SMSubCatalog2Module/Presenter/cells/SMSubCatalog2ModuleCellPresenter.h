//
//  SMSubCatalog2ModuleSMSubCatalog2ModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "catalogModel.h"

@protocol SMSubCatalog2ModuleCellInput <NSObject>

@end

@interface SMSubCatalog2ModuleCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) catalogModel *model;
@property (nonatomic, strong) id<SMSubCatalog2ModuleCellInput> cell;

@end
