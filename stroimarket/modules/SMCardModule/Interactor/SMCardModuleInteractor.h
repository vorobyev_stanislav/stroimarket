//
//  SMCardModuleSMCardModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCardModuleInteractorInput.h"


@protocol SMCardModuleInteractorOutput;

@interface SMCardModuleInteractor : NSObject <SMCardModuleInteractorInput>

@property (nonatomic, weak) id<SMCardModuleInteractorOutput> output;

@end
