//
//  SMCardModuleSMCardModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCardModuleInteractor.h"

#import "SMCardModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMCardModuleCellPresenter.h"

@interface SMCardModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMCardModuleInteractor

#pragma mark - Методы SMCardModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}
@end
