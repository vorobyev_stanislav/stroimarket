//
//  SMCardModuleSMCardModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCardModuleAssembly.h"

#import "SMCardModuleViewController.h"
#import "SMCardModuleInteractor.h"
#import "SMCardModulePresenter.h"
#import "SMCardModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMCardModuleAssembly

- (SMCardModuleViewController *)viewSMCardModuleModule {
    return [TyphoonDefinition withClass:[SMCardModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMCardModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMCardModuleModule]];
                          }];
}

- (SMCardModuleInteractor *)interactorSMCardModuleModule {
    return [TyphoonDefinition withClass:[SMCardModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMCardModuleModule]];
                          }];
}

- (SMCardModulePresenter *)presenterSMCardModuleModule {
    return [TyphoonDefinition withClass:[SMCardModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMCardModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMCardModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMCardModuleModule]];

                          }];
}

- (SMCardModuleRouter *)routerSMCardModuleModule {
    return [TyphoonDefinition withClass:[SMCardModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMCardModuleModule]];
                          }];
}

@end
