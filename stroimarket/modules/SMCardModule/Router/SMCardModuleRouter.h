//
//  SMCardModuleSMCardModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCardModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMCardModuleRouter : NSObject <SMCardModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
