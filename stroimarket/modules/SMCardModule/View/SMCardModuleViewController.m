//
//  SMCardModuleSMCardModuleViewController.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCardModuleViewController.h"
#import "Functions.h"
#import "SMCardModuleTableViewCell.h"
#import "SMCardModuleViewOutput.h"

@implementation SMCardModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Дисконтная карта" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    NSInteger bonus = [[[NSUserDefaults standardUserDefaults] valueForKey:@"bonus"] integerValue];
    self.qrCode.image = [self createQRForString:[NSString stringWithFormat:@"%li", bonus]];
    
    [self.output viewWillApear];
}

- (void)updateViewConstraints {
    [self.qrCode mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo([UIScreen mainScreen].bounds.size.width);
        make.height.mas_equalTo([UIScreen mainScreen].bounds.size.width);
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view);
    }];
    
    [self.label mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.qrCode.mas_bottom).with.offset(5);
    }];
    
    [super updateViewConstraints];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (IBAction)action:(id)sender {
    [[SlideNavigationController sharedInstance] openMenu:1 withCompletion:nil];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

#pragma mark - Вспомогательные функции

- (NSString *)qrCodeGenerator:(NSInteger)lastName andName:(NSString *)firstName andPhone:(NSString *)phone andEmail:(NSString *)email {
    NSMutableArray *vCardArray = [NSMutableArray new];
    [vCardArray addObject:@"BEGIN:VCARD"];
    [vCardArray addObject:@"VERSION:3.0"];
    [vCardArray addObject:[NSString stringWithFormat:@"FN:%@ %@", firstName, lastName]];
    [vCardArray addObject:[NSString stringWithFormat:@"N:%@;%@", lastName, firstName]];
    [vCardArray addObject:[NSString stringWithFormat:@"TEL;TYPE=work:%@", phone]];
    [vCardArray addObject:[NSString stringWithFormat:@"EMAIL;TYPE=INTERNET:%@", email]];
    [vCardArray addObject:@"END:VCARD"];
    return [vCardArray componentsJoinedByString:@"\n"];
}

- (UIImage *)createQRForString:(NSString *)qrString {
    
    NSData *stringData = [qrString dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = [UIScreen mainScreen].bounds.size.width / qrImage.extent.size.width;//self.qrCode.frame.size.width / qrImage.extent.size.width;
    float scaleY = [UIScreen mainScreen].bounds.size.width / qrImage.extent.size.height;//self.qrCode.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    return [UIImage imageWithCIImage:qrImage
                                            scale:[UIScreen mainScreen].scale
                                      orientation:UIImageOrientationUp];
}

- (void) createViewElements {
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.qrCode = [UIFabric imageViewWithImageName:@"" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self.view];
    
    self.label = [UIFabric labelWithText:@"Предъявите на кассе" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:19 andSuperView:self.view];
    self.label.alpha = 0.87f;
    
}
#pragma mark - Методы SMCardModuleViewInput

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
