//
//  SMCardModuleSMCardModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMCardModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMCardModuleViewOutput;

@interface SMCardModuleViewController : UIViewController <SMCardModuleViewInput>

@property (nonatomic, strong) id<SMCardModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIImageView *qrCode;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
