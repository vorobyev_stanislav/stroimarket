//
//  SMCardModuleSMCardModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMCardModuleCellPresenter.h"

@interface SMCardModuleTableViewCell : RETableViewCell <SMCardModuleCellInput>

@property (strong, readwrite, nonatomic) SMCardModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
