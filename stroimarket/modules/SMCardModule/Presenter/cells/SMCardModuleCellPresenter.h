//
//  SMCardModuleSMCardModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMCardModuleModel.h"

@protocol SMCardModuleCellInput <NSObject>

@end

@interface SMCardModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMCardModuleModel *model;
@property (nonatomic, strong) id<SMCardModuleCellInput> cell;
@end
