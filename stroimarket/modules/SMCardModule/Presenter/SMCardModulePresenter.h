//
//  SMCardModuleSMCardModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCardModuleViewOutput.h"
#import "SMCardModuleInteractorOutput.h"
#import "SMCardModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMCardModuleViewInput;
@protocol SMCardModuleInteractorInput;
@protocol SMCardModuleRouterInput;

@interface SMCardModulePresenter : NSObject <SMCardModuleModuleInput, SMCardModuleViewOutput, SMCardModuleInteractorOutput>

@property (nonatomic, weak) id<SMCardModuleViewInput> view;
@property (nonatomic, strong) id<SMCardModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMCardModuleRouterInput> router;

@end
