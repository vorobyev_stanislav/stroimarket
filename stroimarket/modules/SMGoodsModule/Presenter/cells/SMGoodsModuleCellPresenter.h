//
//  SMGoodsModuleSMGoodsModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMGoodsModuleModel.h"

@protocol SMGoodsModuleCellInput <NSObject>

@end

@interface SMGoodsModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMGoodsModuleModel *model;
@property (nonatomic, strong) id<SMGoodsModuleCellInput> cell;
@end
