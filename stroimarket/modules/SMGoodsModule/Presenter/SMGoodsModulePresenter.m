//
//  SMGoodsModuleSMGoodsModulePresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMGoodsModulePresenter.h"

#import "SMGoodsModuleViewInput.h"
#import "SMGoodsModuleInteractorInput.h"
#import "SMGoodsModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMGoodsModulePresenter

#pragma mark - Методы SMGoodsModuleModuleInput

- (void)configureModule:(NSDictionary *)params {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
    self.goods = [params objectForKey:@"goods"];
}

#pragma mark - Методы SMGoodsModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMGoodsModuleViewOutput

- (void)addProductToBasket {
    [self.interactor addProductToBasket:self.goods.goodsId withQuantity:1];
}

- (void) viewWillApear {
    [self.view setProduct:self.goods];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMGoodsModuleInteractorOutput
-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}

- (void)addSuccess {
    [self.view addSuccess];
}

- (void)addError {
    [self.view addError];
}

@end
