//
//  SMGoodsModuleSMGoodsModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMGoodsModuleViewOutput.h"
#import "SMGoodsModuleInteractorOutput.h"
#import "SMGoodsModuleModuleInput.h"
#import "catalogModel.h"
//#import "BasePresenter.h"

@protocol SMGoodsModuleViewInput;
@protocol SMGoodsModuleInteractorInput;
@protocol SMGoodsModuleRouterInput;

@interface SMGoodsModulePresenter : NSObject <SMGoodsModuleModuleInput, SMGoodsModuleViewOutput, SMGoodsModuleInteractorOutput>

@property (nonatomic, weak) id<SMGoodsModuleViewInput> view;
@property (nonatomic, strong) id<SMGoodsModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMGoodsModuleRouterInput> router;

@property goods *goods;

@end
