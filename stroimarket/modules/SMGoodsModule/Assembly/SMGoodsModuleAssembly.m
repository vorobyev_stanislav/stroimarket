//
//  SMGoodsModuleSMGoodsModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMGoodsModuleAssembly.h"

#import "SMGoodsModuleViewController.h"
#import "SMGoodsModuleInteractor.h"
#import "SMGoodsModulePresenter.h"
#import "SMGoodsModuleRouter.h"
#import "orderService.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMGoodsModuleAssembly

- (SMGoodsModuleViewController *)viewSMGoodsModuleModule {
    return [TyphoonDefinition withClass:[SMGoodsModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMGoodsModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMGoodsModuleModule]];
                          }];
}

- (SMGoodsModuleInteractor *)interactorSMGoodsModuleModule {
    return [TyphoonDefinition withClass:[SMGoodsModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMGoodsModuleModule]];
                              [definition injectProperty:@selector(orderService)
                                                    with:[self orderServiceSMGoodsModule]];
                          }];
}

- (SMGoodsModulePresenter *)presenterSMGoodsModuleModule {
    return [TyphoonDefinition withClass:[SMGoodsModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMGoodsModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMGoodsModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMGoodsModuleModule]];

                          }];
}

- (SMGoodsModuleRouter *)routerSMGoodsModuleModule {
    return [TyphoonDefinition withClass:[SMGoodsModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMGoodsModuleModule]];
                          }];
}

- (orderService *)orderServiceSMGoodsModule {
    return [TyphoonDefinition withClass:[orderService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
