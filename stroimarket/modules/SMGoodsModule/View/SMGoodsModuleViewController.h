//
//  SMGoodsModuleSMGoodsModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMGoodsModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMGoodsModuleViewOutput;

@interface SMGoodsModuleViewController : UIViewController <SMGoodsModuleViewInput>

@property (nonatomic, strong) id<SMGoodsModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *name;
//@property (weak, nonatomic) IBOutlet UILabel *oldPrice;
@property (weak, nonatomic) IBOutlet UILabel *Price;
@property (weak, nonatomic) IBOutlet UILabel *skidka;
@property (weak, nonatomic) IBOutlet UILabel *character;
@property (weak, nonatomic) IBOutlet UILabel *priceForUnit;
@property (weak, nonatomic) IBOutlet UILabel *unit;
@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UIButton *toBacket;

@end
