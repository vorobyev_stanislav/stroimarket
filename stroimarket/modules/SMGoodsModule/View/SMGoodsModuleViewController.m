//
//  SMGoodsModuleSMGoodsModuleViewController.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMGoodsModuleViewController.h"
#import "Functions.h"
#import "SMGoodsModuleTableViewCell.h"
#import "SMGoodsModuleViewOutput.h"
#import "catalogModel.h"

@implementation SMGoodsModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Описание товара" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    [self.photo mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(17*[Functions koefX]);
        make.left.equalTo(self.view).with.offset(17*[Functions koefX]);
        make.right.equalTo(self.view).with.offset(-17*[Functions koefX]);
        make.height.mas_equalTo(140*[Functions koefX]);
    }];
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(17*[Functions koefX]);
        make.right.equalTo(self.view).with.offset(-17*[Functions koefX]);
        make.top.equalTo(self.photo.mas_bottom).with.offset(17*[Functions koefX]);
    }];
    
    [self.character mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(17*[Functions koefX]);
        make.top.equalTo(self.name.mas_bottom).with.offset(17*[Functions koefX]);
    }];
    
    [self.Price mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).with.offset(-17*[Functions koefX]);
        make.centerY.equalTo(self.character).with.offset(0);
    }];
    
    [self.unit mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(17*[Functions koefX]);
        make.top.equalTo(self.character.mas_bottom).with.offset(17*[Functions koefX]);
    }];
    
    [self.desc mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(17*[Functions koefX]);
        make.top.equalTo(self.unit.mas_bottom).with.offset(17*[Functions koefX]);
    }];
    
    [self.toBacket mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).with.offset(-17*[Functions koefX]);
        make.top.equalTo(self.Price.mas_bottom).with.offset(17*[Functions koefX]);
        make.width.mas_equalTo(100*[Functions koefX]);
    }];
    
    [super updateViewConstraints];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

- (void)addToBasket {
    [self.output addProductToBasket];
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.photo = [UIFabric imageViewWithImageName:@"" andContentMode:UIViewContentModeScaleAspectFit iconMode:false andSuperView:self.view];
    self.photo.clipsToBounds = YES;
    
    self.name = [UIFabric labelWithText:@"" andTextColor:[UIColor blackColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andSuperView:self.view];
    self.name.alpha = 0.87f;
    
    self.character = [UIFabric labelWithText:@"Характеристики:" andTextColor:[UIColor blackColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:14 andSuperView:self.view];
    self.character.alpha = 0.87f;
    
    self.unit = [UIFabric labelWithText:@"Базовая единица:" andTextColor:[UIColor blackColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:14 andSuperView:self.view];
    self.unit.alpha = 0.87f;
    self.unit.numberOfLines = 0;
    
    self.desc = [UIFabric labelWithText:@"Описание:" andTextColor:[UIColor blackColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:14 andSuperView:self.view];
    self.desc.alpha = 0.87f;
    self.desc.numberOfLines = 0;
    
    self.skidka = [UIFabric labelWithText:@"-5%" andTextColor:[UIColor blackColor] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.view];
    self.skidka.alpha = 0.87f;
    
    self.priceForUnit = [UIFabric labelWithText:@"" andTextColor:[UIColor blackColor] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:11 andSuperView:self.view];
    self.priceForUnit.alpha = 0.87f;
    
    self.Price = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.view];
    self.Price.numberOfLines = 0;
    
    self.toBacket = [UIFabric buttonWithText:@"ОТМЕТИТЬ" andTextColor:[UIColor whiteColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:14 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    [self.toBacket addTarget:self action:@selector(addToBasket) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - Методы SMGoodsModuleViewInput

- (void)setProduct:(goods *)product {
    self.name.text = product.name;
    
    [self.photo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", domain, product.image]] placeholderImage:[UIImage imageNamed:@"ic_empty"] options:0];
    
    UIFont *arialFont = [UIFont fontWithName:@"Roboto-Medium" size:16.0*[Functions koefX]];
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: arialFont forKey:NSFontAttributeName];
    NSString *skidka = [NSString stringWithFormat:@"%@\n", @"-5%"];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:skidka attributes: arialDict];
    [aAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, skidka.length))];
    
    NSString *price = [NSString stringWithFormat:@"%@\n", product.price];
    UIFont *VerdanaFont = [UIFont fontWithName:@"Roboto-Regular" size:16.0*[Functions koefX]];
    NSDictionary *verdanaDict = [NSDictionary dictionaryWithObject:VerdanaFont forKey:NSFontAttributeName];
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString:price attributes:verdanaDict];
    [vAttrString addAttribute:NSForegroundColorAttributeName value:[colorScheme colorWithSchemeName:@"navigationBarColor"] range:(NSMakeRange(0, price.length))];
    [aAttrString appendAttributedString:vAttrString];
    
    NSString *str = @"цена за шт.";
    
    UIFont *VerdanaFont2 = [UIFont fontWithName:@"Roboto-Regular" size:11.0*[Functions koefX]];
    NSDictionary *verdanaDict2 = [NSDictionary dictionaryWithObject:VerdanaFont2 forKey:NSFontAttributeName];
    NSMutableAttributedString *vAttrString2 = [[NSMutableAttributedString alloc]initWithString:str attributes:verdanaDict2];
    [vAttrString2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.87f] range:(NSMakeRange(0, str.length))];
    [aAttrString appendAttributedString:vAttrString2];
    
    self.Price.attributedText = aAttrString;
    
    arialFont = [UIFont fontWithName:@"Roboto-Medium" size:16.0*[Functions koefX]];
    arialDict = [NSDictionary dictionaryWithObject: arialFont forKey:NSFontAttributeName];
    skidka = [NSString stringWithFormat:@"Базовая единица:\n"];
    aAttrString = [[NSMutableAttributedString alloc] initWithString:skidka attributes: arialDict];
    [aAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, skidka.length))];
    
    price = [NSString stringWithFormat:@"%@", @"м2"];
    VerdanaFont = [UIFont fontWithName:@"Roboto-Regular" size:16.0*[Functions koefX]];
    verdanaDict = [NSDictionary dictionaryWithObject:VerdanaFont forKey:NSFontAttributeName];
    vAttrString = [[NSMutableAttributedString alloc]initWithString:price attributes:verdanaDict];
    [vAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, price.length))];
    [aAttrString appendAttributedString:vAttrString];
    
    self.unit.attributedText = aAttrString;
    
    arialFont = [UIFont fontWithName:@"Roboto-Medium" size:16.0*[Functions koefX]];
    arialDict = [NSDictionary dictionaryWithObject: arialFont forKey:NSFontAttributeName];
    skidka = [NSString stringWithFormat:@"Описание:\n"];
    aAttrString = [[NSMutableAttributedString alloc] initWithString:skidka attributes: arialDict];
    [aAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, skidka.length))];
    
    price = [NSString stringWithFormat:@"%@", product.desc];
    VerdanaFont = [UIFont fontWithName:@"Roboto-Regular" size:16.0*[Functions koefX]];
    verdanaDict = [NSDictionary dictionaryWithObject:VerdanaFont forKey:NSFontAttributeName];
    vAttrString = [[NSMutableAttributedString alloc]initWithString:price attributes:verdanaDict];
    [vAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, price.length))];
    [aAttrString appendAttributedString:vAttrString];
    
    self.desc.attributedText = aAttrString;
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

- (void)addSuccess {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Товар добавлен в корзину!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

- (void)addError {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Произошла ошибка! Повторите операцию." cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

@end
