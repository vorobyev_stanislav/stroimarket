//
//  SMGoodsModuleSMGoodsModuleInteractorOutput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMGoodsModuleInteractorOutput <NSObject>
- (void)progressShow;
- (void)progressDismiss;
- (void)addSuccess;
- (void)addError;

@end
