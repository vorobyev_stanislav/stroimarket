//
//  SMGoodsModuleSMGoodsModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMGoodsModuleInteractor.h"

#import "SMGoodsModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMGoodsModuleCellPresenter.h"

@interface SMGoodsModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMGoodsModuleInteractor

#pragma mark - Методы SMGoodsModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)addProductToBasket:(NSInteger)productId withQuantity:(NSInteger)quantity {
    [self.output progressShow];
    [self.orderService addProductToBasket:productId quantity:quantity completed:^(NSInteger order) {
        [self.output progressDismiss];
        if(order) {
            [self.output addSuccess];
        } else {
            [self.output addError];
        }
    }];
}

@end
