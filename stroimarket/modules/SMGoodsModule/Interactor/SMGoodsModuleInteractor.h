//
//  SMGoodsModuleSMGoodsModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMGoodsModuleInteractorInput.h"
#import "orderService.h"

@protocol SMGoodsModuleInteractorOutput;

@interface SMGoodsModuleInteractor : NSObject <SMGoodsModuleInteractorInput>

@property (nonatomic, weak) id<SMGoodsModuleInteractorOutput> output;
@property (nonatomic, strong) orderService *orderService;

@end
