//
//  SMGoodsModuleSMGoodsModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMGoodsModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMGoodsModuleRouter : NSObject <SMGoodsModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
