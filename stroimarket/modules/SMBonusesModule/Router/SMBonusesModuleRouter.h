//
//  SMBonusesModuleSMBonusesModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMBonusesModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMBonusesModuleRouter : NSObject <SMBonusesModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
