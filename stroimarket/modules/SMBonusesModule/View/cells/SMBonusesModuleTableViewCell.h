//
//  SMBonusesModuleSMBonusesModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMBonusesModuleCellPresenter.h"

@interface SMBonusesModuleTableViewCell : RETableViewCell <SMBonusesModuleCellInput>

@property (strong, readwrite, nonatomic) SMBonusesModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
