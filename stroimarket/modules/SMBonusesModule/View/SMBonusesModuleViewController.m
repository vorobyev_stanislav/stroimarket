//
//  SMBonusesModuleSMBonusesModuleViewController.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMBonusesModuleViewController.h"
#import "Functions.h"
#import "SMBonusesModuleTableViewCell.h"
#import "SMBonusesModuleViewOutput.h"

@implementation SMBonusesModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Бонусы" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    
    [self.success mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(105*[Functions koefX]);
        make.height.mas_equalTo(105*[Functions koefX]);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).with.offset(30*[Functions koefX]);
    }];
    
    [self.bonusCount mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.success.mas_bottom).with.offset(30*[Functions koefX]);
    }];
    
    [self.send mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.top.equalTo(self.bonusCount.mas_bottom).with.offset(30*[Functions koefX]);
        make.centerX.equalTo(self.view);
    }];
    
    [super updateViewConstraints];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (IBAction)action:(id)sender {
    [[SlideNavigationController sharedInstance] openMenu:1 withCompletion:nil];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.success = [UIFabric imageViewWithImageName:@"ic_bonuses" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self.view];

    NSInteger bonus = [[[NSUserDefaults standardUserDefaults] valueForKey:@"bonus"] integerValue];
    self.bonusCount = [UIFabric labelWithText:[NSString stringWithFormat:@"У Вас %li баллов", bonus] andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.view];
    self.bonusCount.alpha = 0.87f;
    
    self.send = [UIFabric buttonWithText:@"ПОДЕЛИТЬСЯ С ДРУГОМ" andTextColor:[colorScheme colorWithSchemeName:@"backgroundCellColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    self.send.layer.cornerRadius = 2;
}
#pragma mark - Методы SMBonusesModuleViewInput

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
