//
//  SMBonusesModuleSMBonusesModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMBonusesModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMBonusesModuleViewOutput;

@interface SMBonusesModuleViewController : UIViewController <SMBonusesModuleViewInput>

@property (nonatomic, strong) id<SMBonusesModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIImageView *success;
@property (weak, nonatomic) IBOutlet UILabel *bonusCount;
@property (weak, nonatomic) IBOutlet UIButton *send;

@end
