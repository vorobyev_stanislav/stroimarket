//
//  SMBonusesModuleSMBonusesModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMBonusesModuleAssembly.h"

#import "SMBonusesModuleViewController.h"
#import "SMBonusesModuleInteractor.h"
#import "SMBonusesModulePresenter.h"
#import "SMBonusesModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMBonusesModuleAssembly

- (SMBonusesModuleViewController *)viewSMBonusesModuleModule {
    return [TyphoonDefinition withClass:[SMBonusesModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMBonusesModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMBonusesModuleModule]];
                          }];
}

- (SMBonusesModuleInteractor *)interactorSMBonusesModuleModule {
    return [TyphoonDefinition withClass:[SMBonusesModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMBonusesModuleModule]];
                          }];
}

- (SMBonusesModulePresenter *)presenterSMBonusesModuleModule {
    return [TyphoonDefinition withClass:[SMBonusesModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMBonusesModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMBonusesModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMBonusesModuleModule]];

                          }];
}

- (SMBonusesModuleRouter *)routerSMBonusesModuleModule {
    return [TyphoonDefinition withClass:[SMBonusesModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMBonusesModuleModule]];
                          }];
}

@end
