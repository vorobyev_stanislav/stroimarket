//
//  SMBonusesModuleSMBonusesModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMBonusesModuleInteractorInput.h"


@protocol SMBonusesModuleInteractorOutput;

@interface SMBonusesModuleInteractor : NSObject <SMBonusesModuleInteractorInput>

@property (nonatomic, weak) id<SMBonusesModuleInteractorOutput> output;

@end
