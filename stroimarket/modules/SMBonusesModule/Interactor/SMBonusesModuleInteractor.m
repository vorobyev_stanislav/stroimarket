//
//  SMBonusesModuleSMBonusesModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMBonusesModuleInteractor.h"

#import "SMBonusesModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMBonusesModuleCellPresenter.h"

@interface SMBonusesModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMBonusesModuleInteractor

#pragma mark - Методы SMBonusesModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}
@end
