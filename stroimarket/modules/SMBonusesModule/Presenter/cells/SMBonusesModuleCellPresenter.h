//
//  SMBonusesModuleSMBonusesModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMBonusesModuleModel.h"

@protocol SMBonusesModuleCellInput <NSObject>

@end

@interface SMBonusesModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMBonusesModuleModel *model;
@property (nonatomic, strong) id<SMBonusesModuleCellInput> cell;
@end
