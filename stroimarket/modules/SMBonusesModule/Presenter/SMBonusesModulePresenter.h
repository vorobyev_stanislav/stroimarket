//
//  SMBonusesModuleSMBonusesModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMBonusesModuleViewOutput.h"
#import "SMBonusesModuleInteractorOutput.h"
#import "SMBonusesModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMBonusesModuleViewInput;
@protocol SMBonusesModuleInteractorInput;
@protocol SMBonusesModuleRouterInput;

@interface SMBonusesModulePresenter : NSObject <SMBonusesModuleModuleInput, SMBonusesModuleViewOutput, SMBonusesModuleInteractorOutput>

@property (nonatomic, weak) id<SMBonusesModuleViewInput> view;
@property (nonatomic, strong) id<SMBonusesModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMBonusesModuleRouterInput> router;

@end
