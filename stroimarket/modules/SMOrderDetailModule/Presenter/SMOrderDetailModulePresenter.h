//
//  SMOrderDetailModuleSMOrderDetailModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMOrderDetailModuleViewOutput.h"
#import "SMOrderDetailModuleInteractorOutput.h"
#import "SMOrderDetailModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMOrderDetailModuleViewInput;
@protocol SMOrderDetailModuleInteractorInput;
@protocol SMOrderDetailModuleRouterInput;

@interface SMOrderDetailModulePresenter : NSObject <SMOrderDetailModuleModuleInput, SMOrderDetailModuleViewOutput, SMOrderDetailModuleInteractorOutput>

@property (nonatomic, weak) id<SMOrderDetailModuleViewInput> view;
@property (nonatomic, strong) id<SMOrderDetailModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMOrderDetailModuleRouterInput> router;

@end
