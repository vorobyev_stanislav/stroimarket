//
//  SMOrderDetailModuleSMOrderDetailModulePresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMOrderDetailModulePresenter.h"

#import "SMOrderDetailModuleViewInput.h"
#import "SMOrderDetailModuleInteractorInput.h"
#import "SMOrderDetailModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMOrderDetailModulePresenter

#pragma mark - Методы SMOrderDetailModuleModuleInput

- (void)configureModule:(NSDictionary *)params {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMOrderDetailModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMOrderDetailModuleViewOutput

- (void) viewWillApear {
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMOrderDetailModuleInteractorOutput
-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
