//
//  SMOrderDetailModuleSMOrderDetailModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMOrderDetailModuleModel.h"

@protocol SMOrderDetailModuleCellInput <NSObject>

@end

@interface SMOrderDetailModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMOrderDetailModuleModel *model;
@property (nonatomic, strong) id<SMOrderDetailModuleCellInput> cell;
@end
