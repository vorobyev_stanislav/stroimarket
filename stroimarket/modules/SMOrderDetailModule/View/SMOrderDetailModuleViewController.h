//
//  SMOrderDetailModuleSMOrderDetailModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMOrderDetailModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMOrderDetailModuleViewOutput;

@interface SMOrderDetailModuleViewController : UIViewController <SMOrderDetailModuleViewInput>

@property (nonatomic, strong) id<SMOrderDetailModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;
@end
