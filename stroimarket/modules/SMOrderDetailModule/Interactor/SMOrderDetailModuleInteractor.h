//
//  SMOrderDetailModuleSMOrderDetailModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMOrderDetailModuleInteractorInput.h"


@protocol SMOrderDetailModuleInteractorOutput;

@interface SMOrderDetailModuleInteractor : NSObject <SMOrderDetailModuleInteractorInput>

@property (nonatomic, weak) id<SMOrderDetailModuleInteractorOutput> output;

@end
