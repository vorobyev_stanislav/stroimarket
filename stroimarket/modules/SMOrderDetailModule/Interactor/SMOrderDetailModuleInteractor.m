//
//  SMOrderDetailModuleSMOrderDetailModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMOrderDetailModuleInteractor.h"

#import "SMOrderDetailModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMOrderDetailModuleCellPresenter.h"

@interface SMOrderDetailModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMOrderDetailModuleInteractor

#pragma mark - Методы SMOrderDetailModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}
@end
