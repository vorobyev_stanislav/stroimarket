//
//  SMOrderDetailModuleSMOrderDetailModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMOrderDetailModuleAssembly.h"

#import "SMOrderDetailModuleViewController.h"
#import "SMOrderDetailModuleInteractor.h"
#import "SMOrderDetailModulePresenter.h"
#import "SMOrderDetailModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMOrderDetailModuleAssembly

- (SMOrderDetailModuleViewController *)viewSMOrderDetailModuleModule {
    return [TyphoonDefinition withClass:[SMOrderDetailModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMOrderDetailModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMOrderDetailModuleModule]];
                          }];
}

- (SMOrderDetailModuleInteractor *)interactorSMOrderDetailModuleModule {
    return [TyphoonDefinition withClass:[SMOrderDetailModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMOrderDetailModuleModule]];
                          }];
}

- (SMOrderDetailModulePresenter *)presenterSMOrderDetailModuleModule {
    return [TyphoonDefinition withClass:[SMOrderDetailModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMOrderDetailModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMOrderDetailModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMOrderDetailModuleModule]];

                          }];
}

- (SMOrderDetailModuleRouter *)routerSMOrderDetailModuleModule {
    return [TyphoonDefinition withClass:[SMOrderDetailModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMOrderDetailModuleModule]];
                          }];
}

@end
