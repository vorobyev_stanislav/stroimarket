//
//  SMOrderDetailModuleSMOrderDetailModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMOrderDetailModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMOrderDetailModuleRouter : NSObject <SMOrderDetailModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
