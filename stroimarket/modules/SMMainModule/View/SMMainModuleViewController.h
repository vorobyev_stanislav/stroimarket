//
//  SMMainModuleSMMainModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMMainModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMMainModuleViewOutput;

@interface SMMainModuleViewController : UIViewController <SMMainModuleViewInput>

@property (nonatomic, strong) id<SMMainModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;

@end
