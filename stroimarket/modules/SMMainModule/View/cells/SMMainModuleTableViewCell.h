//
//  SMMainModuleSMMainModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMMainModuleCellPresenter.h"

@interface SMMainModuleTableViewCell : RETableViewCell <SMMainModuleCellInput>

@property (strong, readwrite, nonatomic) SMMainModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIPageControl *page;

@end
