//
//  SMMainModuleTableViewButtonCell.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMMainModuleTableViewButtonCell.h"

@implementation SMMainModuleTableViewButtonCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 70*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

#pragma mark - Методы обработки событий от визуальных элементов

- (void)regButtonClick {
    [MySingleton sharedMySingleton].menu = 2;
}

#pragma mark - Методы SMRegisterModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    
    self.catalog = [UIFabric buttonWithText:@"Перейти в каталог" andTextColor:[colorScheme colorWithSchemeName:@"backgroundCellColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:19 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self];
    self.catalog.layer.cornerRadius = 2;
    [self.catalog addTarget:self action:@selector(regButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.catalog mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40*[Functions koefX]);
        make.left.equalTo(self).with.offset(18*[Functions koefX]);
        make.right.equalTo(self).with.offset(-18*[Functions koefX]);
        make.centerY.equalTo(self);
    }];
    
}

@end
