//
//  SMMainModuleTableViewButtonCell.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMMainModuleCellPresenter.h"

@interface SMMainModuleTableViewButtonCell : RETableViewCell <SMMainModuleCellInput>

@property (strong, readwrite, nonatomic) SMMainModuleButtonCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIButton *catalog;

@end
