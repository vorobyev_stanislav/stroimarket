//
//  SMMainModuleSMMainModuleTableViewCell.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMainModuleTableViewCell.h"
#import "Functions.h"
#import "networkService.h"

@implementation SMMainModuleTableViewCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 120*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    for(int i=0;i<self.item.model.count;i++) {
        slider *cc = self.item.model[i];
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width*i, 0, self.frame.size.width, 120*[Functions koefX])];
//        img.image = [UIImage imageNamed:cc.image];
        [img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", domain, cc.image]] placeholderImage:[UIImage imageNamed:@"ic_empty"] options:0];
        img.clipsToBounds = YES;
        img.contentMode = UIViewContentModeScaleAspectFill;
        [self.scroll addSubview:img];
    }
    
    self.scroll.contentSize = CGSizeMake(self.frame.size.width*self.item.model.count, 120*[Functions koefX]);
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMMainModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.scroll = [UIFabric scrollView:self];
    self.scroll.pagingEnabled = YES;

    [self.scroll mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

@end
