//
//  SMMainModuleTableViewNewsCell.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMMainModuleCellPresenter.h"

@interface SMMainModuleTableViewNewsCell : RETableViewCell <SMMainModuleCellInput>

@property (strong, readwrite, nonatomic) SMMainModuleNewsCellPresenter *item;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *nameAll;

@end
