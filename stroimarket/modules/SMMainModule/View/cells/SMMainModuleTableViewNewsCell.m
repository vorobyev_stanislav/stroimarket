//
//  SMMainModuleTableViewNewsCell.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMMainModuleTableViewNewsCell.h"

@implementation SMMainModuleTableViewNewsCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 280*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    for(id v in self.subviews) {
        if(![v isKindOfClass:[UILabel class]]) {
            UIView *vi = (UIView *)v;
            [vi removeFromSuperview];
        }
    }
    
    self.name.text = self.item.model.title;
    self.nameAll.text = self.item.model.titleAll;
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:self.item.model.titleAll];
    [attString addAttribute:(NSString *)NSUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:NSUnderlineStyleSingle]
                      range:(NSRange){0,[attString length]}];
    self.nameAll.attributedText = attString;
    
    for(int i=0;i<self.item.model.objects.count;i++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(18*[Functions koefX]*(i+1) + (self.frame.size.width - 3*18*[Functions koefX])/2*i, 30*[Functions koefX], (self.frame.size.width - 3*18*[Functions koefX])/2, 225*[Functions koefX])];
        view.backgroundColor = [UIColor whiteColor];
        view.tag = self.item.model.objects[i].objectId;
        [self addSubview:view];
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openNews:)];
        gesture.numberOfTapsRequired = 1;
        view.userInteractionEnabled = YES;
        [view addGestureRecognizer:gesture];
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, (self.frame.size.width - 3*18*[Functions koefX])/2, 160*[Functions koefX])];
        img.contentMode = UIViewContentModeScaleAspectFill;
        img.clipsToBounds = YES;
//        img.image = [UIImage imageNamed:self.item.model.objects[i].avatar];
        [img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", domain, self.item.model.objects[i].avatar]] placeholderImage:[UIImage imageNamed:@"ic_empty"] options:0];
        
        if(self.item.model.objects[i].avatar.length) {
            img.contentMode = UIViewContentModeScaleAspectFit;
        } else {
            img.contentMode = UIViewContentModeCenter;
        }
        
        img.tag = self.item.model.objects[i].objectId;
        [view addSubview:img];
        gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openNews:)];
        gesture.numberOfTapsRequired = 1;
        img.userInteractionEnabled = YES;
        [img addGestureRecognizer:gesture];
        
        UILabel *label = [UILabel new];
        label.font = [UIFont fontWithName:@"Roboto-Regular" size:14*[Functions koefX]];
        label.textColor = [colorScheme colorWithSchemeName:@"menuTextColor"];
        label.alpha = 0.87f;
        label.numberOfLines = 0;
        [view addSubview:label];
        label.frame = CGRectMake(10*[Functions koefX], img.frame.size.height, view.frame.size.width - 20*[Functions koefX], view.frame.size.height - img.frame.size.height);
        label.text = self.item.model.objects[i].name;
        label.tag = self.item.model.objects[i].objectId;
        gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openNews:)];
        gesture.numberOfTapsRequired = 1;
        label.userInteractionEnabled = YES;
        [label addGestureRecognizer:gesture];
    }
}

#pragma mark - Методы обработки событий от визуальных элементов

- (void)regButtonClick {
    //    [self.item registerUser];
}

- (void)openNews:(UITapGestureRecognizer *)sender {
    object *obj = self.item.model.objects[sender.view.tag];
    [self.item openNews:obj.text type:self.item.model.title];
}

- (void)openAll {
    [self.item openAllNews:self.item.model.title];
}

#pragma mark - Методы SMRegisterModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    
    self.name = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self];
    self.name.alpha = 0.87f;
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(18*[Functions koefX]);
        make.top.equalTo(self).with.offset(0);
    }];
    
    self.nameAll = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:14 andSuperView:self];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openAll)];
    gesture.numberOfTapsRequired = 1;
    self.nameAll.userInteractionEnabled = YES;
    [self.nameAll addGestureRecognizer:gesture];
    
    [self.nameAll mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).with.offset(-18*[Functions koefX]);
        make.top.equalTo(self).with.offset(0);
    }];
}

@end
