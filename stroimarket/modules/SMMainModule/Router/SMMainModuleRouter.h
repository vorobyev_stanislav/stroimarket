//
//  SMMainModuleSMMainModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMainModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMMainModuleRouter : NSObject <SMMainModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
