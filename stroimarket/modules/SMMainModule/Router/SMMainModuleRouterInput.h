//
//  SMMainModuleSMMainModuleRouterInput.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMMainModuleRouterInput <NSObject>

- (void)goBack;
- (void)openNewsModuleWithParams:(NSDictionary *)params;
- (void)openNewsDetailModuleWithParams:(NSDictionary *)params;

@end
