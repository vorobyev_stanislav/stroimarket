//
//  SMMainModuleSMMainModuleRouter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMainModuleRouter.h"
#import "SMNewsModuleModuleInput.h"
#import "SMNewsDetailModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMMainModuleRouter

#pragma mark - Методы SMMainModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openNewsModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to news"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMNewsModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

- (void)openNewsDetailModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to news detail"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMNewsDetailModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

@end
