//
//  SMMainModuleSMMainModuleCellPresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMainModuleCellPresenter.h"

#import "Functions.h"

@implementation SMMainModuleButtonCellPresenter

@end

@implementation SMMainModuleNewsCellPresenter

- (void)openAllNews:(NSString *)type {
    [self.interactor openAllNews:type];
}

- (void)openNews:(NSString *)newsId type:(NSString *)type {
    [self.interactor openNews:newsId type:type];
}

@end

@implementation SMMainModuleCellPresenter

@end
