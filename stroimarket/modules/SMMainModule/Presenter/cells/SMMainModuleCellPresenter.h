//
//  SMMainModuleSMMainModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "mainPageModel.h"
#import "SMMainModuleInteractorInput.h"
#import "networkService.h"

@protocol SMMainModuleCellInput <NSObject>

@end

@interface SMMainModuleButtonCellPresenter : RETableViewItem

//@property (strong, readwrite, nonatomic) SMMainModuleModel *model;
@property (nonatomic, strong) id<SMMainModuleCellInput> cell;

@end

@interface SMMainModuleNewsCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) mainPageModel *model;
@property (nonatomic, strong) id<SMMainModuleCellInput> cell;
@property (strong, nonatomic) id<SMMainModuleInteractorInput> interactor;

- (void)openAllNews:(NSString *)type;
- (void)openNews:(NSString *)text type:(NSString *)type;

@end

@interface SMMainModuleCellPresenter : RETableViewItem

@property (nonatomic, strong) id<SMMainModuleCellInput> cell;
@property (strong, readwrite, nonatomic) NSArray<slider *> *model;

@end
