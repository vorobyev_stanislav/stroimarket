//
//  SMMainModuleSMMainModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMainModuleViewOutput.h"
#import "SMMainModuleInteractorOutput.h"
#import "SMMainModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMMainModuleViewInput;
@protocol SMMainModuleInteractorInput;
@protocol SMMainModuleRouterInput;

@interface SMMainModulePresenter : NSObject <SMMainModuleModuleInput, SMMainModuleViewOutput, SMMainModuleInteractorOutput>

@property (nonatomic, weak) id<SMMainModuleViewInput> view;
@property (nonatomic, strong) id<SMMainModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMMainModuleRouterInput> router;

@end
