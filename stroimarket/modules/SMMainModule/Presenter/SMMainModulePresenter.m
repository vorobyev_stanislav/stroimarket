//
//  SMMainModuleSMMainModulePresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMainModulePresenter.h"
#import "cellModel.h"
#import "SMMainModuleViewInput.h"
#import "SMMainModuleInteractorInput.h"
#import "SMMainModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMMainModulePresenter

#pragma mark - Методы SMMainModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMMainModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMMainModuleViewOutput

- (void) viewWillApear {
    [self.interactor makeCells];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMMainModuleInteractorOutput

- (void)openAllNews:(NSString *)type {
    [self.router openNewsModuleWithParams:@{@"title" : type}];
}

- (void)openNews:(NSString *)newsId type:(NSString *)type {
    newsModel *news = [newsModel new];
    news.text = newsId;
    [self.router openNewsDetailModuleWithParams:@{@"title" : type, @"news" : news}];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
