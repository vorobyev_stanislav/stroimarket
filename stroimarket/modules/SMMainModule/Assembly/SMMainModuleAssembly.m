//
//  SMMainModuleSMMainModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMainModuleAssembly.h"
#import "SMMainModuleViewController.h"
#import "SMMainModuleInteractor.h"
#import "SMMainModulePresenter.h"
#import "SMMainModuleRouter.h"
#import "networkService.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMMainModuleAssembly

- (SMMainModuleViewController *)viewSMMainModuleModule {
    return [TyphoonDefinition withClass:[SMMainModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMMainModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMMainModuleModule]];
                          }];
}

- (SMMainModuleInteractor *)interactorSMMainModuleModule {
    return [TyphoonDefinition withClass:[SMMainModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMMainModuleModule]];
                              [definition injectProperty:@selector(networkService)
                                                    with:[self networkServiceSMMainModule]];
                          }];
}

- (SMMainModulePresenter *)presenterSMMainModuleModule {
    return [TyphoonDefinition withClass:[SMMainModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMMainModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMMainModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMMainModuleModule]];

                          }];
}

- (SMMainModuleRouter *)routerSMMainModuleModule {
    return [TyphoonDefinition withClass:[SMMainModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMMainModuleModule]];
                          }];
}

- (networkService *)networkServiceSMMainModule {
    return [TyphoonDefinition withClass:[networkService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
