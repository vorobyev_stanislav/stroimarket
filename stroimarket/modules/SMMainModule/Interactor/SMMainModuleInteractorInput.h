//
//  SMMainModuleSMMainModuleInteractorInput.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;
@protocol SMMainModuleInteractorInput <NSObject>

- (void)setTableViewManager:(RETableViewManager*)manager;
- (void)makeCells;
- (void)openAllNews:(NSString *)type;
- (void)openNews:(NSString *)newsId type:(NSString *)type;

@end
