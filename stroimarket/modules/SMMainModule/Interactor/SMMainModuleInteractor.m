//
//  SMMainModuleSMMainModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMainModuleInteractor.h"

#import "SMMainModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMMainModuleCellPresenter.h"
#import "mainPageModel.h"

@interface SMMainModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMMainModuleInteractor

#pragma mark - Методы SMMainModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)makeCells {
    
    [self.networkService getMain:^(NSArray *slider, NSArray<stockModel *> *stocks, NSArray<newsModel *> *news, NSArray<productDayModel *> *products) {
        
        NSLog(@"stock %@ slider %@ news %@", stocks, slider, news);
        [self.manager.sections[0] removeAllItems];
        SMMainModuleCellPresenter *pres1 = [SMMainModuleCellPresenter item];
        pres1.model = slider;
        pres1.selectionHandler = ^(SMMainModuleCellPresenter *newValue){
            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres1];
        
        SMMainModuleButtonCellPresenter *pres2 = [SMMainModuleButtonCellPresenter item];
        //pres2.selectionHandler = ^(SMMainModuleButtonCellPresenter *newValue){
        //    [newValue deselectRowAnimated:YES];
        //};
        [self.manager.sections[0] addItem:(id)pres2];
        
        SMMainModuleNewsCellPresenter *pres3 = [SMMainModuleNewsCellPresenter item];
        mainPageModel *model = [mainPageModel new];
        model.title = @"Акции";
        model.titleAll = @"все акции";
        
        NSMutableArray *objs = [NSMutableArray new];
        
        for(int i=0;i<stocks.count;i++) {
            object *obj = [object new];
            obj.avatar = stocks[i].image;
            obj.name = [NSString stringWithFormat:@"Акция действует с %@ по %@", stocks[i].date_active_from, stocks[i].date_active_to];
            obj.text = stocks[i].text;
            [objs addObject:obj];
        }
        
        model.objects = objs;
        pres3.model = model;
        pres3.interactor = (id)self;
        pres3.selectionHandler = ^(SMMainModuleNewsCellPresenter *newValue){
//            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres3];
        
        pres3 = [SMMainModuleNewsCellPresenter item];
        model = [mainPageModel new];
        model.title = @"Новости";
        model.titleAll = @"все новости";
        
        objs = [NSMutableArray new];
        
        for(int i=0;i<news.count;i++) {
            object *obj = [object new];
            obj.avatar = news[i].preview;
            obj.name = news[i].name;
            obj.text = news[i].text;
            [objs addObject:obj];
        }
        
        model.objects = objs;
        
        pres3.model = model;
        pres3.interactor = (id)self;
        pres3.selectionHandler = ^(SMMainModuleNewsCellPresenter *newValue){
//            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres3];
        
        pres3 = [SMMainModuleNewsCellPresenter item];
        model = [mainPageModel new];
        model.title = @"Лучшее предложение";
        model.titleAll = @"все предложения";
        
        objs = [NSMutableArray new];
        
        for(int i=0;i<products.count;i++) {
            object *obj = [object new];
            obj.avatar =products[i].image;
            obj.name = products[i].name;
            obj.text = products[i].desc;
            [objs addObject:obj];
        }
        
        model.objects = objs;
        
        pres3.model = model;
        pres3.interactor = (id)self;
        pres3.selectionHandler = ^(SMMainModuleButtonCellPresenter *newValue){
            [newValue deselectRowAnimated:YES];
        };
        [self.manager.sections[0] addItem:(id)pres3];
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
    }];
}

- (void)openAllNews:(NSString *)type {
    [self.output openAllNews:type];
}

- (void)openNews:(NSString *)newsId type:(NSString *)type {
    [self.output openNews:newsId type:type];
}

@end
