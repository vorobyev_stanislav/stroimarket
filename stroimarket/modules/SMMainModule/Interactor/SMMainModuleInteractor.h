//
//  SMMainModuleSMMainModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMainModuleInteractorInput.h"
#import "networkService.h"
@protocol SMMainModuleInteractorOutput;

@interface SMMainModuleInteractor : NSObject <SMMainModuleInteractorInput>

@property (nonatomic, weak) id<SMMainModuleInteractorOutput> output;
@property (nonatomic, strong) networkService *networkService;

@end
