//
//  SMEnterCodeModuleSMEnterCodeModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterCodeModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMEnterCodeModuleRouter : NSObject <SMEnterCodeModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
