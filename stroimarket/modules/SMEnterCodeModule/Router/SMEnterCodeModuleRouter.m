//
//  SMEnterCodeModuleSMEnterCodeModuleRouter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterCodeModuleRouter.h"
#import "SMMainModuleModuleInput.h"
#import "SMEnterErrorModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMEnterCodeModuleRouter

#pragma mark - Методы SMEnterCodeModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openMainModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to main"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMMainModuleModuleInput> moduleInput) {
        [moduleInput configureModule];
        return nil;
    }];
}

- (void)openEnterErrorModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to error"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMEnterErrorModuleModuleInput> moduleInput) {
        [moduleInput configureModule];
        return nil;
    }];
}

@end
