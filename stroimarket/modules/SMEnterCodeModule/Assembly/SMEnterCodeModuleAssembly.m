//
//  SMEnterCodeModuleSMEnterCodeModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterCodeModuleAssembly.h"

#import "SMEnterCodeModuleViewController.h"
#import "SMEnterCodeModuleInteractor.h"
#import "SMEnterCodeModulePresenter.h"
#import "SMEnterCodeModuleRouter.h"
#import "networkService.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMEnterCodeModuleAssembly

- (SMEnterCodeModuleViewController *)viewSMEnterCodeModuleModule {
    return [TyphoonDefinition withClass:[SMEnterCodeModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMEnterCodeModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMEnterCodeModuleModule]];
                          }];
}

- (SMEnterCodeModuleInteractor *)interactorSMEnterCodeModuleModule {
    return [TyphoonDefinition withClass:[SMEnterCodeModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMEnterCodeModuleModule]];
                              [definition injectProperty:@selector(networkService)
                                                    with:[self networkServiceSMEnterErrorModule]];
                          }];
}

- (SMEnterCodeModulePresenter *)presenterSMEnterCodeModuleModule {
    return [TyphoonDefinition withClass:[SMEnterCodeModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMEnterCodeModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMEnterCodeModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMEnterCodeModuleModule]];

                          }];
}

- (SMEnterCodeModuleRouter *)routerSMEnterCodeModuleModule {
    return [TyphoonDefinition withClass:[SMEnterCodeModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMEnterCodeModuleModule]];
                          }];
}

- (networkService *)networkServiceSMEnterErrorModule {
    return [TyphoonDefinition withClass:[networkService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
