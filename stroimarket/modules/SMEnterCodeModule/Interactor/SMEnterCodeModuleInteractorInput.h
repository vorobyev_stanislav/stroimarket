//
//  SMEnterCodeModuleSMEnterCodeModuleInteractorInput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;
@protocol SMEnterCodeModuleInteractorInput <NSObject>

- (void)setTableViewManager:(RETableViewManager*)manager;
- (void)validateCode:(NSString *)code withPhone:(NSString *)phone;
- (void)resendCode:(NSString *)phone;

@end
