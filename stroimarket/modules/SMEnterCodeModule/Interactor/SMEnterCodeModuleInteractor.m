//
//  SMEnterCodeModuleSMEnterCodeModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterCodeModuleInteractor.h"

#import "SMEnterCodeModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMEnterCodeModuleCellPresenter.h"

@interface SMEnterCodeModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMEnterCodeModuleInteractor

#pragma mark - Методы SMEnterCodeModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)resendCode:(NSString *)phone {
    [self.output progressShow];
    [self.networkService resendCode:phone completed:^(BOOL status, NSString *message) {
        [self.output progressDismiss];
        if(status) {
            [self.output showMessage:@"Вам отправлен повторно код в СМС"];
        } else {
            [self.output showMessage:message];
        }
    }];
}

- (void)validateCode:(NSString *)code withPhone:(NSString *)phone {
    [self.output progressShow];
    [self.networkService validateCode:code withPhone:phone completed:^(BOOL status, NSString *token, profile *profile) {
        [self.output progressDismiss];
        if(status) {
            [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.output validateSuccess:profile];
        } else {
//            [MySingleton sharedMySingleton].profile = [profile new];
            [MySingleton sharedMySingleton].profile.phone = phone;
            [self.output validateError];
        }
    }];
}

@end
