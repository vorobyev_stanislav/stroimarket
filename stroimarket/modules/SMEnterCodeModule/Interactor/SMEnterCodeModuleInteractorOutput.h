//
//  SMEnterCodeModuleSMEnterCodeModuleInteractorOutput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "networkService.h"

@protocol SMEnterCodeModuleInteractorOutput <NSObject>

- (void)progressShow;
- (void)progressDismiss;
- (void)validateSuccess:(profile *)profile;
- (void)validateError;
- (void)showMessage:(NSString *)message;

@end
