//
//  SMEnterCodeModuleSMEnterCodeModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterCodeModuleInteractorInput.h"
#import "networkService.h"

@protocol SMEnterCodeModuleInteractorOutput;

@interface SMEnterCodeModuleInteractor : NSObject <SMEnterCodeModuleInteractorInput>

@property (nonatomic, weak) id<SMEnterCodeModuleInteractorOutput> output;
@property (nonatomic, strong) networkService *networkService;

@end
