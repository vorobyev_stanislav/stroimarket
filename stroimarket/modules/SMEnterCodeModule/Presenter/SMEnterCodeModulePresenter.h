//
//  SMEnterCodeModuleSMEnterCodeModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterCodeModuleViewOutput.h"
#import "SMEnterCodeModuleInteractorOutput.h"
#import "SMEnterCodeModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMEnterCodeModuleViewInput;
@protocol SMEnterCodeModuleInteractorInput;
@protocol SMEnterCodeModuleRouterInput;

@interface SMEnterCodeModulePresenter : NSObject <SMEnterCodeModuleModuleInput, SMEnterCodeModuleViewOutput, SMEnterCodeModuleInteractorOutput>

@property (nonatomic, weak) id<SMEnterCodeModuleViewInput> view;
@property (nonatomic, strong) id<SMEnterCodeModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMEnterCodeModuleRouterInput> router;

@property NSString *phone;

@end
