//
//  SMEnterCodeModuleSMEnterCodeModulePresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterCodeModulePresenter.h"

#import "SMEnterCodeModuleViewInput.h"
#import "SMEnterCodeModuleInteractorInput.h"
#import "SMEnterCodeModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMEnterCodeModulePresenter

#pragma mark - Методы SMEnterCodeModuleModuleInput

- (void)configureModule:(NSDictionary *)params {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
    self.phone = [params valueForKey:@"phone"];
}

#pragma mark - Методы SMEnterCodeModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMEnterCodeModuleViewOutput

- (void)resendCode {
    [self.interactor resendCode:self.phone];
}

- (void)sendCode:(NSString *)code {
    [self.interactor validateCode:code withPhone:self.phone];
}

- (void) viewWillApear {
    [self.view setPhoneLabel:self.phone];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMEnterCodeModuleInteractorOutput

- (void)showMessage:(NSString *)message {
    [self.view showMessage:message];
}

- (void)validateSuccess:(profile *)profile {
    if(profile == nil) {
        [MySingleton sharedMySingleton].profile.phone = self.phone;
        [self.router openEnterErrorModuleWithParams:@{}];
    } else {
        [self.router openMainModuleWithParams:@{}];
    }
}

- (void)validateError {
    [self.view validateError];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
