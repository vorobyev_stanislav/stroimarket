//
//  SMEnterCodeModuleSMEnterCodeModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMEnterCodeModuleModel.h"

@protocol SMEnterCodeModuleCellInput <NSObject>

@end

@interface SMEnterCodeModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMEnterCodeModuleModel *model;
@property (nonatomic, strong) id<SMEnterCodeModuleCellInput> cell;
@end
