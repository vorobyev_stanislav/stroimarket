//
//  SMEnterCodeModuleSMEnterCodeModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMEnterCodeModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMEnterCodeModuleViewOutput;

@interface SMEnterCodeModuleViewController : UIViewController <SMEnterCodeModuleViewInput>

@property (nonatomic, strong) id<SMEnterCodeModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *code;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *send;
@property (weak, nonatomic) IBOutlet UIButton *resend;

@end
