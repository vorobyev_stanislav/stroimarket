//
//  SMEnterCodeModuleSMEnterCodeModuleViewController.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterCodeModuleViewController.h"
#import "Functions.h"
#import "SMEnterCodeModuleTableViewCell.h"
#import "SMEnterCodeModuleViewOutput.h"

@implementation SMEnterCodeModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Вход" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:nil];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    [self.code mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.top.equalTo(self.view).with.offset(35*[Functions koefX]);
        make.centerX.equalTo(self.view);
    }];
    
    [self.border mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(1);
        make.top.equalTo(self.code.mas_bottom).with.offset(0);
        make.centerX.equalTo(self.view);
    }];
    
    [self.label mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.code.mas_bottom).with.offset(10*[Functions koefX]);
    }];
    
    [self.send mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.top.equalTo(self.label.mas_bottom).with.offset(30*[Functions koefX]);
        make.centerX.equalTo(self.view);
    }];
    
    [self.resend mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.top.equalTo(self.send.mas_bottom).with.offset(20*[Functions koefX]);
        make.centerX.equalTo(self.view);
    }];
    
    [super updateViewConstraints];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.navigationController popViewControllerAnimated:NO];
//    [self.output backBtnTap];
}

-(void)moreBtnTap {
    
}

- (void)enterCode {
    if([self.code.phoneNumber length] == 4) {
        [self.output sendCode:self.code.phoneNumber];
    } else {
        [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Введите код смс!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
    }
}

- (void)resendCode {
    NSLog(@"resendCode");
    [self.output resendCode];
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.code = [UIFabric shsPhoneTextField:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto-Regular" andFontSize:19 andSuperView:self.view];
    [self.code.formatter setDefaultOutputPattern:@"####"];
    self.code.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Введите код" attributes:@{NSForegroundColorAttributeName:[colorScheme colorWithSchemeName:@"placeholderColor"]}];
    
    self.border = [UIFabric viewWithBackgroundColor:[UIColor colorWithRed:218.0f/255.0f green:218.0f/255.0f blue:218.0f/255.0f alpha:1.0f] andSuperView:self.view];
    
    self.label = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.view];
    self.label.numberOfLines = 0;
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.alpha = 0.87f;
    
    self.send = [UIFabric buttonWithText:@"ДАЛЕЕ" andTextColor:[colorScheme colorWithSchemeName:@"backgroundCellColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    self.send.layer.cornerRadius = 2;
    [self.send addTarget:self action:@selector(enterCode) forControlEvents:UIControlEventTouchUpInside];
    
    self.resend = [UIFabric buttonWithText:@"Выслать код повторно" andTextColor:[colorScheme colorWithSchemeName:@"yellowColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andBackgroundColor:[UIColor clearColor] andImage:nil andSuperView:self.view];
    [self.resend addTarget:self action:@selector(resendCode) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Методы SMEnterCodeModuleViewInput

- (void)showMessage:(NSString *)message {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:message cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

- (void)setPhoneLabel:(NSString *)phone {
    
    UIFont *arialFont = [UIFont fontWithName:@"Roboto-Regular" size:16.0*[Functions koefX]];
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: arialFont forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:@"Введите код отправленный на номер\n " attributes: arialDict];
    
    UIFont *VerdanaFont = [UIFont fontWithName:@"Roboto-Medium" size:16.0*[Functions koefX]];
    NSDictionary *verdanaDict = [NSDictionary dictionaryWithObject:VerdanaFont forKey:NSFontAttributeName];
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString:phone attributes:verdanaDict];
    
    [aAttrString appendAttributedString:vAttrString];
    
    
    self.label.attributedText = aAttrString;
}

- (void)validateError {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Ошибка при проверке кода смс!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
