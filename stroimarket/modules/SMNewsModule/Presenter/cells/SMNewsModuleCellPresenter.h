//
//  SMNewsModuleSMNewsModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "cellModel.h"

@protocol SMNewsModuleCellInput <NSObject>

@end

@interface SMNewsModuleCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) newsModel *model;
@property NSInteger type;
@property (nonatomic, strong) id<SMNewsModuleCellInput> cell;

@end

@interface SMNewsModuleStockCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) stockModel *model;
@property NSInteger type;
@property (nonatomic, strong) id<SMNewsModuleCellInput> cell;

@end
