//
//  SMNewsModuleSMNewsModulePresenter.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsModuleViewOutput.h"
#import "SMNewsModuleInteractorOutput.h"
#import "SMNewsModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMNewsModuleViewInput;
@protocol SMNewsModuleInteractorInput;
@protocol SMNewsModuleRouterInput;

@interface SMNewsModulePresenter : NSObject <SMNewsModuleModuleInput, SMNewsModuleViewOutput, SMNewsModuleInteractorOutput>

@property (nonatomic, weak) id<SMNewsModuleViewInput> view;
@property (nonatomic, strong) id<SMNewsModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMNewsModuleRouterInput> router;
@property NSString *title;

@end
