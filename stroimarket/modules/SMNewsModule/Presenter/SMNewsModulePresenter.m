//
//  SMNewsModuleSMNewsModulePresenter.m
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsModulePresenter.h"

#import "SMNewsModuleViewInput.h"
#import "SMNewsModuleInteractorInput.h"
#import "SMNewsModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMNewsModulePresenter

#pragma mark - Методы SMNewsModuleModuleInput

- (void)configureModule:(NSDictionary *)params {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
    self.title = [params valueForKey:@"title"];
}

#pragma mark - Методы SMNewsModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMNewsModuleViewOutput

- (void) viewWillApear {
    [self.view setTitle:self.title];
    if([self.title isEqualToString:@"Акции"]) {
        [self.interactor getStocks];
    } else if([self.title isEqualToString:@"Новости"]) {
        [self.interactor getNews];
    }
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMNewsModuleInteractorOutput

- (void)openNewsDetail:(id)news {
    [self.router openNewsDetailModuleWithParams:@{@"title" : self.title, @"news" : news}];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
