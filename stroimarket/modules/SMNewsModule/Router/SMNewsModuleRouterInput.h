//
//  SMNewsModuleSMNewsModuleRouterInput.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMNewsModuleRouterInput <NSObject>

- (void)goBack;
- (void)openNewsDetailModuleWithParams:(NSDictionary *)params;

@end
