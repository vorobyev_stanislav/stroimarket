//
//  SMNewsModuleSMNewsModuleRouter.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMNewsModuleRouter : NSObject <SMNewsModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
