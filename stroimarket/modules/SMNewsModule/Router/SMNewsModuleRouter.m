//
//  SMNewsModuleSMNewsModuleRouter.m
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsModuleRouter.h"
#import "SMNewsDetailModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMNewsModuleRouter

#pragma mark - Методы SMNewsModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openNewsDetailModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to news detail"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMNewsDetailModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

@end
