//
//  SMNewsModuleSMNewsModuleInteractorInput.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;
@protocol SMNewsModuleInteractorInput <NSObject>

- (void)setTableViewManager:(RETableViewManager*)manager;
- (void)getNews;
- (void)getStocks;

@end
