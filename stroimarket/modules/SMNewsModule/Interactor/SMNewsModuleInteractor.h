//
//  SMNewsModuleSMNewsModuleInteractor.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsModuleInteractorInput.h"
#import "networkService.h"

@protocol SMNewsModuleInteractorOutput;

@interface SMNewsModuleInteractor : NSObject <SMNewsModuleInteractorInput>

@property (nonatomic, weak) id<SMNewsModuleInteractorOutput> output;
@property (nonatomic, strong) networkService *networkService;

@end
