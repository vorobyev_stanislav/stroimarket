//
//  SMNewsModuleSMNewsModuleInteractor.m
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsModuleInteractor.h"

#import "SMNewsModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMNewsModuleCellPresenter.h"

@interface SMNewsModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMNewsModuleInteractor

#pragma mark - Методы SMNewsModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)getNews {
    
    [self.output progressShow];
    [self.networkService getNews:^(NSArray *news) {
        [self.manager.sections[0] removeAllItems];
        
        for(int i=0;i<news.count;i++) {
            SMNewsModuleCellPresenter *pres2 = [SMNewsModuleCellPresenter item];
            pres2.model = news[i];
            pres2.selectionHandler = ^(SMNewsModuleCellPresenter *newValue){
                [newValue deselectRowAnimated:YES];
                [self.output openNewsDetail:newValue.model];
            };
            [self.manager.sections[0] addItem:(id)pres2];
        }
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
        [self.output progressDismiss];
    }];
}

- (void)getStocks {
    
    [self.output progressShow];
    [self.networkService getStock:^(NSArray *news) {
        [self.manager.sections[0] removeAllItems];
        
        for(int i=0;i<news.count;i++) {
            SMNewsModuleStockCellPresenter *pres2 = [SMNewsModuleStockCellPresenter item];
            pres2.model = news[i];
            pres2.selectionHandler = ^(SMNewsModuleStockCellPresenter *newValue){
                [newValue deselectRowAnimated:YES];
                [self.output openNewsDetail:newValue.model];
            };
            [self.manager.sections[0] addItem:(id)pres2];
        }
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
        [self.output progressDismiss];
    }];
}

@end
