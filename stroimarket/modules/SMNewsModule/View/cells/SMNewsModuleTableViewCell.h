//
//  SMNewsModuleSMNewsModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMNewsModuleCellPresenter.h"

@interface SMNewsModuleTableViewCell : RETableViewCell <SMNewsModuleCellInput>

@property (strong, readwrite, nonatomic) SMNewsModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *date;

@end
