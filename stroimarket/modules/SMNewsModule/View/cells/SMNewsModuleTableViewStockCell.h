//
//  SMNewsModuleTableViewStockCell.h
//  stroimarket
//
//  Created by apple on 08.06.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMNewsModuleCellPresenter.h"

@interface SMNewsModuleTableViewStockCell : RETableViewCell <SMNewsModuleCellInput>

@property (strong, readwrite, nonatomic) SMNewsModuleStockCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *date;

@end
