//
//  SMNewsModuleTableViewStockCell.m
//  stroimarket
//
//  Created by apple on 08.06.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMNewsModuleTableViewStockCell.h"

@implementation SMNewsModuleTableViewStockCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 110*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    //self.currencyLab.text = [self.item.model.currency uppercaseString];
    
    self.name.text = self.item.model.name;
    
    self.date.text = [NSString stringWithFormat:@"%@ - %@", self.item.model.date_active_from, self.item.model.date_active_to];
    
    if(self.item.model.image.length) {
        self.photo.contentMode = UIViewContentModeScaleAspectFit;
    } else {
        self.photo.contentMode = UIViewContentModeCenter;
    }
    
    [self.photo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", domain, self.item.model.image]] placeholderImage:[UIImage imageNamed:@"ic_empty"] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageUrl) {
        
    }];
    
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMNewsModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.backView = [UIFabric viewWithBackgroundColor:[UIColor whiteColor] andSuperView:self];
    self.backView.layer.cornerRadius = 2;
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(10*[Functions koefX]);
        make.right.equalTo(self).with.offset(-10*[Functions koefX]);
        make.bottom.equalTo(self).with.offset(-5*[Functions koefX]);
        make.top.equalTo(self).with.offset(5*[Functions koefX]);
    }];
    
    self.photo = [UIFabric imageViewWithImageName:@"ic_empty" andContentMode:UIViewContentModeCenter iconMode:false andSuperView:self.backView];
    self.photo.clipsToBounds = YES;
    
    [self.photo mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(95*[Functions koefX]);
        make.bottom.equalTo(self.backView).with.offset(0);
        make.left.equalTo(self.backView).with.offset(0);
        make.top.equalTo(self.backView).with.offset(0);
    }];
    
    self.name = [UIFabric labelWithText:@"Наименование акции" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:19 andSuperView:self.backView];
    self.name.alpha = 0.87f;
    self.name.numberOfLines = 2;
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(120*[Functions koefX]);
        make.right.equalTo(self.backView).with.offset(-15*[Functions koefX]);
        make.top.equalTo(self.backView).with.offset(20*[Functions koefX]);
    }];
    
    self.date = [UIFabric labelWithText:@"Акция действует до 01.01.2018" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:13 andSuperView:self.backView];
    self.date.alpha = 0.54f;
    
    [self.date mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView).with.offset(-8*[Functions koefX]);
        make.bottom.equalTo(self.backView).with.offset(-8*[Functions koefX]);
    }];
}

@end
