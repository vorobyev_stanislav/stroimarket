//
//  SMNewsModuleSMNewsModuleViewInput.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMNewsModuleViewInput <NSObject>

/**
 @author apple

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;
- (void)progressShow;
- (void)progressDismiss;
- (void)setTitle:(NSString *)title;

@end
