//
//  SMNewsModuleSMNewsModuleViewController.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMNewsModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMNewsModuleViewOutput;

@interface SMNewsModuleViewController : UIViewController <SMNewsModuleViewInput>

@property (nonatomic, strong) id<SMNewsModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;

@end
