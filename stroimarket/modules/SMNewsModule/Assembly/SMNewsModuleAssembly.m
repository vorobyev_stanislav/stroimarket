//
//  SMNewsModuleSMNewsModuleAssembly.m
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsModuleAssembly.h"
#import "networkService.h"
#import "SMNewsModuleViewController.h"
#import "SMNewsModuleInteractor.h"
#import "SMNewsModulePresenter.h"
#import "SMNewsModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMNewsModuleAssembly

- (SMNewsModuleViewController *)viewSMNewsModuleModule {
    return [TyphoonDefinition withClass:[SMNewsModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMNewsModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMNewsModuleModule]];
                          }];
}

- (SMNewsModuleInteractor *)interactorSMNewsModuleModule {
    return [TyphoonDefinition withClass:[SMNewsModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMNewsModuleModule]];
                              [definition injectProperty:@selector(networkService)
                                                    with:[self networkServiceSMNewsModule]];
                          }];
}

- (SMNewsModulePresenter *)presenterSMNewsModuleModule {
    return [TyphoonDefinition withClass:[SMNewsModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMNewsModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMNewsModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMNewsModuleModule]];

                          }];
}

- (SMNewsModuleRouter *)routerSMNewsModuleModule {
    return [TyphoonDefinition withClass:[SMNewsModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMNewsModuleModule]];
                          }];
}

- (networkService *)networkServiceSMNewsModule {
    return [TyphoonDefinition withClass:[networkService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
