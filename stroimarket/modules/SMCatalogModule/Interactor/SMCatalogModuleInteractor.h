//
//  SMCatalogModuleSMCatalogModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogModuleInteractorInput.h"
#import "catalogService.h"

@protocol SMCatalogModuleInteractorOutput;

@interface SMCatalogModuleInteractor : NSObject <SMCatalogModuleInteractorInput>

@property (nonatomic, weak) id<SMCatalogModuleInteractorOutput> output;
@property (nonatomic, strong) catalogService *catalogService;

@end
