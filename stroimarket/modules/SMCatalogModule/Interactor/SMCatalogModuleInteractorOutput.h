//
//  SMCatalogModuleSMCatalogModuleInteractorOutput.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMCatalogModuleInteractorOutput <NSObject>

- (void)progressShow;
- (void)progressDismiss;
- (void)openCatalogDetail:(NSInteger)catalogId;

@end
