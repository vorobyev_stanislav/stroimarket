//
//  SMCatalogModuleSMCatalogModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogModuleInteractor.h"

#import "SMCatalogModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMCatalogModuleCellPresenter.h"

@interface SMCatalogModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMCatalogModuleInteractor

#pragma mark - Методы SMCatalogModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)getCatalogs {
    
    [self.catalogService getCatalogFromDB:0 completed:^(NSArray<catalogModel *> *catalogs2) {
        if(!catalogs2.count) {
            [self.output progressShow];
        }
        
        [self.manager.sections[0] removeAllItems];
        
        for(int i=0;i<catalogs2.count;i++) {
            SMCatalogModuleCellPresenter *pres1 = [SMCatalogModuleCellPresenter item];
            pres1.model = catalogs2[i];
            pres1.selectionHandler = ^(SMCatalogModuleCellPresenter *newValue){
                [newValue deselectRowAnimated:YES];
                [self.output openCatalogDetail:newValue.model.catalogId];
            };
            [self.manager.sections[0] addItem:(id)pres1];
        }
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
        
        [self.catalogService getCatalogs:^(NSArray<catalogModel *> *catalogs) {
            [self.manager.sections[0] removeAllItems];
            
            for(int i=0;i<catalogs.count;i++) {
                SMCatalogModuleCellPresenter *pres1 = [SMCatalogModuleCellPresenter item];
                pres1.model = catalogs[i];
                pres1.selectionHandler = ^(SMCatalogModuleCellPresenter *newValue){
                    [newValue deselectRowAnimated:YES];
                    [self.output openCatalogDetail:newValue.model.catalogId];
                };
                [self.manager.sections[0] addItem:(id)pres1];
            }
            
            [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
            [self.output progressDismiss];
        }];
    }];
}

@end
