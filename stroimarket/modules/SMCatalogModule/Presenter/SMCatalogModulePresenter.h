//
//  SMCatalogModuleSMCatalogModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogModuleViewOutput.h"
#import "SMCatalogModuleInteractorOutput.h"
#import "SMCatalogModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMCatalogModuleViewInput;
@protocol SMCatalogModuleInteractorInput;
@protocol SMCatalogModuleRouterInput;

@interface SMCatalogModulePresenter : NSObject <SMCatalogModuleModuleInput, SMCatalogModuleViewOutput, SMCatalogModuleInteractorOutput>

@property (nonatomic, weak) id<SMCatalogModuleViewInput> view;
@property (nonatomic, strong) id<SMCatalogModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMCatalogModuleRouterInput> router;

@end
