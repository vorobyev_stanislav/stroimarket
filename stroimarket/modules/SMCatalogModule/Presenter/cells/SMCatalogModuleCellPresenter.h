//
//  SMCatalogModuleSMCatalogModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "catalogModel.h"

@protocol SMCatalogModuleCellInput <NSObject>

@end

@interface SMCatalogModuleCellPresenter : RETableViewItem
@property (strong, readwrite, nonatomic) catalogModel *model;
@property (nonatomic, strong) id<SMCatalogModuleCellInput> cell;
@end
