//
//  SMCatalogModuleSMCatalogModulePresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogModulePresenter.h"

#import "SMCatalogModuleViewInput.h"
#import "SMCatalogModuleInteractorInput.h"
#import "SMCatalogModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMCatalogModulePresenter

#pragma mark - Методы SMCatalogModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMCatalogModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMCatalogModuleViewOutput

- (void) viewWillApear {
    if(![MySingleton sharedMySingleton].catalogReload) {
        [self.interactor getCatalogs];
        [MySingleton sharedMySingleton].catalogReload = 1;
    }
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMCatalogModuleInteractorOutput

- (void)openCatalogDetail:(NSInteger)catalogId {
    [self.router openSubCatalog1ModuleWithParams:@{@"catalogId" : @(catalogId)}];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
