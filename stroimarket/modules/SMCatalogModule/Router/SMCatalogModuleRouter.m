//
//  SMCatalogModuleSMCatalogModuleRouter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogModuleRouter.h"
#import "SMCatalogDetailModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"
#import "SMSubCatalog1ModuleModuleInput.h"

@implementation SMCatalogModuleRouter

#pragma mark - Методы SMCatalogModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openSubCatalog1ModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to sub1"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMSubCatalog1ModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

@end
