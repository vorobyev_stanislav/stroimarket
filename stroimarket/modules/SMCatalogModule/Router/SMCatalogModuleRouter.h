//
//  SMCatalogModuleSMCatalogModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMCatalogModuleRouter : NSObject <SMCatalogModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
