//
//  SMCatalogModuleSMCatalogModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogModuleAssembly.h"
#import "catalogService.h"
#import "SMCatalogModuleViewController.h"
#import "SMCatalogModuleInteractor.h"
#import "SMCatalogModulePresenter.h"
#import "SMCatalogModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMCatalogModuleAssembly

- (SMCatalogModuleViewController *)viewSMCatalogModuleModule {
    return [TyphoonDefinition withClass:[SMCatalogModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMCatalogModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMCatalogModuleModule]];
                          }];
}

- (SMCatalogModuleInteractor *)interactorSMCatalogModuleModule {
    return [TyphoonDefinition withClass:[SMCatalogModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMCatalogModuleModule]];
                              [definition injectProperty:@selector(catalogService)
                                                    with:[self catalogServiceSMCatalogModule]];
                          }];
}

- (SMCatalogModulePresenter *)presenterSMCatalogModuleModule {
    return [TyphoonDefinition withClass:[SMCatalogModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMCatalogModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMCatalogModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMCatalogModuleModule]];

                          }];
}

- (SMCatalogModuleRouter *)routerSMCatalogModuleModule {
    return [TyphoonDefinition withClass:[SMCatalogModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMCatalogModuleModule]];
                          }];
}

- (catalogService *)catalogServiceSMCatalogModule {
    return [TyphoonDefinition withClass:[catalogService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
