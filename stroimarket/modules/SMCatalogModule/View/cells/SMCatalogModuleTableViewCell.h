//
//  SMCatalogModuleSMCatalogModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMCatalogModuleCellPresenter.h"

@interface SMCatalogModuleTableViewCell : RETableViewCell <SMCatalogModuleCellInput>

@property (strong, readwrite, nonatomic) SMCatalogModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;

@end
