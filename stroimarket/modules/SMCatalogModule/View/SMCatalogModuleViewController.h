//
//  SMCatalogModuleSMCatalogModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMCatalogModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMCatalogModuleViewOutput;

@interface SMCatalogModuleViewController : UIViewController <SMCatalogModuleViewInput>

@property (nonatomic, strong) id<SMCatalogModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;

@end
