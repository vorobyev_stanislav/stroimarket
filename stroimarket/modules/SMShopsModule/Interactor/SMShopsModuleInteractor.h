//
//  SMShopsModuleSMShopsModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMShopsModuleInteractorInput.h"
#import "shopService.h"

@protocol SMShopsModuleInteractorOutput;

@interface SMShopsModuleInteractor : NSObject <SMShopsModuleInteractorInput>

@property (nonatomic, weak) id<SMShopsModuleInteractorOutput> output;
@property (nonatomic, strong) shopService *shopService;

@end
