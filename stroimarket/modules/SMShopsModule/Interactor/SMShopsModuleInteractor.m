//
//  SMShopsModuleSMShopsModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMShopsModuleInteractor.h"

#import "SMShopsModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMShopsModuleCellPresenter.h"

@interface SMShopsModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMShopsModuleInteractor

#pragma mark - Методы SMShopsModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)getShops {
    
    [self.shopService getShops:^(NSArray<shopModel *> *shops) {
        [self.manager.sections[0] removeAllItems];
        
        SMShopsModuleHeaderCellPresenter *pres = [SMShopsModuleHeaderCellPresenter item];
        [self.manager.sections[0] addItem:(id)pres];
        
        if(shops.count) {
            [self.output receiveShop:shops];
        }
        
        for(int i=0;i<shops.count;i++) {
            SMShopsModuleCellPresenter *pres2 = [SMShopsModuleCellPresenter item];
            pres2.model = shops[i];
            pres2.selectionHandler = ^(SMShopsModuleCellPresenter *newValue){
                [newValue deselectRowAnimated:YES];
            };
            [self.manager.sections[0] addItem:(id)pres2];
        }
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
    }];
}

@end
