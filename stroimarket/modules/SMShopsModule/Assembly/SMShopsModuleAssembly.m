//
//  SMShopsModuleSMShopsModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMShopsModuleAssembly.h"

#import "SMShopsModuleViewController.h"
#import "SMShopsModuleInteractor.h"
#import "SMShopsModulePresenter.h"
#import "SMShopsModuleRouter.h"
#import "shopService.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMShopsModuleAssembly

- (SMShopsModuleViewController *)viewSMShopsModuleModule {
    return [TyphoonDefinition withClass:[SMShopsModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMShopsModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMShopsModuleModule]];
                          }];
}

- (SMShopsModuleInteractor *)interactorSMShopsModuleModule {
    return [TyphoonDefinition withClass:[SMShopsModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMShopsModuleModule]];
                              [definition injectProperty:@selector(shopService)
                                                    with:[self shopServiceSMShopsModule]];
                          }];
}

- (SMShopsModulePresenter *)presenterSMShopsModuleModule {
    return [TyphoonDefinition withClass:[SMShopsModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMShopsModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMShopsModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMShopsModuleModule]];

                          }];
}

- (SMShopsModuleRouter *)routerSMShopsModuleModule {
    return [TyphoonDefinition withClass:[SMShopsModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMShopsModuleModule]];
                          }];
}

- (shopService *)shopServiceSMShopsModule {
    return [TyphoonDefinition withClass:[shopService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
