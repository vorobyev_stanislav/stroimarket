//
//  SMShopsModuleSMShopsModuleRouter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMShopsModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMShopsModuleRouter

#pragma mark - Методы SMShopsModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

@end
