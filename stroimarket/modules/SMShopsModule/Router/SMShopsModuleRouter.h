//
//  SMShopsModuleSMShopsModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMShopsModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMShopsModuleRouter : NSObject <SMShopsModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
