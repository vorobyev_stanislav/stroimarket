//
//  SMShopsModuleSMShopsModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMShopsModuleCellPresenter.h"

@interface SMShopsModuleTableViewCell : RETableViewCell <SMShopsModuleCellInput>

@property (strong, readwrite, nonatomic) SMShopsModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *geotag;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *phone;

@end
