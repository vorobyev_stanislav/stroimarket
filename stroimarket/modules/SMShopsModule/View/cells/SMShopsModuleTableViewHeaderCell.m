//
//  SMShopsModuleTableViewHeaderCell.m
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMShopsModuleTableViewHeaderCell.h"

@implementation SMShopsModuleTableViewHeaderCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 60*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMShopsModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.header = [UIFabric labelWithText:@"Пункты выдачи товара" andTextColor:[colorScheme colorWithSchemeName:@"labelTextColor1"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self];
    
    [self.header mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(15*[Functions koefX]);
        make.centerY.equalTo(self).with.offset(0);
    }];
}
@end
