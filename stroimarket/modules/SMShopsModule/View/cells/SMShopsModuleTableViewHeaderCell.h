//
//  SMShopsModuleTableViewHeaderCell.h
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMShopsModuleCellPresenter.h"

@interface SMShopsModuleTableViewHeaderCell : RETableViewCell <SMShopsModuleCellInput>

@property (strong, readwrite, nonatomic) SMShopsModuleHeaderCellPresenter *item;

@property (weak, nonatomic) IBOutlet UILabel *header;

@end
