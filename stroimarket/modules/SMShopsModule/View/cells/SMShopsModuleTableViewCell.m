//
//  SMShopsModuleSMShopsModuleTableViewCell.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMShopsModuleTableViewCell.h"
#import "Functions.h"

@implementation SMShopsModuleTableViewCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 60*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.address.text = self.item.model.address;
    self.phone.text = self.item.model.phone;
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMShopsModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.geotag = [UIFabric imageViewWithImageName:@"ic_geotag" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];

    [self.geotag mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(15*[Functions koefX]);
        make.top.equalTo(self).with.offset(5*[Functions koefX]);
    }];
    
    self.address = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"labelTextColor1"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self];
    
    [self.address mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.geotag.mas_right).with.offset(5);
        make.centerY.equalTo(self.geotag).with.offset(0);
    }];
    
    self.phone = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"labelTextColor1"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self];
    
    [self.phone mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.geotag.mas_right).with.offset(5);
        make.top.equalTo(self.address.mas_bottom).with.offset(5);
    }];
}

@end
