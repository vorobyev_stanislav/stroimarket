//
//  SMShopsModuleSMShopsModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMShopsModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMShopsModuleViewOutput;

@interface SMShopsModuleViewController : UIViewController <SMShopsModuleViewInput>

@property (nonatomic, strong) id<SMShopsModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIButton *listButton;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;
@property (weak, nonatomic) IBOutlet UIView *indicator;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;

@end
