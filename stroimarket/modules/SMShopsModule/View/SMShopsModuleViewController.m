//
//  SMShopsModuleSMShopsModuleViewController.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMShopsModuleViewController.h"
#import "Functions.h"
#import "SMShopsModuleTableViewCell.h"
#import "SMShopsModuleViewOutput.h"
#import "shopModel.h"

@implementation SMShopsModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Адреса магазинов" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [colorScheme colorWithSchemeName:@"navigationBarColor"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.backgroundColor = [colorScheme colorWithSchemeName:@"navigationBarColor"];
    
    [self.output viewWillApear];   
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)updateViewConstraints {
    [self.listButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.view).with.offset(0);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(self.view.frame.size.width/2);
    }];
    
    [self.mapButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.view).with.offset(0);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(self.view.frame.size.width/2);
    }];
    
    [self.mapView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.bottom.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.listButton.mas_bottom).with.offset(0);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.bottom.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.listButton.mas_bottom).with.offset(0);
    }];
    
    [self.indicator mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.listButton).with.offset(0);
        make.right.equalTo(self.listButton).with.offset(0);
        make.bottom.equalTo(self.listButton).with.offset(0);
        make.height.mas_equalTo(3);
    }];
    
    [super updateViewConstraints];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (IBAction)action:(id)sender {
    [[SlideNavigationController sharedInstance] openMenu:1 withCompletion:nil];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

- (void)showList {
    self.tableView.hidden = NO;
    self.mapView.hidden = YES;
    
    CGRect newFrame = self.indicator.frame;
    newFrame.origin.x = 0;
    [UIView animateWithDuration:0.2
                          delay:0
                        options:0
                     animations:^{
                         self.indicator.frame = newFrame;
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

- (void)showMap {
    self.tableView.hidden = YES;
    self.mapView.hidden = NO;
    
    CGRect newFrame = self.indicator.frame;
    newFrame.origin.x = self.view.frame.size.width/2;
    [UIView animateWithDuration:0.2
                          delay:0
                        options:0
                     animations:^{
                         self.indicator.frame = newFrame;
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.listButton = [UIFabric buttonWithText:@"СПИСОК" andTextColor:[UIColor whiteColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:19 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    [self.listButton addTarget:self action:@selector(showList) forControlEvents:UIControlEventTouchUpInside];
    
    self.mapButton = [UIFabric buttonWithText:@"КАРТА" andTextColor:[UIColor whiteColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:19 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    [self.mapButton addTarget:self action:@selector(showMap) forControlEvents:UIControlEventTouchUpInside];
    
    self.indicator = [UIFabric viewWithBackgroundColor:[UIColor whiteColor] andSuperView:self.view];
    
    self.mapView = [UIFabric mapView:self.view];
    self.mapView.hidden = YES;
    
    UITableView *tv = [[UITableView alloc] init];
    self.tableView = tv;
    [self.view addSubview:tv];
    self.tableView.bounces = NO;
    tv.backgroundColor = [UIColor whiteColor];
    tv.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableManager = [[RETableViewManager alloc] initWithTableView:self.tableView];
    RETableViewSection *dumbSection = [RETableViewSection sectionWithHeaderTitle:@""];
    [self.tableManager addSection:dumbSection];
    self.tableManager[@"SMShopsModuleCellPresenter"] = @"SMShopsModuleTableViewCell";
    self.tableManager[@"SMShopsModuleHeaderCellPresenter"] = @"SMShopsModuleTableViewHeaderCell";
    [self.output setTableViewManager:self.tableManager];
}
#pragma mark - Методы SMShopsModuleViewInput

- (void)receiveShop:(NSArray *)shops {
    [self.mapView clear];
    
    for(int i=0;i<shops.count;i++) {
        shopModel *shop = shops[i];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(shop.latitude, shop.longitude);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.icon = [UIImage imageNamed:@"ic_geotag_red"];
        marker.map = self.mapView;
    }
    
    if(shops.count) {
        shopModel *shop = shops[0];
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:shop.latitude
                                                                longitude:shop.longitude
                                                                     zoom:6];
        [self.mapView setCamera:camera];
    }
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
