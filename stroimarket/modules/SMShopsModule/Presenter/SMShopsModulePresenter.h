//
//  SMShopsModuleSMShopsModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMShopsModuleViewOutput.h"
#import "SMShopsModuleInteractorOutput.h"
#import "SMShopsModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMShopsModuleViewInput;
@protocol SMShopsModuleInteractorInput;
@protocol SMShopsModuleRouterInput;

@interface SMShopsModulePresenter : NSObject <SMShopsModuleModuleInput, SMShopsModuleViewOutput, SMShopsModuleInteractorOutput>

@property (nonatomic, weak) id<SMShopsModuleViewInput> view;
@property (nonatomic, strong) id<SMShopsModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMShopsModuleRouterInput> router;

@end
