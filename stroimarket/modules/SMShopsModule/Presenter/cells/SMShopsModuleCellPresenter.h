//
//  SMShopsModuleSMShopsModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "shopModel.h"

@protocol SMShopsModuleCellInput <NSObject>

@end

@interface SMShopsModuleHeaderCellPresenter : RETableViewItem

//@property (strong, readwrite, nonatomic) SMShopsModuleModel *model;
@property (nonatomic, strong) id<SMShopsModuleCellInput> cell;

@end

@interface SMShopsModuleCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) shopModel *model;
@property (nonatomic, strong) id<SMShopsModuleCellInput> cell;

@end
