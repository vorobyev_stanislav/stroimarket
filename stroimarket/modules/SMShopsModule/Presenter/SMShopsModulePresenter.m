//
//  SMShopsModuleSMShopsModulePresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMShopsModulePresenter.h"

#import "SMShopsModuleViewInput.h"
#import "SMShopsModuleInteractorInput.h"
#import "SMShopsModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMShopsModulePresenter

#pragma mark - Методы SMShopsModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMShopsModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMShopsModuleViewOutput

- (void) viewWillApear {
    [self.interactor getShops];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMShopsModuleInteractorOutput

- (void)receiveShop:(NSArray *)shops {
    [self.view receiveShop:shops];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
