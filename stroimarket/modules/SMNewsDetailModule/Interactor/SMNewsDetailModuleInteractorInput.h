//
//  SMNewsDetailModuleSMNewsDetailModuleInteractorInput.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cellModel.h"

@class RETableViewManager;
@protocol SMNewsDetailModuleInteractorInput <NSObject>
- (void) setTableViewManager:(RETableViewManager*)manager;

@end
