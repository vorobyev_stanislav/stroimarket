//
//  SMNewsDetailModuleSMNewsDetailModuleInteractor.m
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsDetailModuleInteractor.h"

#import "SMNewsDetailModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMNewsDetailModuleCellPresenter.h"

@interface SMNewsDetailModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMNewsDetailModuleInteractor

#pragma mark - Методы SMNewsDetailModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

@end
