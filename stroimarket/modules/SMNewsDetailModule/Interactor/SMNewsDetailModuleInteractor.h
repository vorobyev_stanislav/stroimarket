//
//  SMNewsDetailModuleSMNewsDetailModuleInteractor.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsDetailModuleInteractorInput.h"


@protocol SMNewsDetailModuleInteractorOutput;

@interface SMNewsDetailModuleInteractor : NSObject <SMNewsDetailModuleInteractorInput>

@property (nonatomic, weak) id<SMNewsDetailModuleInteractorOutput> output;

@end
