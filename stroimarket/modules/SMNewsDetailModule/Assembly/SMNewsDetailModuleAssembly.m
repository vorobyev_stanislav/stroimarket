//
//  SMNewsDetailModuleSMNewsDetailModuleAssembly.m
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsDetailModuleAssembly.h"

#import "SMNewsDetailModuleViewController.h"
#import "SMNewsDetailModuleInteractor.h"
#import "SMNewsDetailModulePresenter.h"
#import "SMNewsDetailModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMNewsDetailModuleAssembly

- (SMNewsDetailModuleViewController *)viewSMNewsDetailModuleModule {
    return [TyphoonDefinition withClass:[SMNewsDetailModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMNewsDetailModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMNewsDetailModuleModule]];
                          }];
}

- (SMNewsDetailModuleInteractor *)interactorSMNewsDetailModuleModule {
    return [TyphoonDefinition withClass:[SMNewsDetailModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMNewsDetailModuleModule]];
                          }];
}

- (SMNewsDetailModulePresenter *)presenterSMNewsDetailModuleModule {
    return [TyphoonDefinition withClass:[SMNewsDetailModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMNewsDetailModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMNewsDetailModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMNewsDetailModuleModule]];

                          }];
}

- (SMNewsDetailModuleRouter *)routerSMNewsDetailModuleModule {
    return [TyphoonDefinition withClass:[SMNewsDetailModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMNewsDetailModuleModule]];
                          }];
}

@end
