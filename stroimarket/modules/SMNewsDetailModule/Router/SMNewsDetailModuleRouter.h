//
//  SMNewsDetailModuleSMNewsDetailModuleRouter.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsDetailModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMNewsDetailModuleRouter : NSObject <SMNewsDetailModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
