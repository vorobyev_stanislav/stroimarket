//
//  SMNewsDetailModuleSMNewsDetailModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMNewsDetailModuleModel.h"

@protocol SMNewsDetailModuleCellInput <NSObject>

@end

@interface SMNewsDetailModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMNewsDetailModuleModel *model;
@property (nonatomic, strong) id<SMNewsDetailModuleCellInput> cell;
@end
