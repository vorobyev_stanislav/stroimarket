//
//  SMNewsDetailModuleSMNewsDetailModulePresenter.m
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsDetailModulePresenter.h"

#import "SMNewsDetailModuleViewInput.h"
#import "SMNewsDetailModuleInteractorInput.h"
#import "SMNewsDetailModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMNewsDetailModulePresenter

#pragma mark - Методы SMNewsDetailModuleModuleInput

- (void)configureModule:(NSDictionary *)params {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
    self.title = [params valueForKey:@"title"];
    self.news = [params valueForKey:@"news"];
}

#pragma mark - Методы SMNewsDetailModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMNewsDetailModuleViewOutput

- (void) viewWillApear {
    [self.view setTitle:self.title];
    [self.view showNews:self.news];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMNewsDetailModuleInteractorOutput
-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
