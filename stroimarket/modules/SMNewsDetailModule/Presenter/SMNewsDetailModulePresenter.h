//
//  SMNewsDetailModuleSMNewsDetailModulePresenter.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsDetailModuleViewOutput.h"
#import "SMNewsDetailModuleInteractorOutput.h"
#import "SMNewsDetailModuleModuleInput.h"
#import "cellModel.h"
//#import "BasePresenter.h"

@protocol SMNewsDetailModuleViewInput;
@protocol SMNewsDetailModuleInteractorInput;
@protocol SMNewsDetailModuleRouterInput;

@interface SMNewsDetailModulePresenter : NSObject <SMNewsDetailModuleModuleInput, SMNewsDetailModuleViewOutput, SMNewsDetailModuleInteractorOutput>

@property (nonatomic, weak) id<SMNewsDetailModuleViewInput> view;
@property (nonatomic, strong) id<SMNewsDetailModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMNewsDetailModuleRouterInput> router;
@property NSString *title;
@property newsModel *news;

@end
