//
//  SMNewsDetailModuleSMNewsDetailModuleViewController.m
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsDetailModuleViewController.h"
#import "Functions.h"
#import "SMNewsDetailModuleTableViewCell.h"
#import "SMNewsDetailModuleViewOutput.h"

@implementation SMNewsDetailModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [Functions setupNavigationController:self.navigationController andTitle:WCLocalize(@"SMNewsDetailModuleTitle") andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    [self progressShow];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    [self.webView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [super updateViewConstraints];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.webView = [UIFabric webView:self andSuperView:self.view];
    self.webView.delegate = (id)self;
}

#pragma mark - UIWebView delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self progressDismiss];
    
}

#pragma mark - Методы SMNewsDetailModuleViewInput

- (void)setTitle:(NSString *)title {
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:title andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
}

- (void)showNews:(newsModel *)news {
    [self.webView loadHTMLString:[NSString stringWithFormat:@"<style>img{object-fit:cover;width:100%%;max-width:%.0fpx;}</style><base href='http://stroymarket-sk.ru'>%@", [UIScreen mainScreen].bounds.size.width - 40, news.text] baseURL:[NSURL URLWithString:@"stroymarket-sk.ru"]];
}

- (void)showStock:(stockModel *)stock {
    [self.webView loadHTMLString:[NSString stringWithFormat:@"<style>img{object-fit:cover;width:100%%;max-width:%.0fpx;}</style><base href='http://stroymarket-sk.ru'>%@", [UIScreen mainScreen].bounds.size.width - 40, stock.text] baseURL:[NSURL URLWithString:@"stroymarket-sk.ru"]];
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
