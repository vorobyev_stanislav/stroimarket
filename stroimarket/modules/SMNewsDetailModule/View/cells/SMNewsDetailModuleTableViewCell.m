//
//  SMNewsDetailModuleSMNewsDetailModuleTableViewCell.m
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNewsDetailModuleTableViewCell.h"
#import "Functions.h"

@implementation SMNewsDetailModuleTableViewCell
#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 44;
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    //self.currencyLab.text = [self.item.model.currency uppercaseString];
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMNewsDetailModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    self.background = [UIFabric imageViewWithImageName:@"backgound" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];

}
@end
