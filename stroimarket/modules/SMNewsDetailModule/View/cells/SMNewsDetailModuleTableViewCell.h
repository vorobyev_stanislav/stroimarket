//
//  SMNewsDetailModuleSMNewsDetailModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMNewsDetailModuleCellPresenter.h"

@interface SMNewsDetailModuleTableViewCell : RETableViewCell <SMNewsDetailModuleCellInput>

@property (strong, readwrite, nonatomic) SMNewsDetailModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
