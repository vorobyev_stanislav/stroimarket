//
//  SMNewsDetailModuleSMNewsDetailModuleViewController.h
//  stroimarket
//
//  Created by apple on 09/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMNewsDetailModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMNewsDetailModuleViewOutput;

@interface SMNewsDetailModuleViewController : UIViewController <SMNewsDetailModuleViewInput, UIWebViewDelegate>

@property (nonatomic, strong) id<SMNewsDetailModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
