//
//  SMCatalogDetailModuleSMCatalogDetailModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogDetailModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMCatalogDetailModuleRouter : NSObject <SMCatalogDetailModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
