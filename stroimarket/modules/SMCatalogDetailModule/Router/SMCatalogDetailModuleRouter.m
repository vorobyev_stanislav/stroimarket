//
//  SMCatalogDetailModuleSMCatalogDetailModuleRouter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogDetailModuleRouter.h"
#import "SMGoodsModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMCatalogDetailModuleRouter

#pragma mark - Методы SMCatalogDetailModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openGoodsModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to goods"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMGoodsModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

@end
