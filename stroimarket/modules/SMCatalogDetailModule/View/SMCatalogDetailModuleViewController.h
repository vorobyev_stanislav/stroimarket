//
//  SMCatalogDetailModuleSMCatalogDetailModuleViewController.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMCatalogDetailModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMCatalogDetailModuleViewOutput;

@interface SMCatalogDetailModuleViewController : UIViewController <SMCatalogDetailModuleViewInput>

@property (nonatomic, strong) id<SMCatalogDetailModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@property NSArray *goods;

@end
