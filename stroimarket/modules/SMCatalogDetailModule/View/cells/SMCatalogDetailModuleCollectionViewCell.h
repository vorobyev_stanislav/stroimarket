//
//  SMCatalogDetailModuleCollectionViewCell.h
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "catalogModel.h"

@interface SMCatalogDetailModuleCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UIButton *price;

- (void)setGoods:(goods *)goods;

@end
