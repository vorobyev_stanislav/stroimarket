//
//  SMCatalogDetailModuleCollectionViewCell.m
//  stroimarket
//
//  Created by apple on 08.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMCatalogDetailModuleCollectionViewCell.h"

@implementation SMCatalogDetailModuleCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

// Initialize the collectiion cell based on the frame and add the tap gesture recognizer for custom animation
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backView = [UIFabric viewWithBackgroundColor:[UIColor whiteColor] andSuperView:self];
    
    self.backView.layer.cornerRadius = 2;
    self.backView.layer.masksToBounds = NO;
    self.backView.layer.shadowOffset = CGSizeMake(0, 0);
    self.backView.layer.shadowRadius = 5;
    self.backView.layer.shadowOpacity = 0.15;
    
    self.photo = [UIFabric imageViewWithImageName:@"ic_menu_card" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self.backView];
    self.photo.clipsToBounds = YES;
    
    self.name = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto-Regular" andFontSize:16 andSuperView:self.backView];
    self.name.alpha = 0.87f;
    self.name.numberOfLines = 2;
    
    self.type = [UIFabric labelWithText:@"цена за шт." andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto-Regular" andFontSize:12 andSuperView:self.backView];
    self.type.alpha = 0.54f;
    
    //self.price = [UIFabric labelWithText:@"" andTextColor:[UIColor whiteColor] andFontName:@"Roboto-Medium" andFontSize:14 andSuperView:self.backView];
    self.price = [UIFabric buttonWithText:@"" andTextColor:[UIColor whiteColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:14 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.backView];
    self.price.layer.cornerRadius = 2;
    //self.price.backgroundColor = [colorScheme colorWithSchemeName:@"navigationBarColor"];
    
    return self;
}

// Initialize the collection cell based on the nscoder and add the tap gesture recognizer for custom animation
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    return self;
}

// Initialize the collectiion cell and add the tap gesture recognizer for custom animation
- (id)init {
    self = [super init];
    
    return self;
}

-(void)layoutSubviews {
    
    CGRect newCellSubViewsFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    CGRect newCellViewFrame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    
    self.contentView.frame = self.contentView.bounds = self.backgroundView.frame  = newCellSubViewsFrame;
    self.frame = newCellViewFrame;
    
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(235*[Functions koefX]);
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.centerY.equalTo(self);
    }];
    
    [self.photo mas_remakeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(self.backView).with.offset(0);
        make.top.equalTo(self.backView).with.offset(0);
        make.right.equalTo(self.backView).with.offset(0);
        make.height.mas_equalTo(100*[Functions koefX]);
    }];
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(12*[Functions koefX]);
        make.right.equalTo(self.backView).with.offset(-12*[Functions koefX]);
        make.top.equalTo(self.photo.mas_bottom).with.offset(0*[Functions koefX]);
    }];
    
    [self.type mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(12*[Functions koefX]);
        make.right.equalTo(self.backView).with.offset(-12*[Functions koefX]);
        make.bottom.equalTo(self.backView).with.offset(-5*[Functions koefX]);
    }];
    
    [super layoutSubviews];
}

#pragma mark - actions

- (void)setGoods:(goods *)goods {
    self.name.text = goods.name;
    //self.type.text = goods.type;
    //self.price.text = goods.price;
    self.price.titleEdgeInsets = UIEdgeInsetsMake(2, 5, 2, 5);
    [self.price setTitle:[NSString stringWithFormat:@"%@", goods.price] forState:UIControlStateNormal];
    //UIEdgeInsets insets = {5, 5, 5, 5};
    float maxWidth = ceil(([UIScreen mainScreen].bounds.size.width - 4*18*[Functions koefX])/2);
    CGSize size = [self.price.titleLabel.text sizeWithFont:[UIFont fontWithName:@"Roboto-Medium" size:14*[Functions koefY]] constrainedToSize:CGSizeMake(maxWidth, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    //[self.price drawTextInRect:UIEdgeInsetsInsetRect(CGRectMake(0, 0, size.width, size.height), insets)];
    //self.price.edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.price mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(12*[Functions koefX]);
        //make.right.equalTo(self.backView).with.offset(-12*[Functions koefX]);
        make.bottom.equalTo(self.type.mas_top).with.offset(-3*[Functions koefX]);
        make.width.mas_equalTo(size.width + 10);
    }];
    
    [self.photo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", domain, goods.image]] placeholderImage:[UIImage imageNamed:@"ic_empty"] options:0];
}

@end
