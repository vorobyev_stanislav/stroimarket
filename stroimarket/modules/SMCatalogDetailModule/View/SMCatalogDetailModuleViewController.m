//
//  SMCatalogDetailModuleSMCatalogDetailModuleViewController.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogDetailModuleViewController.h"
#import "Functions.h"
#import "SMCatalogDetailModuleCollectionViewCell.h"
#import "SMCatalogDetailModuleViewOutput.h"

@implementation SMCatalogDetailModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [Functions setupNavigationController:self.navigationController andSearchTitle:@"Я ищу" andTarget:self andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    
    [self.scroll mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(0);
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.height.mas_equalTo(150*[Functions koefX]);
    }];
    
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scroll.mas_bottom).with.offset(0);
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.bottom.equalTo(self.view).with.offset(0);
    }];
    
    [super updateViewConstraints];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.view.backgroundColor = [colorScheme colorWithSchemeName:@"backgroundViewColor"];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    UICollectionView *cv = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    self.collectionView = cv;
    [self.collectionView setDataSource:(id)self];
    [self.collectionView setDelegate:(id)self];
    [self.view addSubview:cv];
    cv.backgroundColor = [UIColor clearColor];
    
    [self.collectionView registerClass:[SMCatalogDetailModuleCollectionViewCell class] forCellWithReuseIdentifier:@"goods"];
    
    self.scroll = [UIFabric scrollView:self.view];
    self.scroll.pagingEnabled = YES;
}

#pragma mark - UICollectionView delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.goods.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SMCatalogDetailModuleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"goods" forIndexPath:indexPath];
    [cell setGoods:self.goods[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    goods *cc = self.goods[indexPath.row];
    [self.output showProduct:cc];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(ceil((self.view.frame.size.width - 4*18*[Functions koefX])/2), ceil(250*[Functions koefX]));
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 18*[Functions koefX], 0, 18*[Functions koefX]);
}

#pragma mark - Методы SMCatalogDetailModuleViewInput

- (void)goodsReceived:(NSArray *)goods andImages:(NSArray *)images {
    self.goods = goods;
    [self.collectionView reloadData];
    
    float height = 0;
    if(images.count) {
        height = 150;
    } else {
        height = 0;
    }
    
    [self.scroll mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(0);
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.height.mas_equalTo(height*[Functions koefX]);
    }];
    
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scroll.mas_bottom).with.offset(0);
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.bottom.equalTo(self.view).with.offset(0);
    }];
    
    for(int i=0;i<images.count;i++) {
        collectionImages *cc = images[i];
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width*i, 0, self.view.frame.size.width, height)];
        [self.scroll addSubview:img];
        img.clipsToBounds = YES;
        if([cc.image length]) {
            img.contentMode = UIViewContentModeScaleAspectFill;
        } else {
            img.contentMode = UIViewContentModeScaleAspectFit;
        }
        
        [img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", domain, cc.image]] placeholderImage:[UIImage imageNamed:@"ic_empty"] completed:nil];
    }
    
    self.scroll.contentSize = CGSizeMake(self.view.frame.size.width*images.count, height);
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
