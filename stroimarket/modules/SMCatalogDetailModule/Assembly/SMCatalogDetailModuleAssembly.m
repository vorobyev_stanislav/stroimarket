//
//  SMCatalogDetailModuleSMCatalogDetailModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogDetailModuleAssembly.h"
#import "catalogService.h"
#import "SMCatalogDetailModuleViewController.h"
#import "SMCatalogDetailModuleInteractor.h"
#import "SMCatalogDetailModulePresenter.h"
#import "SMCatalogDetailModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMCatalogDetailModuleAssembly

- (SMCatalogDetailModuleViewController *)viewSMCatalogDetailModuleModule {
    return [TyphoonDefinition withClass:[SMCatalogDetailModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMCatalogDetailModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMCatalogDetailModuleModule]];
                          }];
}

- (SMCatalogDetailModuleInteractor *)interactorSMCatalogDetailModuleModule {
    return [TyphoonDefinition withClass:[SMCatalogDetailModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMCatalogDetailModuleModule]];
                              [definition injectProperty:@selector(catalogService)
                                                    with:[self catalogServiceSMCatalogDetailModule]];
                          }];
}

- (SMCatalogDetailModulePresenter *)presenterSMCatalogDetailModuleModule {
    return [TyphoonDefinition withClass:[SMCatalogDetailModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMCatalogDetailModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMCatalogDetailModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMCatalogDetailModuleModule]];

                          }];
}

- (SMCatalogDetailModuleRouter *)routerSMCatalogDetailModuleModule {
    return [TyphoonDefinition withClass:[SMCatalogDetailModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMCatalogDetailModuleModule]];
                          }];
}

- (catalogService *)catalogServiceSMCatalogDetailModule {
    return [TyphoonDefinition withClass:[catalogService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
