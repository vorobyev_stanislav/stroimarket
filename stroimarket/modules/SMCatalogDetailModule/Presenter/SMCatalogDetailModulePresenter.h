//
//  SMCatalogDetailModuleSMCatalogDetailModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogDetailModuleViewOutput.h"
#import "SMCatalogDetailModuleInteractorOutput.h"
#import "SMCatalogDetailModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMCatalogDetailModuleViewInput;
@protocol SMCatalogDetailModuleInteractorInput;
@protocol SMCatalogDetailModuleRouterInput;

@interface SMCatalogDetailModulePresenter : NSObject <SMCatalogDetailModuleModuleInput, SMCatalogDetailModuleViewOutput, SMCatalogDetailModuleInteractorOutput>

@property (nonatomic, weak) id<SMCatalogDetailModuleViewInput> view;
@property (nonatomic, strong) id<SMCatalogDetailModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMCatalogDetailModuleRouterInput> router;

@property NSInteger collectionId;

@end
