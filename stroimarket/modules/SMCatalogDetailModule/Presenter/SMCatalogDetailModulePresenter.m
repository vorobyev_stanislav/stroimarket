//
//  SMCatalogDetailModuleSMCatalogDetailModulePresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogDetailModulePresenter.h"

#import "SMCatalogDetailModuleViewInput.h"
#import "SMCatalogDetailModuleInteractorInput.h"
#import "SMCatalogDetailModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMCatalogDetailModulePresenter

#pragma mark - Методы SMCatalogDetailModuleModuleInput

- (void)configureModule:(NSDictionary *)params {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
    self.collectionId = [[params valueForKey:@"collectionId"] integerValue];
}

#pragma mark - Методы SMCatalogDetailModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMCatalogDetailModuleViewOutput

- (void)showProduct:(id)product {
    [self.router openGoodsModuleWithParams:@{@"goods" : product}];
}

- (void) viewWillApear {
    [self.interactor getGoodsForCatalog:self.collectionId];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMCatalogDetailModuleInteractorOutput

- (void)goodsReceived:(NSArray *)goods andImages:(NSArray *)images {
    [self.view goodsReceived:goods andImages:images];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
