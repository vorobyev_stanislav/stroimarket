//
//  SMCatalogDetailModuleSMCatalogDetailModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMCatalogDetailModuleModel.h"

@protocol SMCatalogDetailModuleCellInput <NSObject>

@end

@interface SMCatalogDetailModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMCatalogDetailModuleModel *model;
@property (nonatomic, strong) id<SMCatalogDetailModuleCellInput> cell;
@end
