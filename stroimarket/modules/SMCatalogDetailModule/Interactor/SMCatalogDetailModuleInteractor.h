//
//  SMCatalogDetailModuleSMCatalogDetailModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogDetailModuleInteractorInput.h"
#import "catalogService.h"

@protocol SMCatalogDetailModuleInteractorOutput;

@interface SMCatalogDetailModuleInteractor : NSObject <SMCatalogDetailModuleInteractorInput>

@property (nonatomic, weak) id<SMCatalogDetailModuleInteractorOutput> output;
@property (nonatomic, strong) catalogService *catalogService;

@end
