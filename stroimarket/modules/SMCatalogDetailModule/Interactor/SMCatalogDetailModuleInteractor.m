//
//  SMCatalogDetailModuleSMCatalogDetailModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMCatalogDetailModuleInteractor.h"

#import "SMCatalogDetailModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMCatalogDetailModuleCellPresenter.h"

@interface SMCatalogDetailModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMCatalogDetailModuleInteractor

#pragma mark - Методы SMCatalogDetailModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)getGoodsForCatalog:(NSInteger)catalogId {
    [self.output progressShow];
    [self.catalogService getGoodsForCatalog:catalogId completed:^(NSArray<goods *> *goods, NSArray<collectionImages *> *images) {
        [self.output goodsReceived:goods andImages:images];
        [self.output progressDismiss];
    }];
}

@end
