//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleRouterInput.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMSubCatalog1ModuleRouterInput <NSObject>

-(void)goBack;
- (void)openSubCatalog2ModuleWithParams:(NSDictionary *)params;

@end
