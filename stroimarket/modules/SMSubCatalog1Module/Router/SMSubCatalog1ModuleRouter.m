//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleRouter.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog1ModuleRouter.h"
#import "SMSubCatalog2ModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMSubCatalog1ModuleRouter

#pragma mark - Методы SMSubCatalog1ModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openSubCatalog2ModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to collection"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMSubCatalog2ModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

@end
