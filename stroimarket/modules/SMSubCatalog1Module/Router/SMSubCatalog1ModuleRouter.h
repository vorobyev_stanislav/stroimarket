//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleRouter.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog1ModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMSubCatalog1ModuleRouter : NSObject <SMSubCatalog1ModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
