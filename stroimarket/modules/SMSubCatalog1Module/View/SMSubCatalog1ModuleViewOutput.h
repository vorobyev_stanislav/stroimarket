//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleViewOutput.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;

@protocol SMSubCatalog1ModuleViewOutput <NSObject>

/**
 @author apple

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)backBtnTap;
- (void) setTableViewManager:(RETableViewManager*)manager;
- (void) viewWillApear;
@end
