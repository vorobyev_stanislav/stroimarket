//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMSubCatalog1ModuleCellPresenter.h"

@interface SMSubCatalog1ModuleTableViewCell : RETableViewCell <SMSubCatalog1ModuleCellInput>

@property (strong, readwrite, nonatomic) SMSubCatalog1ModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *border;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;

@end
