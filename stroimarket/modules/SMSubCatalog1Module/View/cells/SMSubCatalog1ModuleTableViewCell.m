//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleTableViewCell.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog1ModuleTableViewCell.h"
#import "Functions.h"

@implementation SMSubCatalog1ModuleTableViewCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 60*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.name.text = self.item.model.name;
    
    [self.avatar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", domain, self.item.model.image]] placeholderImage:[UIImage imageNamed:@"ic_empty"] options:0];
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMCatalogModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.border = [UIFabric viewWithBackgroundColor:[UIColor colorWithRed:245.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1.0f] andSuperView:self];
    
    [self.border mas_remakeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(self).with.offset(0);
        make.right.equalTo(self).with.offset(0);
        make.bottom.equalTo(self).with.offset(0);
        make.height.mas_equalTo(1);
    }];
    
    self.name = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:19 andSuperView:self];
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(self).with.offset(110*[Functions koefX]);
        make.centerY.equalTo(self);
    }];
    
    self.avatar = [UIFabric imageViewWithImageName:@"ic_empty" andContentMode:UIViewContentModeScaleAspectFit iconMode:false andSuperView:self];
    
    [self.avatar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.centerY.equalTo(self);
        make.left.equalTo(self).with.offset(20*[Functions koefX]);
    }];
}

@end
