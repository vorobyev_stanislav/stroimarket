//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleViewController.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMSubCatalog1ModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMSubCatalog1ModuleViewOutput;

@interface SMSubCatalog1ModuleViewController : UIViewController <SMSubCatalog1ModuleViewInput>

@property (nonatomic, strong) id<SMSubCatalog1ModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;
@end
