//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleInteractor.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog1ModuleInteractorInput.h"
#import "catalogService.h"

@protocol SMSubCatalog1ModuleInteractorOutput;

@interface SMSubCatalog1ModuleInteractor : NSObject <SMSubCatalog1ModuleInteractorInput>

@property (nonatomic, weak) id<SMSubCatalog1ModuleInteractorOutput> output;
@property (nonatomic, strong) catalogService *catalogService;

@end
