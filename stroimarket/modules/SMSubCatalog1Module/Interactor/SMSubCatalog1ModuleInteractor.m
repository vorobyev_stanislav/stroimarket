//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleInteractor.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog1ModuleInteractor.h"

#import "SMSubCatalog1ModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMSubCatalog1ModuleCellPresenter.h"

@interface SMSubCatalog1ModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMSubCatalog1ModuleInteractor

#pragma mark - Методы SMSubCatalog1ModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)makeCatalog:(NSInteger)catalogId {
    [self.catalogService getSubCatalog:catalogId completed:^(NSArray<catalogModel *> *catalogs) {
        [self.manager.sections[0] removeAllItems];
        
        for(int i=0;i<catalogs.count;i++) {
            SMSubCatalog1ModuleCellPresenter *pres1 = [SMSubCatalog1ModuleCellPresenter item];
            pres1.model = catalogs[i];
            pres1.selectionHandler = ^(SMSubCatalog1ModuleCellPresenter *newValue){
                [newValue deselectRowAnimated:YES];
                [self.output openCollection:newValue.model.catalogId];
            };
            [self.manager.sections[0] addItem:(id)pres1];
        }
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
        [self.output progressDismiss];
    }];
}

@end
