//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleInteractorOutput.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMSubCatalog1ModuleInteractorOutput <NSObject>

-(void) progressShow;
-(void) progressDismiss;
- (void)openCollection:(NSInteger)collectionId;

@end
