//
//  SMSubCatalog1ModuleSMSubCatalog1ModulePresenter.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog1ModulePresenter.h"

#import "SMSubCatalog1ModuleViewInput.h"
#import "SMSubCatalog1ModuleInteractorInput.h"
#import "SMSubCatalog1ModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMSubCatalog1ModulePresenter

#pragma mark - Методы SMSubCatalog1ModuleModuleInput

- (void)configureModule:(NSDictionary *) params{
    // Стартовая конфигурация модуля, не привязанная к состоянию view
    self.catalogId = [[params valueForKey:@"catalogId"] integerValue];
}

#pragma mark - Методы SMSubCatalog1ModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMSubCatalog1ModuleViewOutput

- (void) viewWillApear {
    [self.interactor makeCatalog:self.catalogId];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMSubCatalog1ModuleInteractorOutput

- (void)openCollection:(NSInteger)collectionId {
    [self.router openSubCatalog2ModuleWithParams:@{@"collectionId" : @(collectionId)}];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
