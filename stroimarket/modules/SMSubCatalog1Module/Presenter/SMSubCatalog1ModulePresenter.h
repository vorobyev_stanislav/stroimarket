//
//  SMSubCatalog1ModuleSMSubCatalog1ModulePresenter.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog1ModuleViewOutput.h"
#import "SMSubCatalog1ModuleInteractorOutput.h"
#import "SMSubCatalog1ModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMSubCatalog1ModuleViewInput;
@protocol SMSubCatalog1ModuleInteractorInput;
@protocol SMSubCatalog1ModuleRouterInput;

@interface SMSubCatalog1ModulePresenter : NSObject <SMSubCatalog1ModuleModuleInput, SMSubCatalog1ModuleViewOutput, SMSubCatalog1ModuleInteractorOutput>

@property (nonatomic, weak) id<SMSubCatalog1ModuleViewInput> view;
@property (nonatomic, strong) id<SMSubCatalog1ModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMSubCatalog1ModuleRouterInput> router;
@property NSInteger catalogId;

@end
