//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "catalogModel.h"

@protocol SMSubCatalog1ModuleCellInput <NSObject>

@end

@interface SMSubCatalog1ModuleCellPresenter : RETableViewItem
@property (strong, readwrite, nonatomic) catalogModel *model;
@property (nonatomic, strong) id<SMSubCatalog1ModuleCellInput> cell;
@end
