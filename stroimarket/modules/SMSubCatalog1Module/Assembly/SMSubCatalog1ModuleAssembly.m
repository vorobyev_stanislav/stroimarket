//
//  SMSubCatalog1ModuleSMSubCatalog1ModuleAssembly.m
//  stroimarket
//
//  Created by apple on 22/05/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSubCatalog1ModuleAssembly.h"
#import "catalogService.h"
#import "SMSubCatalog1ModuleViewController.h"
#import "SMSubCatalog1ModuleInteractor.h"
#import "SMSubCatalog1ModulePresenter.h"
#import "SMSubCatalog1ModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMSubCatalog1ModuleAssembly

- (SMSubCatalog1ModuleViewController *)viewSMSubCatalog1ModuleModule {
    return [TyphoonDefinition withClass:[SMSubCatalog1ModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSubCatalog1ModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMSubCatalog1ModuleModule]];
                          }];
}

- (SMSubCatalog1ModuleInteractor *)interactorSMSubCatalog1ModuleModule {
    return [TyphoonDefinition withClass:[SMSubCatalog1ModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSubCatalog1ModuleModule]];
                              [definition injectProperty:@selector(catalogService)
                                                    with:[self catalogServiceSMSubCatalog1Module]];
                          }];
}

- (SMSubCatalog1ModulePresenter *)presenterSMSubCatalog1ModuleModule {
    return [TyphoonDefinition withClass:[SMSubCatalog1ModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMSubCatalog1ModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMSubCatalog1ModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMSubCatalog1ModuleModule]];

                          }];
}

- (SMSubCatalog1ModuleRouter *)routerSMSubCatalog1ModuleModule {
    return [TyphoonDefinition withClass:[SMSubCatalog1ModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMSubCatalog1ModuleModule]];
                          }];
}

- (catalogService *)catalogServiceSMSubCatalog1Module {
    return [TyphoonDefinition withClass:[catalogService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
