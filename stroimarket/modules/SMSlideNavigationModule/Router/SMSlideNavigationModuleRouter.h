//
//  SMSlideNavigationModuleSMSlideNavigationModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSlideNavigationModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMSlideNavigationModuleRouter : NSObject <SMSlideNavigationModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
