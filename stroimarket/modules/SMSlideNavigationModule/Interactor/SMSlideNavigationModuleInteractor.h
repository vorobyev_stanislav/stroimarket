//
//  SMSlideNavigationModuleSMSlideNavigationModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSlideNavigationModuleInteractorInput.h"


@protocol SMSlideNavigationModuleInteractorOutput;

@interface SMSlideNavigationModuleInteractor : NSObject <SMSlideNavigationModuleInteractorInput>

@property (nonatomic, weak) id<SMSlideNavigationModuleInteractorOutput> output;

@end
