//
//  SMSlideNavigationModuleSMSlideNavigationModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSlideNavigationModuleInteractor.h"

#import "SMSlideNavigationModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMSlideNavigationModuleCellPresenter.h"

@interface SMSlideNavigationModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMSlideNavigationModuleInteractor

#pragma mark - Методы SMSlideNavigationModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}
@end
