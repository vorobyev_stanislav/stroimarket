//
//  SMSlideNavigationModuleSMSlideNavigationModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMSlideNavigationModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMSlideNavigationModuleViewOutput;

@interface SMSlideNavigationModuleViewController : SlideNavigationController <SMSlideNavigationModuleViewInput>

@property (nonatomic, strong) id<SMSlideNavigationModuleViewOutput> output;

@end
