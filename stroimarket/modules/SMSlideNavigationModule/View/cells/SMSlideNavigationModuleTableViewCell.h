//
//  SMSlideNavigationModuleSMSlideNavigationModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMSlideNavigationModuleCellPresenter.h"

@interface SMSlideNavigationModuleTableViewCell : RETableViewCell <SMSlideNavigationModuleCellInput>

@property (strong, readwrite, nonatomic) SMSlideNavigationModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
