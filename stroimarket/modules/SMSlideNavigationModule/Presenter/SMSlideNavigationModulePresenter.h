//
//  SMSlideNavigationModuleSMSlideNavigationModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSlideNavigationModuleViewOutput.h"
#import "SMSlideNavigationModuleInteractorOutput.h"
#import "SMSlideNavigationModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMSlideNavigationModuleViewInput;
@protocol SMSlideNavigationModuleInteractorInput;
@protocol SMSlideNavigationModuleRouterInput;

@interface SMSlideNavigationModulePresenter : NSObject <SMSlideNavigationModuleModuleInput, SMSlideNavigationModuleViewOutput, SMSlideNavigationModuleInteractorOutput>

@property (nonatomic, weak) id<SMSlideNavigationModuleViewInput> view;
@property (nonatomic, strong) id<SMSlideNavigationModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMSlideNavigationModuleRouterInput> router;

@end
