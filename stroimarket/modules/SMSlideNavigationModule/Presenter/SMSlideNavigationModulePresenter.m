//
//  SMSlideNavigationModuleSMSlideNavigationModulePresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSlideNavigationModulePresenter.h"

#import "SMSlideNavigationModuleViewInput.h"
#import "SMSlideNavigationModuleInteractorInput.h"
#import "SMSlideNavigationModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMSlideNavigationModulePresenter

#pragma mark - Методы SMSlideNavigationModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMSlideNavigationModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMSlideNavigationModuleViewOutput

- (void) viewWillApear {
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMSlideNavigationModuleInteractorOutput
-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
