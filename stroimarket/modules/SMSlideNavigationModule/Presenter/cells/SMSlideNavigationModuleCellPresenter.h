//
//  SMSlideNavigationModuleSMSlideNavigationModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMSlideNavigationModuleModel.h"

@protocol SMSlideNavigationModuleCellInput <NSObject>

@end

@interface SMSlideNavigationModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMSlideNavigationModuleModel *model;
@property (nonatomic, strong) id<SMSlideNavigationModuleCellInput> cell;
@end
