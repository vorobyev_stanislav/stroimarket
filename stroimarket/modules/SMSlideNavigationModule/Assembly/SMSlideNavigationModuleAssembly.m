//
//  SMSlideNavigationModuleSMSlideNavigationModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSlideNavigationModuleAssembly.h"

#import "SMSlideNavigationModuleViewController.h"
#import "SMSlideNavigationModuleInteractor.h"
#import "SMSlideNavigationModulePresenter.h"
#import "SMSlideNavigationModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMSlideNavigationModuleAssembly

- (SMSlideNavigationModuleViewController *)viewSMSlideNavigationModuleModule {
    return [TyphoonDefinition withClass:[SMSlideNavigationModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSlideNavigationModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMSlideNavigationModuleModule]];
                          }];
}

- (SMSlideNavigationModuleInteractor *)interactorSMSlideNavigationModuleModule {
    return [TyphoonDefinition withClass:[SMSlideNavigationModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSlideNavigationModuleModule]];
                          }];
}

- (SMSlideNavigationModulePresenter *)presenterSMSlideNavigationModuleModule {
    return [TyphoonDefinition withClass:[SMSlideNavigationModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMSlideNavigationModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMSlideNavigationModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMSlideNavigationModuleModule]];

                          }];
}

- (SMSlideNavigationModuleRouter *)routerSMSlideNavigationModuleModule {
    return [TyphoonDefinition withClass:[SMSlideNavigationModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMSlideNavigationModuleModule]];
                          }];
}

@end
