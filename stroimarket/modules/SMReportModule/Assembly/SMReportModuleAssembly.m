//
//  SMReportModuleSMReportModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMReportModuleAssembly.h"
#import "networkService.h"
#import "SMReportModuleViewController.h"
#import "SMReportModuleInteractor.h"
#import "SMReportModulePresenter.h"
#import "SMReportModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMReportModuleAssembly

- (SMReportModuleViewController *)viewSMReportModuleModule {
    return [TyphoonDefinition withClass:[SMReportModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMReportModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMReportModuleModule]];
                          }];
}

- (SMReportModuleInteractor *)interactorSMReportModuleModule {
    return [TyphoonDefinition withClass:[SMReportModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMReportModuleModule]];
                              [definition injectProperty:@selector(networkService)
                                                    with:[self networkServiceSMReportModule]];
                          }];
}

- (SMReportModulePresenter *)presenterSMReportModuleModule {
    return [TyphoonDefinition withClass:[SMReportModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMReportModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMReportModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMReportModuleModule]];

                          }];
}

- (SMReportModuleRouter *)routerSMReportModuleModule {
    return [TyphoonDefinition withClass:[SMReportModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMReportModuleModule]];
                          }];
}

- (networkService *)networkServiceSMReportModule {
    return [TyphoonDefinition withClass:[networkService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
