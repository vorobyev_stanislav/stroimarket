//
//  SMReportModuleSMReportModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMReportModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMReportModuleViewOutput;

@interface SMReportModuleViewController : UIViewController <SMReportModuleViewInput>

@property (nonatomic, strong) id<SMReportModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UITextView *message;
//@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UIButton *send;

@end
