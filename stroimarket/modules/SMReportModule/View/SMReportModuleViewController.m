//
//  SMReportModuleSMReportModuleViewController.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMReportModuleViewController.h"
#import "Functions.h"
#import "SMReportModuleTableViewCell.h"
#import "SMReportModuleViewOutput.h"

@implementation SMReportModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Оставить отзыв" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    
//    [self.name mas_remakeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(300*[Functions koefX]);
//        make.height.mas_equalTo(40*[Functions koefX]);
//        make.top.equalTo(self.view).with.offset(35*[Functions koefX]);
//        make.centerX.equalTo(self.view);
//    }];
    
    [self.message mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(155*[Functions koefX]);
        make.top.equalTo(self.view).with.offset(35*[Functions koefX]);
        make.centerX.equalTo(self.view);
    }];
    
    [self.send mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.top.equalTo(self.message.mas_bottom).with.offset(35*[Functions koefX]);
        make.centerX.equalTo(self.view);
    }];
    
    [super updateViewConstraints];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (IBAction)action:(id)sender {
    [[SlideNavigationController sharedInstance] openMenu:1 withCompletion:nil];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

- (void)nameDone {
//    [self.name resignFirstResponder];
}

- (void)messageDone {
    [self.message resignFirstResponder];
}

- (void)sendMessageClick {
    
    if(self.message.text.length) {
        [self.output sendMessage:self.message.text];
    } else {
        [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Заполните поле!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
    }
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@"Ваш отзыв"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Ваш отзыв";
        textView.textColor = [colorScheme colorWithSchemeName:@"placeholderColor"];
    }
    [textView resignFirstResponder];
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
//    self.name = [UIFabric textFieldWithPlaceholder:@"Ваше имя" andPlaceholderColor:[colorScheme colorWithSchemeName:@"placeholderColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andIsPassword:false andSuperView:self.view];
//    self.name.layer.cornerRadius = 2;
//    self.name.layer.borderWidth = 1;
//    self.name.layer.borderColor = [UIColor colorWithRed:218.0f/255.0f green:218.0f/255.0f blue:218.0f/255.0f alpha:1.0f].CGColor;
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40*[Functions koefX])];
//    self.name.leftView = paddingView;
//    self.name.leftViewMode = UITextFieldViewModeAlways;
//    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40*[Functions koefX])];
//    self.name.rightView = paddingView;
//    self.name.leftViewMode = UITextFieldViewModeAlways;
//    self.name.autocorrectionType = UITextAutocorrectionTypeNo;
    
//    [Functions addKeyboardToolbarForTextField:self.name width:self.view.frame.size.width target:self doneTitle:@"Готово" doneAction:@selector(nameDone)];
    
    self.message = [UIFabric textViewWithText:@"Ваш отзыв" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.view];
    self.message.textColor = [colorScheme colorWithSchemeName:@"placeholderColor"];
    self.message.delegate = (id)self;
    self.message.layer.cornerRadius = 2;
    self.message.layer.borderWidth = 1;
    self.message.layer.borderColor = [UIColor colorWithRed:218.0f/255.0f green:218.0f/255.0f blue:218.0f/255.0f alpha:1.0f].CGColor;
    self.message.autocorrectionType = UITextAutocorrectionTypeNo;
    [Functions addKeyboardToolbarForTextField:self.message width:self.view.frame.size.width target:self doneTitle:@"Готово" doneAction:@selector(messageDone)];
    
    self.send = [UIFabric buttonWithText:@"ОСТАВИТЬ ОТЗЫВ" andTextColor:[colorScheme colorWithSchemeName:@"backgroundCellColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    self.send.layer.cornerRadius = 2;
    
    [self.send addTarget:self action:@selector(sendMessageClick) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - Методы SMReportModuleViewInput

- (void)sendError {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Произошла ошибка! Повторите операцию снова." cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

- (void)sendSuccess {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Спасибо за сообщение! Мы рассмотрим его в ближайшее время." cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
    self.message.text = @"";
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
