//
//  SMReportModuleSMReportModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMReportModuleCellPresenter.h"

@interface SMReportModuleTableViewCell : RETableViewCell <SMReportModuleCellInput>

@property (strong, readwrite, nonatomic) SMReportModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
