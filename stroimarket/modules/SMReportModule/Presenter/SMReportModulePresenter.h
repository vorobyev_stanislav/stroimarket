//
//  SMReportModuleSMReportModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMReportModuleViewOutput.h"
#import "SMReportModuleInteractorOutput.h"
#import "SMReportModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMReportModuleViewInput;
@protocol SMReportModuleInteractorInput;
@protocol SMReportModuleRouterInput;

@interface SMReportModulePresenter : NSObject <SMReportModuleModuleInput, SMReportModuleViewOutput, SMReportModuleInteractorOutput>

@property (nonatomic, weak) id<SMReportModuleViewInput> view;
@property (nonatomic, strong) id<SMReportModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMReportModuleRouterInput> router;

@end
