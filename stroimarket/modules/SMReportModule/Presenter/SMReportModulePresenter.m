//
//  SMReportModuleSMReportModulePresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMReportModulePresenter.h"

#import "SMReportModuleViewInput.h"
#import "SMReportModuleInteractorInput.h"
#import "SMReportModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMReportModulePresenter

#pragma mark - Методы SMReportModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMReportModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMReportModuleViewOutput

- (void)sendMessage:(NSString *)message {
    [self.interactor sendMessage:message];
}

- (void) viewWillApear {
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMReportModuleInteractorOutput

- (void)sendSuccess {
    [self.view sendSuccess];
}

- (void)sendError {
    [self.view sendError];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
