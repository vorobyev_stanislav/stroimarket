//
//  SMReportModuleSMReportModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMReportModuleModel.h"

@protocol SMReportModuleCellInput <NSObject>

@end

@interface SMReportModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMReportModuleModel *model;
@property (nonatomic, strong) id<SMReportModuleCellInput> cell;
@end
