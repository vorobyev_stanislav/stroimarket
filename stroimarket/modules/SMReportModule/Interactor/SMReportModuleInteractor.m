//
//  SMReportModuleSMReportModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMReportModuleInteractor.h"

#import "SMReportModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMReportModuleCellPresenter.h"

@interface SMReportModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMReportModuleInteractor

#pragma mark - Методы SMReportModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)sendMessage:(NSString *)message {
    [self.output progressShow];
    [self.networkService sendMessage:message completed:^(BOOL status) {
        [self.output progressDismiss];
        if(status) {
            [self.output sendSuccess];
        } else {
            [self.output sendError];
        }
    }];
}

@end
