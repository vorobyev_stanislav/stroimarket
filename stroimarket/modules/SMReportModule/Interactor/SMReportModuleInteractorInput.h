//
//  SMReportModuleSMReportModuleInteractorInput.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;
@protocol SMReportModuleInteractorInput <NSObject>

- (void) setTableViewManager:(RETableViewManager*)manager;
- (void)sendMessage:(NSString *)message;

@end
