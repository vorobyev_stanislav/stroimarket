//
//  SMReportModuleSMReportModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMReportModuleInteractorInput.h"
#import "networkService.h"

@protocol SMReportModuleInteractorOutput;

@interface SMReportModuleInteractor : NSObject <SMReportModuleInteractorInput>

@property (nonatomic, weak) id<SMReportModuleInteractorOutput> output;
@property (nonatomic, strong) networkService *networkService;

@end
