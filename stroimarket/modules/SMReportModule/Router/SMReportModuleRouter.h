//
//  SMReportModuleSMReportModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMReportModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMReportModuleRouter : NSObject <SMReportModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
