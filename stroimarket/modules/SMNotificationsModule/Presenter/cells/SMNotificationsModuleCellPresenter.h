//
//  SMNotificationsModuleSMNotificationsModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "notificationModel.h"

@protocol SMNotificationsModuleCellInput <NSObject>

@end

@interface SMNotificationsModuleCellPresenter : RETableViewItem
@property (strong, readwrite, nonatomic) notificationModel *model;
@property (nonatomic, strong) id<SMNotificationsModuleCellInput> cell;
@end
