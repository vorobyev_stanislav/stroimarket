//
//  SMNotificationsModuleSMNotificationsModulePresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNotificationsModulePresenter.h"

#import "SMNotificationsModuleViewInput.h"
#import "SMNotificationsModuleInteractorInput.h"
#import "SMNotificationsModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMNotificationsModulePresenter

#pragma mark - Методы SMNotificationsModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMNotificationsModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMNotificationsModuleViewOutput

- (void) viewWillApear {
    [self.interactor getNotifications];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMNotificationsModuleInteractorOutput
-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
