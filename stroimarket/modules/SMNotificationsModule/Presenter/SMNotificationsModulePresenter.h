//
//  SMNotificationsModuleSMNotificationsModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNotificationsModuleViewOutput.h"
#import "SMNotificationsModuleInteractorOutput.h"
#import "SMNotificationsModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMNotificationsModuleViewInput;
@protocol SMNotificationsModuleInteractorInput;
@protocol SMNotificationsModuleRouterInput;

@interface SMNotificationsModulePresenter : NSObject <SMNotificationsModuleModuleInput, SMNotificationsModuleViewOutput, SMNotificationsModuleInteractorOutput>

@property (nonatomic, weak) id<SMNotificationsModuleViewInput> view;
@property (nonatomic, strong) id<SMNotificationsModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMNotificationsModuleRouterInput> router;

@end
