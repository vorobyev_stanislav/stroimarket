//
//  SMNotificationsModuleSMNotificationsModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMNotificationsModuleCellPresenter.h"

@interface SMNotificationsModuleTableViewCell : RETableViewCell <SMNotificationsModuleCellInput>

@property (strong, readwrite, nonatomic) SMNotificationsModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *alarm;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *notification;

@end
