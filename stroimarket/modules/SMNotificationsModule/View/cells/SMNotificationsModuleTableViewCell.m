//
//  SMNotificationsModuleSMNotificationsModuleTableViewCell.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNotificationsModuleTableViewCell.h"
#import "Functions.h"

@implementation SMNotificationsModuleTableViewCell
#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 75*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.notification.text = self.item.model.notification;
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMNotificationsModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.backView = [UIFabric viewWithBackgroundColor:[UIColor whiteColor] andSuperView:self];
    self.backView.layer.cornerRadius = 2;
    
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(5);
        make.right.equalTo(self).with.offset(-5);
        make.top.equalTo(self).with.offset(5);
        make.bottom.equalTo(self).with.offset(-5);
    }];
    
    self.alarm = [UIFabric imageViewWithImageName:@"ic_menu_notifications_yellow" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self.backView];

    [self.alarm mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(10);
        make.top.equalTo(self.backView).with.offset(8);
    }];
    
    self.notification = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"labelTextColor1"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.backView];
    self.notification.numberOfLines = 0;
    
    [self.notification mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(80*[Functions koefX]);
        make.right.equalTo(self.backView).with.offset(-5);
        make.top.equalTo(self.backView).with.offset(5);
    }];
}
@end
