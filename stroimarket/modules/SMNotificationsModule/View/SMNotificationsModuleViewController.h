//
//  SMNotificationsModuleSMNotificationsModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMNotificationsModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMNotificationsModuleViewOutput;

@interface SMNotificationsModuleViewController : UIViewController <SMNotificationsModuleViewInput>

@property (nonatomic, strong) id<SMNotificationsModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;

@end
