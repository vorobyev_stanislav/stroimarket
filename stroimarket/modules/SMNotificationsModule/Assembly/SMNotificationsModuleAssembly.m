//
//  SMNotificationsModuleSMNotificationsModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNotificationsModuleAssembly.h"
#import "notificationService.h"
#import "SMNotificationsModuleViewController.h"
#import "SMNotificationsModuleInteractor.h"
#import "SMNotificationsModulePresenter.h"
#import "SMNotificationsModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMNotificationsModuleAssembly

- (SMNotificationsModuleViewController *)viewSMNotificationsModuleModule {
    return [TyphoonDefinition withClass:[SMNotificationsModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMNotificationsModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMNotificationsModuleModule]];
                          }];
}

- (SMNotificationsModuleInteractor *)interactorSMNotificationsModuleModule {
    return [TyphoonDefinition withClass:[SMNotificationsModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMNotificationsModuleModule]];
                              [definition injectProperty:@selector(notificationService)
                                                    with:[self notificationServiceSMNotificationsModule]];
                          }];
}

- (SMNotificationsModulePresenter *)presenterSMNotificationsModuleModule {
    return [TyphoonDefinition withClass:[SMNotificationsModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMNotificationsModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMNotificationsModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMNotificationsModuleModule]];

                          }];
}

- (SMNotificationsModuleRouter *)routerSMNotificationsModuleModule {
    return [TyphoonDefinition withClass:[SMNotificationsModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMNotificationsModuleModule]];
                          }];
}

- (notificationService *)notificationServiceSMNotificationsModule {
    return [TyphoonDefinition withClass:[notificationService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
