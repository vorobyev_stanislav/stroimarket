//
//  SMNotificationsModuleSMNotificationsModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNotificationsModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMNotificationsModuleRouter : NSObject <SMNotificationsModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
