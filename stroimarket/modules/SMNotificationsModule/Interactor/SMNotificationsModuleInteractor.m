//
//  SMNotificationsModuleSMNotificationsModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNotificationsModuleInteractor.h"

#import "SMNotificationsModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMNotificationsModuleCellPresenter.h"

@interface SMNotificationsModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMNotificationsModuleInteractor

#pragma mark - Методы SMNotificationsModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)getNotifications {
    [self.notificationService getNotifications:^(NSArray<notificationModel *> *notifications) {
        [self.manager.sections[0] removeAllItems];
        
        for(int i=0;i<notifications.count;i++) {
            SMNotificationsModuleCellPresenter *pres2 = [SMNotificationsModuleCellPresenter item];
            pres2.model = notifications[i];
            pres2.selectionHandler = ^(SMNotificationsModuleCellPresenter *newValue){
                [newValue deselectRowAnimated:YES];
            };
            [self.manager.sections[0] addItem:(id)pres2];
        }
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
    }];
}

@end
