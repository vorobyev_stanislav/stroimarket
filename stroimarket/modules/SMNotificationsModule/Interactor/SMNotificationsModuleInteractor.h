//
//  SMNotificationsModuleSMNotificationsModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMNotificationsModuleInteractorInput.h"
#import "notificationService.h"

@protocol SMNotificationsModuleInteractorOutput;

@interface SMNotificationsModuleInteractor : NSObject <SMNotificationsModuleInteractorInput>

@property (nonatomic, weak) id<SMNotificationsModuleInteractorOutput> output;
@property (nonatomic, strong) notificationService *notificationService;

@end
