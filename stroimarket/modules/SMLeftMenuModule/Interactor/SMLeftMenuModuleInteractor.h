//
//  SMLeftMenuModuleSMLeftMenuModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMLeftMenuModuleInteractorInput.h"
#import "networkService.h"

@protocol SMLeftMenuModuleInteractorOutput;

@interface SMLeftMenuModuleInteractor : NSObject <SMLeftMenuModuleInteractorInput>

@property (nonatomic, weak) id<SMLeftMenuModuleInteractorOutput> output;
@property (nonatomic, strong) networkService *networkService;

@end
