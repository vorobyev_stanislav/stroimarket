//
//  SMLeftMenuModuleSMLeftMenuModuleInteractorOutput.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMLeftMenuModuleInteractorOutput <NSObject>
-(void) progressShow;
-(void) progressDismiss;
- (void)openProfile;
- (void)openCard;
- (void)needExit;

@end
