//
//  SMLeftMenuModuleSMLeftMenuModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMLeftMenuModuleInteractor.h"

#import "SMLeftMenuModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMLeftMenuModuleCellPresenter.h"

@interface SMLeftMenuModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMLeftMenuModuleInteractor

#pragma mark - Методы SMLeftMenuModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)makeMenu {
    
    [self.manager.sections[0] removeAllItems];
    
    SMLeftMenuModuleHeaderCellPresenter *pres = [SMLeftMenuModuleHeaderCellPresenter item];
    pres.interactor = (id)self;
    [self.manager.sections[0] addItem:(id)pres];
    
    SMLeftMenuModuleCellPresenter *pres2 = [SMLeftMenuModuleCellPresenter item];
    menuModel *model = [menuModel new];
    model.avatar = @"ic_menu_main";
    model.name = @"Главная";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [MySingleton sharedMySingleton].menu = 1;
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMLeftMenuModuleCellPresenter item];
    model = [menuModel new];
    model.avatar = @"ic_menu_catalog";
    model.name = @"Каталог";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [MySingleton sharedMySingleton].menu = 2;
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMLeftMenuModuleCellPresenter item];
    model = [menuModel new];
    model.avatar = @"ic_menu_selected_goods";
    model.name = @"Отмеченные товары";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [MySingleton sharedMySingleton].menu = 3;
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMLeftMenuModuleCellPresenter item];
    model = [menuModel new];
    model.avatar = @"ic_menu_myorders";
    model.name = @"Мои заказы";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [MySingleton sharedMySingleton].menu = 4;
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMLeftMenuModuleCellPresenter item];
    model = [menuModel new];
    model.avatar = @"ic_menu_bonuses";
    model.name = @"Бонусы";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [MySingleton sharedMySingleton].menu = 5;
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMLeftMenuModuleCellPresenter item];
    model = [menuModel new];
    model.avatar = @"ic_menu_notifications";
    model.name = @"Уведомления";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [MySingleton sharedMySingleton].menu = 6;
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMLeftMenuModuleCellPresenter item];
    model = [menuModel new];
    model.avatar = @"ic_menu_shops";
    model.name = @"Адреса магазинов";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [MySingleton sharedMySingleton].menu = 7;
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMLeftMenuModuleCellPresenter item];
    model = [menuModel new];
    model.avatar = @"ic_menu_report";
    model.name = @"Оставить отзыв";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [MySingleton sharedMySingleton].menu = 8;
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMLeftMenuModuleCellPresenter item];
    model = [menuModel new];
    model.avatar = @"ic_menu_about";
    model.name = @"О компании";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [MySingleton sharedMySingleton].menu = 9;
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    pres2 = [SMLeftMenuModuleCellPresenter item];
    model = [menuModel new];
    model.avatar = @"ic-exit";
    model.name = @"Выход";
    pres2.model = model;
    pres2.selectionHandler = ^(SMLeftMenuModuleCellPresenter *newValue){
        [self.output needExit];
        [newValue deselectRowAnimated:YES];
    };
    [self.manager.sections[0] addItem:(id)pres2];
    
    [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
}

- (void)makeExit {
    [MySingleton sharedMySingleton].menu = 12;
}

- (void)getProfile {
    [self.networkService getProfile:^(profile *profile) {
        [self makeMenu];
    }];
}

- (void)openProfile {
    [self.output openProfile];
}

- (void)openCard {
    [self.output openCard];
}

@end
