//
//  SMLeftMenuModuleSMLeftMenuModuleInteractorInput.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;
@protocol SMLeftMenuModuleInteractorInput <NSObject>

- (void)setTableViewManager:(RETableViewManager*)manager;
- (void)makeMenu;
- (void)openProfile;
- (void)openCard;
- (void)getProfile;
- (void)makeExit;

@end
