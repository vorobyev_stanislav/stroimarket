//
//  SMLeftMenuModuleSMLeftMenuModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMLeftMenuModuleAssembly.h"

#import "SMLeftMenuModuleViewController.h"
#import "SMLeftMenuModuleInteractor.h"
#import "SMLeftMenuModulePresenter.h"
#import "SMLeftMenuModuleRouter.h"
#import "TyphoonStoryboardDefinition.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"
#import "networkService.h"

@implementation SMLeftMenuModuleAssembly

- (SMLeftMenuModuleViewController *)viewSMLeftMenuModuleModule {
    return [TyphoonDefinition withClass:[SMLeftMenuModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMLeftMenuModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMLeftMenuModuleModule]];
                              [definition injectProperty:@selector(mainController) with:[self mainViewController1]];
                              [definition injectProperty:@selector(catalogController) with:[self catalogViewController1]];
                              [definition injectProperty:@selector(selectedGoodsController) with:[self selectedGoodsViewController1]];
                              [definition injectProperty:@selector(myOrdersController) with:[self myOrdersViewController1]];
                              [definition injectProperty:@selector(bonusesController) with:[self bonusesViewController1]];
                              [definition injectProperty:@selector(notificationsController) with:[self NotificationsViewController1]];
                              [definition injectProperty:@selector(shopsController) with:[self shopsViewController1]];
                              [definition injectProperty:@selector(reportController) with:[self reportViewController1]];
                              [definition injectProperty:@selector(aboutController) with:[self aboutViewController1]];
                              [definition injectProperty:@selector(profileController) with:[self profileViewController1]];
                              [definition injectProperty:@selector(cardController) with:[self cardViewController1]];
                          }];
}

- (SMLeftMenuModuleInteractor *)interactorSMLeftMenuModuleModule {
    return [TyphoonDefinition withClass:[SMLeftMenuModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMLeftMenuModuleModule]];
                              [definition injectProperty:@selector(networkService)
                                                    with:[self networkServiceSMLeftMenuModule]];
                          }];
}

- (SMLeftMenuModulePresenter *)presenterSMLeftMenuModuleModule {
    return [TyphoonDefinition withClass:[SMLeftMenuModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMLeftMenuModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMLeftMenuModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMLeftMenuModuleModule]];

                          }];
}

- (SMLeftMenuModuleRouter *)routerSMLeftMenuModuleModule {
    return [TyphoonDefinition withClass:[SMLeftMenuModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMLeftMenuModuleModule]];
                          }];
}

- (UIViewController *)mainViewController1 {
    
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMMainModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
    
}

- (UIViewController *)catalogViewController1 {
    
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMCatalogModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)selectedGoodsViewController1 {
    
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMSelectedGoodsModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)myOrdersViewController1 {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMMyOrdersModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)bonusesViewController1 {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMBonusesModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)NotificationsViewController1 {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMNotificationsModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)shopsViewController1 {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMShopsModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)reportViewController1 {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMReportModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)aboutViewController1 {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMAboutModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)cardViewController1 {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMCardModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)profileViewController1 {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard1]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMProfileModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIStoryboard*)mainStoryboard1 {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}

- (networkService *)networkServiceSMLeftMenuModule {
    return [TyphoonDefinition withClass:[networkService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
