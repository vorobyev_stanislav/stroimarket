//
//  SMLeftMenuModuleSMLeftMenuModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMLeftMenuModuleViewOutput.h"
#import "SMLeftMenuModuleInteractorOutput.h"
#import "SMLeftMenuModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMLeftMenuModuleViewInput;
@protocol SMLeftMenuModuleInteractorInput;
@protocol SMLeftMenuModuleRouterInput;

@interface SMLeftMenuModulePresenter : NSObject <SMLeftMenuModuleModuleInput, SMLeftMenuModuleViewOutput, SMLeftMenuModuleInteractorOutput>

@property (nonatomic, weak) id<SMLeftMenuModuleViewInput> view;
@property (nonatomic, strong) id<SMLeftMenuModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMLeftMenuModuleRouterInput> router;

@end
