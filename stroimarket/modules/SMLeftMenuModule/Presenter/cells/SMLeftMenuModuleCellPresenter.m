//
//  SMLeftMenuModuleSMLeftMenuModuleCellPresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMLeftMenuModuleCellPresenter.h"

#import "Functions.h"

@implementation SMLeftMenuModuleHeaderCellPresenter

- (void)openProfile {
    [self.interactor openProfile];
}

- (void)openCard {
    [self.interactor openCard];
}

@end

@implementation SMLeftMenuModuleCellPresenter

@end
