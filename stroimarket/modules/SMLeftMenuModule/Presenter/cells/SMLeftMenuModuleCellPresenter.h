//
//  SMLeftMenuModuleSMLeftMenuModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "menuModel.h"
#import "SMLeftMenuModuleInteractorInput.h"

@protocol SMLeftMenuModuleCellInput <NSObject>

@end

@interface SMLeftMenuModuleHeaderCellPresenter : RETableViewItem

//@property (strong, readwrite, nonatomic) SMLeftMenuModuleModel *model;
@property (nonatomic, strong) id<SMLeftMenuModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMLeftMenuModuleCellInput> cell;

- (void)openProfile;
- (void)openCard;

@end

@interface SMLeftMenuModuleCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) menuModel *model;
@property (nonatomic, strong) id<SMLeftMenuModuleCellInput> cell;

@end
