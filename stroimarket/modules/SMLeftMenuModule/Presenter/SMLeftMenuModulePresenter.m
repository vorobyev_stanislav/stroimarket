//
//  SMLeftMenuModuleSMLeftMenuModulePresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMLeftMenuModulePresenter.h"

#import "SMLeftMenuModuleViewInput.h"
#import "SMLeftMenuModuleInteractorInput.h"
#import "SMLeftMenuModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMLeftMenuModulePresenter

#pragma mark - Методы SMLeftMenuModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMLeftMenuModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMLeftMenuModuleViewOutput

- (void)makeExit {
    [self.interactor makeExit];
}

- (void) viewWillApear {
    [self.interactor makeMenu];
    [self.interactor getProfile];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMLeftMenuModuleInteractorOutput

- (void)needExit {
    [self.view needExit];
}

- (void)openCard {
    [self.view openCard];
}

- (void)openProfile {
    [self.view openProfile];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
