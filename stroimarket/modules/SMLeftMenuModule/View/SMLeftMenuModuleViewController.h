//
//  SMLeftMenuModuleSMLeftMenuModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMLeftMenuModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMLeftMenuModuleViewOutput;

@interface SMLeftMenuModuleViewController : UIViewController <SMLeftMenuModuleViewInput>

@property (nonatomic, strong) id<SMLeftMenuModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;

@property UIViewController *mainController;
@property UIViewController *catalogController;
@property UIViewController *selectedGoodsController;
@property UIViewController *myOrdersController;
@property UIViewController *bonusesController;
@property UIViewController *notificationsController;
@property UIViewController *shopsController;
@property UIViewController *reportController;
@property UIViewController *aboutController;
@property UIViewController *profileController;
@property UIViewController *cardController;

@end
