//
//  SMLeftMenuModuleSMLeftMenuModuleTableViewCell.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMLeftMenuModuleTableViewCell.h"
#import "Functions.h"

@implementation SMLeftMenuModuleTableViewCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 44;
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.name.text = self.item.model.name;
    self.avatar.image = [UIImage imageNamed:self.item.model.avatar];
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMLeftMenuModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.backgroundColor = [UIColor clearColor];
    self.avatar = [UIFabric imageViewWithImageName:@"" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];
    
    [self.avatar mas_remakeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(self).with.offset(18*[Functions koefX]);
        make.centerY.equalTo(self).with.offset(0);
        make.height.mas_equalTo(25*[Functions koefX]);
        make.width.mas_equalTo(25*[Functions koefX]);
    }];
    
    self.name = [UIFabric labelWithText:@"" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:19 andSuperView:self];
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(self).with.offset(80*[Functions koefX]);
        make.centerY.equalTo(self).with.offset(0);
    }];

}

@end
