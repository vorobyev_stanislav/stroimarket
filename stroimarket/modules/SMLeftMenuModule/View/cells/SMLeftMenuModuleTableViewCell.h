//
//  SMLeftMenuModuleSMLeftMenuModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMLeftMenuModuleCellPresenter.h"

@interface SMLeftMenuModuleTableViewCell : RETableViewCell <SMLeftMenuModuleCellInput>

@property (strong, readwrite, nonatomic) SMLeftMenuModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
