//
//  SMLeftMenuModuleTableViewHeaderCell.m
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "SMLeftMenuModuleTableViewHeaderCell.h"

@implementation SMLeftMenuModuleTableViewHeaderCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 170*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSInteger bonus = [[[NSUserDefaults standardUserDefaults] valueForKey:@"bonus"] integerValue];
    if(bonus) {
        self.name.text = [NSString stringWithFormat:@"Дисконтная карта: %li баллов", bonus];
    } else {
        self.name.text = [NSString stringWithFormat:@"Дисконтная карта:"];
    }
}

#pragma mark - Методы обработки событий от визуальных элементов

- (void)openCard {
    [MySingleton sharedMySingleton].menu = 11;
//    [self.item openCard];
}

- (void)openProfile {
    [MySingleton sharedMySingleton].menu = 10;
//    [self.item openProfile];
}

#pragma mark - Методы SMLeftMenuModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.avatar = [UIFabric imageViewWithImageName:@"header_new" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];
    
    [self.avatar mas_remakeConstraints:^(MASConstraintMaker *make){
        make.edges.equalTo(self).with.offset(0);
    }];
    
    self.user = [UIFabric imageViewWithImageName:@"ic_menu_user" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openProfile)];
    gesture.numberOfTapsRequired = 1;
    self.user.userInteractionEnabled = YES;
    [self.user addGestureRecognizer:gesture];
    
    [self.user mas_remakeConstraints:^(MASConstraintMaker *make){
        make.width.mas_equalTo(50*[Functions koefX]);
        make.height.mas_equalTo(50*[Functions koefX]);
        make.left.equalTo(self).with.offset(18*[Functions koefX]);
        make.top.equalTo(self).with.offset(25*[Functions koefX]);
    }];
    
    self.card = [UIFabric imageViewWithImageName:@"ic_bank_card" andContentMode:UIViewContentModeScaleAspectFill iconMode:false andSuperView:self];
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openCard)];
    gesture.numberOfTapsRequired = 1;
    self.card.userInteractionEnabled = YES;
    [self.card addGestureRecognizer:gesture];
    
    [self.card mas_remakeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(self).with.offset(18*[Functions koefX]);
        make.bottom.equalTo(self).with.offset(-10);
    }];
    
    self.name = [UIFabric labelWithText:@"Дисконтная карта:" andTextColor:[UIColor whiteColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andSuperView:self];
    self.name.alpha = 0.87f;
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openCard)];
    gesture.numberOfTapsRequired = 1;
    self.name.userInteractionEnabled = YES;
    [self.name addGestureRecognizer:gesture];
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(self).with.offset(70*[Functions koefX]);
        make.bottom.equalTo(self).with.offset(-10);
    }];
    
}

@end
