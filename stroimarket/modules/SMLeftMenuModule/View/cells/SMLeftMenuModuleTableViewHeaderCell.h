//
//  SMLeftMenuModuleTableViewHeaderCell.h
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "SMLeftMenuModuleCellPresenter.h"

@interface SMLeftMenuModuleTableViewHeaderCell : RETableViewCell <SMLeftMenuModuleCellInput>

@property (strong, readwrite, nonatomic) SMLeftMenuModuleHeaderCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *user;
@property (weak, nonatomic) IBOutlet UIImageView *card;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
