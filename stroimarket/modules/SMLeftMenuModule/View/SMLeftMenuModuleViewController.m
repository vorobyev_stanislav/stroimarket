//
//  SMLeftMenuModuleSMLeftMenuModuleViewController.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMLeftMenuModuleViewController.h"
#import "Functions.h"
#import "SMLeftMenuModuleTableViewCell.h"
#import "SMLeftMenuModuleViewOutput.h"
#import "SMCardModuleViewController.h"
#import "SMProfileModuleViewController.h"
#import "AppDelegate.h"
#import "SMSplashModuleViewController.h"
#import "notificationModel.h"

@implementation SMLeftMenuModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

    [[MySingleton sharedMySingleton] bk_removeAllBlockObservers];
    [[MySingleton sharedMySingleton] bk_addObserverForKeyPath:@"menu" task:^(id task){
        NSLog(@"appdelegate wishes changed %@ %li", task, (long)[MySingleton sharedMySingleton].menu);
        [MySingleton sharedMySingleton].reload = 0;
        //        [self viewWillAppear:NO];
        if([MySingleton sharedMySingleton].menu == 1) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.mainController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 2) {
            [MySingleton sharedMySingleton].catalogReload = 0;
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.catalogController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 3) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.selectedGoodsController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 4) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.myOrdersController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 5) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.bonusesController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 6) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.notificationsController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 7) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.shopsController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 8) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.reportController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 9) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.aboutController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 10) {
            [MySingleton sharedMySingleton].edit = NO;
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.profileController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 11) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.cardController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 12) {
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"authToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[RLMRealm defaultRealm] beginWriteTransaction];
            RLMResults *res = [notificationModel allObjects];
            for(notificationModel *cc in res) {
                [[RLMRealm defaultRealm] deleteObject:cc];
            }
            [[RLMRealm defaultRealm] commitWriteTransaction];
            
            AppDelegate *app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
            for(UIViewController *vc in app.firstViewController.viewControllers)
            {
                if([vc isKindOfClass:[SMSplashModuleViewController class]])
                {
                    [app.firstViewController popToViewController:vc animated:NO];
                }
            }
        }
    }];
    
	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [Functions setupNavigationController:self.navigationController andTitle:WCLocalize(@"SMLeftMenuModuleTitle") andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).with.offset(-60);
        make.left.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.view).with.offset(0);
        make.bottom.equalTo(self.view).with.offset(0);
    }];
    [super updateViewConstraints];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    UITableView *tv = [[UITableView alloc] init];
    self.tableView = tv;
    [self.view addSubview:tv];
    self.tableView.bounces = NO;
    tv.backgroundColor = [UIColor clearColor];
    tv.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableManager = [[RETableViewManager alloc] initWithTableView:self.tableView];
    RETableViewSection *dumbSection = [RETableViewSection sectionWithHeaderTitle:@""];
    [self.tableManager addSection:dumbSection];
    self.tableManager[@"SMLeftMenuModuleCellPresenter"] = @"SMLeftMenuModuleTableViewCell";
    self.tableManager[@"SMLeftMenuModuleHeaderCellPresenter"] = @"SMLeftMenuModuleTableViewHeaderCell";
    [self.output setTableViewManager:self.tableManager];
}
#pragma mark - Методы SMLeftMenuModuleViewInput

- (void)needExit {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Вы действительно хотите выйти?" cancelButtonTitle:@"Да" otherButtonTitles:@[@"Нет"] handler:^(UIAlertView *alert, NSInteger buttonIndex) {
        if(buttonIndex == 0) {
            [self.output makeExit];
        }
    }];
}

- (void)openProfile {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SMProfileModuleViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SMProfileModuleViewController"];
    UINavigationController *navCtr = [[UINavigationController alloc] initWithRootViewController:controller];
    [self showViewController:navCtr sender:self];
}

- (void)openCard {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SMCardModuleViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SMCardModuleViewController"];
    UINavigationController *navCtr = [[UINavigationController alloc] initWithRootViewController:controller];
    [self showViewController:navCtr sender:self];
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
