//
//  SMLeftMenuModuleSMLeftMenuModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMLeftMenuModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMLeftMenuModuleRouter : NSObject <SMLeftMenuModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
