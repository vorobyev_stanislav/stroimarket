//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSelectedGoodsModuleAssembly.h"
#import "orderService.h"
#import "SMSelectedGoodsModuleViewController.h"
#import "SMSelectedGoodsModuleInteractor.h"
#import "SMSelectedGoodsModulePresenter.h"
#import "SMSelectedGoodsModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMSelectedGoodsModuleAssembly

- (SMSelectedGoodsModuleViewController *)viewSMSelectedGoodsModuleModule {
    return [TyphoonDefinition withClass:[SMSelectedGoodsModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSelectedGoodsModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMSelectedGoodsModuleModule]];
                          }];
}

- (SMSelectedGoodsModuleInteractor *)interactorSMSelectedGoodsModuleModule {
    return [TyphoonDefinition withClass:[SMSelectedGoodsModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMSelectedGoodsModuleModule]];
                              [definition injectProperty:@selector(orderService)
                                                    with:[self orderServiceSMSelectedGoodsModule]];
                          }];
}

- (SMSelectedGoodsModulePresenter *)presenterSMSelectedGoodsModuleModule {
    return [TyphoonDefinition withClass:[SMSelectedGoodsModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMSelectedGoodsModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMSelectedGoodsModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMSelectedGoodsModuleModule]];

                          }];
}

- (SMSelectedGoodsModuleRouter *)routerSMSelectedGoodsModuleModule {
    return [TyphoonDefinition withClass:[SMSelectedGoodsModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMSelectedGoodsModuleModule]];
                          }];
}

- (orderService *)orderServiceSMSelectedGoodsModule {
    return [TyphoonDefinition withClass:[orderService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
