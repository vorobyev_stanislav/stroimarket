//
//  SMSelectedGoodsModuleSMSelectedGoodsModulePresenter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSelectedGoodsModulePresenter.h"

#import "SMSelectedGoodsModuleViewInput.h"
#import "SMSelectedGoodsModuleInteractorInput.h"
#import "SMSelectedGoodsModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMSelectedGoodsModulePresenter

#pragma mark - Методы SMSelectedGoodsModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMSelectedGoodsModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMSelectedGoodsModuleViewOutput

- (void)makeOrder {
    [self.interactor makeOrder];
}

- (void) viewWillApear {
    [self.interactor getBasket];
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMSelectedGoodsModuleInteractorOutput

- (void)goodsReceived:(float)price {
    [self.view goodsReceived:price];
}

-(void) progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
