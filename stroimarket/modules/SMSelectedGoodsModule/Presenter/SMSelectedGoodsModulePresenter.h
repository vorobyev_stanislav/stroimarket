//
//  SMSelectedGoodsModuleSMSelectedGoodsModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSelectedGoodsModuleViewOutput.h"
#import "SMSelectedGoodsModuleInteractorOutput.h"
#import "SMSelectedGoodsModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMSelectedGoodsModuleViewInput;
@protocol SMSelectedGoodsModuleInteractorInput;
@protocol SMSelectedGoodsModuleRouterInput;

@interface SMSelectedGoodsModulePresenter : NSObject <SMSelectedGoodsModuleModuleInput, SMSelectedGoodsModuleViewOutput, SMSelectedGoodsModuleInteractorOutput>

@property (nonatomic, weak) id<SMSelectedGoodsModuleViewInput> view;
@property (nonatomic, strong) id<SMSelectedGoodsModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMSelectedGoodsModuleRouterInput> router;

@end
