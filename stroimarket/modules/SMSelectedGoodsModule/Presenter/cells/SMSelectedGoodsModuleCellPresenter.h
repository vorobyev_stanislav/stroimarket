//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "orderModel.h"
#import "SMSelectedGoodsModuleInteractorInput.h"

@protocol SMSelectedGoodsModuleCellInput <NSObject>

@end

@interface SMSelectedGoodsModuleCellPresenter : RETableViewItem

@property (strong, readwrite, nonatomic) selectedGoods *model;
@property (nonatomic, strong) id<SMSelectedGoodsModuleCellInput> cell;
@property (weak, nonatomic) id<SMSelectedGoodsModuleInteractorInput> interactor;

- (void)deleteProduct;

@end
