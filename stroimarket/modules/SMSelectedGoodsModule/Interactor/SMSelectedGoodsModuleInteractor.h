//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSelectedGoodsModuleInteractorInput.h"
#import "orderService.h"

@protocol SMSelectedGoodsModuleInteractorOutput;

@interface SMSelectedGoodsModuleInteractor : NSObject <SMSelectedGoodsModuleInteractorInput>

@property (nonatomic, weak) id<SMSelectedGoodsModuleInteractorOutput> output;
@property (nonatomic, strong) orderService *orderService;

@end
