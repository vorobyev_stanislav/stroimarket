//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSelectedGoodsModuleInteractor.h"

#import "SMSelectedGoodsModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMSelectedGoodsModuleCellPresenter.h"

@interface SMSelectedGoodsModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMSelectedGoodsModuleInteractor

#pragma mark - Методы SMSelectedGoodsModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)getBasket {
    [self.output progressShow];
    [self.orderService getBasket:^(NSArray<selectedGoods *> *goods) {
        [self.manager.sections[0] removeAllItems];
        
        float price = 0;
        for(int i=0;i<goods.count;i++) {
            SMSelectedGoodsModuleCellPresenter *pres2 = [SMSelectedGoodsModuleCellPresenter item];
            pres2.model = goods[i];
            pres2.interactor = (id)self;
            pres2.selectionHandler = ^(SMSelectedGoodsModuleCellPresenter *newValue){
                [newValue deselectRowAnimated:YES];
                //[self.output showOrder:nil];
            };
            [self.manager.sections[0] addItem:(id)pres2];
            price = price + [goods[i].price floatValue]*goods[i].quantity;
        }
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
        
        [self.output progressDismiss];
        [self.output goodsReceived:price];
    }];
}

- (void)deleteProduct:(NSInteger)productId {
    [self.output progressShow];
    [self.orderService deleteProductFromBasket:productId completed:^(BOOL success) {
        if(success) {
            [self.orderService getBasket:^(NSArray<selectedGoods *> *goods) {
                [self.manager.sections[0] removeAllItems];
                
                for(int i=0;i<goods.count;i++) {
                    SMSelectedGoodsModuleCellPresenter *pres2 = [SMSelectedGoodsModuleCellPresenter item];
                    pres2.model = goods[i];
                    pres2.interactor = (id)self;
                    pres2.selectionHandler = ^(SMSelectedGoodsModuleCellPresenter *newValue){
                        [newValue deselectRowAnimated:YES];
                        //[self.output showOrder:nil];
                    };
                    [self.manager.sections[0] addItem:(id)pres2];
                }
                
                [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
                
                [self.output progressDismiss];
            }];
        }
    }];
}

- (void)makeOrder {
    [self.output progressShow];
    [self.orderService makeOrder:^(BOOL status) {
        [self.output progressDismiss];
        [self getBasket];
    }];
}

@end
