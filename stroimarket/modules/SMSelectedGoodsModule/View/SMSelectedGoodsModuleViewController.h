//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMSelectedGoodsModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMSelectedGoodsModuleViewOutput;

@interface SMSelectedGoodsModuleViewController : UIViewController <SMSelectedGoodsModuleViewInput>

@property (nonatomic, strong) id<SMSelectedGoodsModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UIButton *makeOrder;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;

@end
