//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleViewController.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSelectedGoodsModuleViewController.h"
#import "Functions.h"
#import "SMSelectedGoodsModuleTableViewCell.h"
#import "SMSelectedGoodsModuleViewOutput.h"

@implementation SMSelectedGoodsModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Отмеченные товары" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:nil];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    
    [self.makeOrder mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40*[Functions koefX]);
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.bottom.equalTo(self.view).with.offset(0);
    }];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.bottom.equalTo(self.makeOrder.mas_top).with.offset(0);
    }];
    [super updateViewConstraints];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (IBAction)action:(id)sender {
    [[SlideNavigationController sharedInstance] openMenu:1 withCompletion:nil];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    
}

- (void)makeOrderClick {
    [self.output makeOrder];
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.makeOrder = [UIFabric buttonWithText:@"ОФОРМИТЬ ЗАКАЗ" andTextColor:[UIColor whiteColor] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    [self.makeOrder addTarget:self action:@selector(makeOrderClick) forControlEvents:UIControlEventTouchUpInside];
    self.makeOrder.hidden = YES;
    
    UITableView *tv = [[UITableView alloc] init];
    self.tableView = tv;
    [self.view addSubview:tv];
    tv.backgroundColor = [UIColor clearColor];
    tv.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableManager = [[RETableViewManager alloc] initWithTableView:self.tableView];
    RETableViewSection *dumbSection = [RETableViewSection sectionWithHeaderTitle:@""];
    [self.tableManager addSection:dumbSection];
    self.tableManager[@"SMSelectedGoodsModuleCellPresenter"] = @"SMSelectedGoodsModuleTableViewCell";
    [self.output setTableViewManager:self.tableManager];
}
#pragma mark - Методы SMSelectedGoodsModuleViewInput

- (void)goodsReceived:(float)price {
    if(price>0) {
        self.makeOrder.hidden = NO;
    }
    [self.makeOrder setTitle:[NSString stringWithFormat:@"ОФОРМИТЬ ЗАКАЗ: %.2f \u20BD", price] forState:UIControlStateNormal];
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
