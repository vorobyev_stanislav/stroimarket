//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleTableViewCell.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSelectedGoodsModuleTableViewCell.h"
#import "Functions.h"

@implementation SMSelectedGoodsModuleTableViewCell

#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 80*[Functions koefX];
}

- (void)cellDidLoad
{
    //[super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    //[super cellWillAppear];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.name.text = self.item.model.name;
    
    self.collection.text = self.item.model.collection;
    
    [self.basket setTitle:[NSString stringWithFormat:@"%.0fx%@ \u20BD", self.item.model.quantity, self.item.model.price] forState:UIControlStateNormal];
    
    [self.photo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", domain, self.item.model.image]] placeholderImage:[UIImage imageNamed:@"ic_empty"] options:0];
    
    NSString *str = [NSString stringWithFormat:@"%.0fx%@ \u20BD", self.item.model.quantity, self.item.model.price];
    CGSize expectedLabelSize = [str sizeWithFont:[UIFont fontWithName:@"Roboto-Regular" size:14*[Functions koefX]]
                                      constrainedToSize:CGSizeMake(300, CGFLOAT_MAX)
                                          lineBreakMode:NSLineBreakByWordWrapping];
    [self.basket mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.right.equalTo(self.backView).with.offset(-20*[Functions koefX]);
        make.width.mas_equalTo(ceil(expectedLabelSize.width + 10));//951337 Анна Вячеславовна
    }];
}

+ (BOOL)canFocusWithItem:(RETableViewItem *)item {
    return NO;
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMSelectedGoodsModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.backView = [UIFabric viewWithBackgroundColor:[UIColor whiteColor] andSuperView:self.swipeContentView];
    
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.swipeContentView);
    }];
    
    self.photo = [UIFabric imageViewWithImageName:@"ic_empty" andContentMode:UIViewContentModeScaleAspectFit iconMode:false andSuperView:self.backView];
    self.photo.clipsToBounds = YES;
    
    [self.photo mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(0);
        make.top.equalTo(self.backView).with.offset(0);
        make.bottom.equalTo(self.backView).with.offset(0);
        make.width.mas_equalTo(60*[Functions koefX]);
    }];

    self.basket = [UIFabric buttonWithText:@"" andTextColor:[UIColor whiteColor] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:14 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.backView];
    
    [self.basket mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.right.equalTo(self.backView).with.offset(-20*[Functions koefX]);
    }];
    
    self.name = [UIFabric labelWithText:@"" andTextColor:[UIColor blackColor] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:14 andSuperView:self.backView];
    self.name.alpha = 0.87f;
    self.name.numberOfLines = 2;
    
    [self.name mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(70*[Functions koefX]);
        make.right.equalTo(self.basket.mas_left).with.offset(-10*[Functions koefX]);
        make.top.equalTo(self.backView).with.offset(10*[Functions koefX]);
    }];
    
    self.collection = [UIFabric labelWithText:@"" andTextColor:[UIColor blackColor] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:14 andSuperView:self.backView];
    self.collection.alpha = 0.54f;
    
    [self.collection mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(70*[Functions koefX]);
        make.right.equalTo(self.basket.mas_left).with.offset(-10*[Functions koefX]);
        make.top.equalTo(self.name.mas_bottom).with.offset(5*[Functions koefX]);
    }];
    
    self.border = [UIFabric viewWithBackgroundColor:[Functions colorWithRGBHex:0x0] andSuperView:self.backView];
    self.border.alpha = 0.3f;
    
    [self.border mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(0);
        make.right.equalTo(self.backView).with.offset(0);
        make.bottom.equalTo(self.backView).with.offset(0);
        make.height.mas_equalTo(0.5f);
    }];
    
    self.deleteGoods = [MGSwipeButton buttonWithTitle:@"Удалить" backgroundColor:[UIColor redColor] callback:^BOOL(MGSwipeTableCell *sender) {
        
        NSLog(@"delete");
        [self.item deleteProduct];
        return YES;
    }];
    self.deleteGoods.titleLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:17*[Functions koefY]];
    [self.deleteGoods setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.rightButtons = @[self.deleteGoods];
}

@end
