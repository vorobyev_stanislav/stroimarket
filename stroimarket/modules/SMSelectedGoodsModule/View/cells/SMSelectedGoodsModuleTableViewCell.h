//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMSelectedGoodsModuleCellPresenter.h"
#import "MGSwipeTableCell.h"

@interface SMSelectedGoodsModuleTableViewCell : MGSwipeTableCell <SMSelectedGoodsModuleCellInput>

@property (strong, readwrite, nonatomic) SMSelectedGoodsModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *collection;
@property (weak, nonatomic) IBOutlet UIButton *basket;
@property (weak, nonatomic) IBOutlet UIView *border;
@property MGSwipeButton *deleteGoods;

@property id tableViewManager;
@property NSInteger rowIndex;
@property NSInteger sectionIndex;
@property UITableView *parentTableView;
@property id section;

@end
