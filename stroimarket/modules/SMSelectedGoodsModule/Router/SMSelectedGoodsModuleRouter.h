//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSelectedGoodsModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMSelectedGoodsModuleRouter : NSObject <SMSelectedGoodsModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
