//
//  SMSelectedGoodsModuleSMSelectedGoodsModuleRouter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMSelectedGoodsModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMSelectedGoodsModuleRouter

#pragma mark - Методы SMSelectedGoodsModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

@end
