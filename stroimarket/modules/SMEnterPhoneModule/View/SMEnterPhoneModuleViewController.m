//
//  SMEnterPhoneModuleSMEnterPhoneModuleViewController.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterPhoneModuleViewController.h"
#import "Functions.h"
#import "SMEnterPhoneModuleTableViewCell.h"
#import "SMEnterPhoneModuleViewOutput.h"

@implementation SMEnterPhoneModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"Вход" andItem:self.navigationItem withLeftBtn:nil andRightBtn:nil];
    
    [self.phone becomeFirstResponder];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    
    [self.phone mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.top.equalTo(self.view).with.offset(35*[Functions koefX]);
        make.centerX.equalTo(self.view);
    }];
    
    [self.border mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(1);
        make.top.equalTo(self.phone.mas_bottom).with.offset(0);
        make.centerX.equalTo(self.view);
    }];
    
    [self.send mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(300*[Functions koefX]);
        make.height.mas_equalTo(40*[Functions koefX]);
        make.top.equalTo(self.phone.mas_bottom).with.offset(30*[Functions koefX]);
        make.centerX.equalTo(self.view);
    }];
    
    [super updateViewConstraints];
}

#pragma mark - Методы обработки событий от визуальных элементов
- (void)backBtnTap {
    [self.output backBtnTap];
}

- (void)moreBtnTap {
    
}

- (void)openEnterCode {
    
    if([self.phone.phoneNumber length] == 11) {
        [self.output loginWithPhone:self.phone.phoneNumber andEmail:@""];
    } else {
        [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Введите номер телефона!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
    }
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.phone = [UIFabric shsPhoneTextField:@"+7" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto-Regular" andFontSize:19 andSuperView:self.view];
    self.phone.alpha = 0.87f;
    [self.phone.formatter setDefaultOutputPattern:@"+#(###)###-##-##"];
    self.phone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Введите ваш номер телефона" attributes:@{NSForegroundColorAttributeName:[colorScheme colorWithSchemeName:@"placeholderColor"]}];
    
    self.border = [UIFabric viewWithBackgroundColor:[UIColor colorWithRed:218.0f/255.0f green:218.0f/255.0f blue:218.0f/255.0f alpha:1.0f] andSuperView:self.view];
    
    self.send = [UIFabric buttonWithText:@"ДАЛЕЕ" andTextColor:[colorScheme colorWithSchemeName:@"backgroundCellColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andBackgroundColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andImage:nil andSuperView:self.view];
    self.send.layer.cornerRadius = 2;
    [self.send addTarget:self action:@selector(openEnterCode) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Методы SMEnterPhoneModuleViewInput

- (void)smsSendedError {
    [UIAlertView bk_showAlertViewWithTitle:@"Строймаркет" message:@"Ошибка при входе!" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
