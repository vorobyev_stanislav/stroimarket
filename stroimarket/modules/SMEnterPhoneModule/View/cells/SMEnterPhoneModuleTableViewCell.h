//
//  SMEnterPhoneModuleSMEnterPhoneModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMEnterPhoneModuleCellPresenter.h"

@interface SMEnterPhoneModuleTableViewCell : RETableViewCell <SMEnterPhoneModuleCellInput>

@property (strong, readwrite, nonatomic) SMEnterPhoneModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
