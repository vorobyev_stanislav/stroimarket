//
//  SMEnterPhoneModuleSMEnterPhoneModuleInteractorInput.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RETableViewManager;
@protocol SMEnterPhoneModuleInteractorInput <NSObject>

- (void) setTableViewManager:(RETableViewManager*)manager;
- (void)loginWithPhone:(NSString *)phone andEmail:(NSString *)email;

@end
