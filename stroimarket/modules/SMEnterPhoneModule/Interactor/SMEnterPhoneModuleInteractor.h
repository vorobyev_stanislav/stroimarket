//
//  SMEnterPhoneModuleSMEnterPhoneModuleInteractor.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterPhoneModuleInteractorInput.h"
#import "networkService.h"

@protocol SMEnterPhoneModuleInteractorOutput;

@interface SMEnterPhoneModuleInteractor : NSObject <SMEnterPhoneModuleInteractorInput>

@property (nonatomic, weak) id<SMEnterPhoneModuleInteractorOutput> output;
@property (nonatomic, strong) networkService *networkService;
@property NSString *phone;

@end
