//
//  SMEnterPhoneModuleSMEnterPhoneModuleInteractor.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterPhoneModuleInteractor.h"

#import "SMEnterPhoneModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMEnterPhoneModuleCellPresenter.h"

@interface SMEnterPhoneModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMEnterPhoneModuleInteractor

#pragma mark - Методы SMEnterPhoneModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)loginWithPhone:(NSString *)phone andEmail:(NSString *)email {
    [self.output progressShow];
    [self.networkService loginWithPhone:phone andEmail:email completed:^(BOOL status) {
        [self.output progressDismiss];
        if(status) {
            [self.output smsSendedSuccess:phone];
        } else {
            [self.output smsSendedError];
        }
    }];
}

@end
