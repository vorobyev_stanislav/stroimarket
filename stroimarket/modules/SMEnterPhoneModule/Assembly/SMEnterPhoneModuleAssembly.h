//
//  SMEnterPhoneModuleSMEnterPhoneModuleAssembly.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Typhoon/Typhoon.h>
//#import <RamblerTyphoonUtils/AssemblyCollector.h>
//#import "BaseModuleAssembly.h"
/**
 @author apple

 SMEnterPhoneModule module
 */
@interface SMEnterPhoneModuleAssembly : TyphoonAssembly// <RamblerInitialAssembly>

@end
