//
//  SMEnterPhoneModuleSMEnterPhoneModuleAssembly.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterPhoneModuleAssembly.h"

#import "SMEnterPhoneModuleViewController.h"
#import "SMEnterPhoneModuleInteractor.h"
#import "SMEnterPhoneModulePresenter.h"
#import "SMEnterPhoneModuleRouter.h"
#import "networkService.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMEnterPhoneModuleAssembly

- (SMEnterPhoneModuleViewController *)viewSMEnterPhoneModuleModule {
    return [TyphoonDefinition withClass:[SMEnterPhoneModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMEnterPhoneModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMEnterPhoneModuleModule]];
                          }];
}

- (SMEnterPhoneModuleInteractor *)interactorSMEnterPhoneModuleModule {
    return [TyphoonDefinition withClass:[SMEnterPhoneModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMEnterPhoneModuleModule]];
                              [definition injectProperty:@selector(networkService)
                                                    with:[self networkServiceSMEnterPhoneModule]];
                          }];
}

- (SMEnterPhoneModulePresenter *)presenterSMEnterPhoneModuleModule {
    return [TyphoonDefinition withClass:[SMEnterPhoneModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMEnterPhoneModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMEnterPhoneModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMEnterPhoneModuleModule]];

                          }];
}

- (SMEnterPhoneModuleRouter *)routerSMEnterPhoneModuleModule {
    return [TyphoonDefinition withClass:[SMEnterPhoneModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMEnterPhoneModuleModule]];
                          }];
}

- (networkService *)networkServiceSMEnterPhoneModule {
    return [TyphoonDefinition withClass:[networkService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
