//
//  SMEnterPhoneModuleSMEnterPhoneModuleRouter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterPhoneModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMEnterPhoneModuleRouter : NSObject <SMEnterPhoneModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
