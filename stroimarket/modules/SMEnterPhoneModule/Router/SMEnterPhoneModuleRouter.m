//
//  SMEnterPhoneModuleSMEnterPhoneModuleRouter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterPhoneModuleRouter.h"
#import "SMEnterErrorModuleModuleInput.h"
#import "SMEnterCodeModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMEnterPhoneModuleRouter

#pragma mark - Методы SMEnterPhoneModuleRouterInput
-(void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openErrorModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to error"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMEnterErrorModuleModuleInput> moduleInput) {
        [moduleInput configureModule];
        return nil;
    }];
}

- (void)openEnterCodeModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to code"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMEnterCodeModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

@end
