//
//  SMEnterPhoneModuleSMEnterPhoneModulePresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterPhoneModuleViewOutput.h"
#import "SMEnterPhoneModuleInteractorOutput.h"
#import "SMEnterPhoneModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMEnterPhoneModuleViewInput;
@protocol SMEnterPhoneModuleInteractorInput;
@protocol SMEnterPhoneModuleRouterInput;

@interface SMEnterPhoneModulePresenter : NSObject <SMEnterPhoneModuleModuleInput, SMEnterPhoneModuleViewOutput, SMEnterPhoneModuleInteractorOutput>

@property (nonatomic, weak) id<SMEnterPhoneModuleViewInput> view;
@property (nonatomic, strong) id<SMEnterPhoneModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMEnterPhoneModuleRouterInput> router;

@end
