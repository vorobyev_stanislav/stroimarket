//
//  SMEnterPhoneModuleSMEnterPhoneModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMEnterPhoneModuleModel.h"

@protocol SMEnterPhoneModuleCellInput <NSObject>

@end

@interface SMEnterPhoneModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMEnterPhoneModuleModel *model;
@property (nonatomic, strong) id<SMEnterPhoneModuleCellInput> cell;
@end
