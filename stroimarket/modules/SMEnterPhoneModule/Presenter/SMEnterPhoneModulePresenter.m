//
//  SMEnterPhoneModuleSMEnterPhoneModulePresenter.m
//  stroimarket
//
//  Created by apple on 08/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMEnterPhoneModulePresenter.h"

#import "SMEnterPhoneModuleViewInput.h"
#import "SMEnterPhoneModuleInteractorInput.h"
#import "SMEnterPhoneModuleRouterInput.h"
#import "RETableViewManager.h"

@implementation SMEnterPhoneModulePresenter

#pragma mark - Методы SMEnterPhoneModuleModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы SMEnterPhoneModuleModuleOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {

}

#pragma mark - Методы SMEnterPhoneModuleViewOutput

- (void)loginWithPhone:(NSString *)phone andEmail:(NSString *)email {
    [self.interactor loginWithPhone:phone andEmail:email];
}

- (void)openEnterCode:(NSString *)phone {
    
    if([phone length]) {
        [MySingleton sharedMySingleton].profile.phone = phone;
        [self.router openEnterCodeModuleWithParams:@{@"phone" : phone}];
    } else {
        [self.router openErrorModuleWithParams:@{}];
    }
}

- (void) viewWillApear {
}

- (void) setTableViewManager:(RETableViewManager*)manager {
    [self.interactor setTableViewManager:manager];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)backBtnTap {
    [self.router goBack];
}
#pragma mark - Методы SMEnterPhoneModuleInteractorOutput

- (void)smsSendedSuccess:(NSString *)phone {
    [self.router openEnterCodeModuleWithParams:@{@"phone" : phone}];
}

- (void)smsSendedError {
//    [self.router openErrorModuleWithParams:@{}];
    [self.view smsSendedError];
}
- (void)progressShow {
    [self.view progressShow];
}

-(void) progressDismiss {
    [self.view progressDismiss];
}
@end
