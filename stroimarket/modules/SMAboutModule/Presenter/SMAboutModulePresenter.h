//
//  SMAboutModuleSMAboutModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMAboutModuleViewOutput.h"
#import "SMAboutModuleInteractorOutput.h"
#import "SMAboutModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMAboutModuleViewInput;
@protocol SMAboutModuleInteractorInput;
@protocol SMAboutModuleRouterInput;

@interface SMAboutModulePresenter : NSObject <SMAboutModuleModuleInput, SMAboutModuleViewOutput, SMAboutModuleInteractorOutput>

@property (nonatomic, weak) id<SMAboutModuleViewInput> view;
@property (nonatomic, strong) id<SMAboutModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMAboutModuleRouterInput> router;

@end
