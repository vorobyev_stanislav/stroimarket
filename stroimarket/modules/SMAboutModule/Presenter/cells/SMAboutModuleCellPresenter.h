//
//  SMAboutModuleSMAboutModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
//#import "SMAboutModuleModel.h"

@protocol SMAboutModuleCellInput <NSObject>

@end

@interface SMAboutModuleCellPresenter : RETableViewItem
//@property (strong, readwrite, nonatomic) SMAboutModuleModel *model;
@property (nonatomic, strong) id<SMAboutModuleCellInput> cell;
@end
