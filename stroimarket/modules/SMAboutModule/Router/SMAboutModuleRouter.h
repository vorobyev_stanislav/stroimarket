//
//  SMAboutModuleSMAboutModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMAboutModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMAboutModuleRouter : NSObject <SMAboutModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
