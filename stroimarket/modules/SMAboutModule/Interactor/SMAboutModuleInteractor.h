//
//  SMAboutModuleSMAboutModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMAboutModuleInteractorInput.h"


@protocol SMAboutModuleInteractorOutput;

@interface SMAboutModuleInteractor : NSObject <SMAboutModuleInteractorInput>

@property (nonatomic, weak) id<SMAboutModuleInteractorOutput> output;

@end
