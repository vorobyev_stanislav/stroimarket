//
//  SMAboutModuleSMAboutModuleInteractorOutput.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMAboutModuleInteractorOutput <NSObject>
-(void) progressShow;
-(void) progressDismiss;
@end
