//
//  SMAboutModuleSMAboutModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMAboutModuleInteractor.h"

#import "SMAboutModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMAboutModuleCellPresenter.h"

@interface SMAboutModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMAboutModuleInteractor

#pragma mark - Методы SMAboutModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}
@end
