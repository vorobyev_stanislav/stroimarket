//
//  SMAboutModuleSMAboutModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMAboutModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMAboutModuleViewOutput;

@interface SMAboutModuleViewController : UIViewController <SMAboutModuleViewInput>

@property (nonatomic, strong) id<SMAboutModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UILabel *text;

@end
