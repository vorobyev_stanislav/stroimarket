//
//  SMAboutModuleSMAboutModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMAboutModuleCellPresenter.h"

@interface SMAboutModuleTableViewCell : RETableViewCell <SMAboutModuleCellInput>

@property (strong, readwrite, nonatomic) SMAboutModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
