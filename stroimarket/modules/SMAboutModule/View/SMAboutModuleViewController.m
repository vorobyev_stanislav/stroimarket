//
//  SMAboutModuleSMAboutModuleViewController.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMAboutModuleViewController.h"
#import "Functions.h"
#import "SMAboutModuleTableViewCell.h"
#import "SMAboutModuleViewOutput.h"

@implementation SMAboutModuleViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    for(id v in self.navigationController.navigationBar.subviews) {
        [v removeFromSuperview];
    }
    
    [Functions setupNavigationController:self.navigationController andTitle:@"О компании" andItem:self.navigationItem withLeftBtn:[Functions setBackButton:self action:@selector(backBtnTap)] andRightBtn:[Functions setMoreButton:self action:@selector(moreBtnTap)]];
    
    [self.output viewWillApear];   
}

- (void)updateViewConstraints {
    [self.text mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(20*[Functions koefX]);
        make.top.equalTo(self.view).with.offset(20*[Functions koefX]);
        make.right.equalTo(self.view).with.offset(-20*[Functions koefX]);
    }];
    [super updateViewConstraints];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (IBAction)action:(id)sender {
    [[SlideNavigationController sharedInstance] openMenu:1 withCompletion:nil];
}

#pragma mark - Методы обработки событий от визуальных элементов
-(void)backBtnTap {
    [self.output backBtnTap];
}

-(void)moreBtnTap {
    [MySingleton sharedMySingleton].menu = 3;
}

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    
    self.text = [UIFabric labelWithText:@"КерамикКлаб уже более 20 лет работает на интерьерном рынке Юга России. В настоящее время в группу компании входят пять салонов в Ставрополе, Пятигорске, Черкесске и Краснодаре. Будучи признанным лидером в регионе, «КерамикКлаб» имеет широкую географию оптовых поставок, сотрудничает с дизайнерскими студиями, архитектурными бюро и компаниями застройщиками." andTextColor:[UIColor blackColor] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.view];
    self.text.numberOfLines = 0;
}
#pragma mark - Методы SMAboutModuleViewInput

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
    [self createViewElements];
}

- (void)progressShow {
    [SVProgressHUD show];
}

- (void)progressDismiss {
    [SVProgressHUD dismiss];
}

@end
