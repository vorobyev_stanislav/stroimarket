//
//  SMAboutModuleSMAboutModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMAboutModuleAssembly.h"

#import "SMAboutModuleViewController.h"
#import "SMAboutModuleInteractor.h"
#import "SMAboutModulePresenter.h"
#import "SMAboutModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMAboutModuleAssembly

- (SMAboutModuleViewController *)viewSMAboutModuleModule {
    return [TyphoonDefinition withClass:[SMAboutModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMAboutModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMAboutModuleModule]];
                          }];
}

- (SMAboutModuleInteractor *)interactorSMAboutModuleModule {
    return [TyphoonDefinition withClass:[SMAboutModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMAboutModuleModule]];
                          }];
}

- (SMAboutModulePresenter *)presenterSMAboutModuleModule {
    return [TyphoonDefinition withClass:[SMAboutModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMAboutModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMAboutModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMAboutModuleModule]];

                          }];
}

- (SMAboutModuleRouter *)routerSMAboutModuleModule {
    return [TyphoonDefinition withClass:[SMAboutModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMAboutModuleModule]];
                          }];
}

@end
