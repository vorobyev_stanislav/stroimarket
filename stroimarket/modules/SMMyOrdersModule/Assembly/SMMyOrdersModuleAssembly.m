//
//  SMMyOrdersModuleSMMyOrdersModuleAssembly.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMyOrdersModuleAssembly.h"
#import "orderService.h"
#import "SMMyOrdersModuleViewController.h"
#import "SMMyOrdersModuleInteractor.h"
#import "SMMyOrdersModulePresenter.h"
#import "SMMyOrdersModuleRouter.h"

//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"


@implementation SMMyOrdersModuleAssembly

- (SMMyOrdersModuleViewController *)viewSMMyOrdersModuleModule {
    return [TyphoonDefinition withClass:[SMMyOrdersModuleViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMMyOrdersModuleModule]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSMMyOrdersModuleModule]];
                          }];
}

- (SMMyOrdersModuleInteractor *)interactorSMMyOrdersModuleModule {
    return [TyphoonDefinition withClass:[SMMyOrdersModuleInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSMMyOrdersModuleModule]];
                              [definition injectProperty:@selector(orderService)
                                                    with:[self orderServiceSMMyOrdersModule]];
                          }];
}

- (SMMyOrdersModulePresenter *)presenterSMMyOrdersModuleModule {
    return [TyphoonDefinition withClass:[SMMyOrdersModulePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSMMyOrdersModuleModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSMMyOrdersModuleModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSMMyOrdersModuleModule]];

                          }];
}

- (SMMyOrdersModuleRouter *)routerSMMyOrdersModuleModule {
    return [TyphoonDefinition withClass:[SMMyOrdersModuleRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSMMyOrdersModuleModule]];
                          }];
}

- (orderService *)orderServiceSMMyOrdersModule {
    return [TyphoonDefinition withClass:[orderService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
