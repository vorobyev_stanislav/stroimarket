//
//  SMMyOrdersModuleSMMyOrdersModulePresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMyOrdersModuleViewOutput.h"
#import "SMMyOrdersModuleInteractorOutput.h"
#import "SMMyOrdersModuleModuleInput.h"
//#import "BasePresenter.h"

@protocol SMMyOrdersModuleViewInput;
@protocol SMMyOrdersModuleInteractorInput;
@protocol SMMyOrdersModuleRouterInput;

@interface SMMyOrdersModulePresenter : NSObject <SMMyOrdersModuleModuleInput, SMMyOrdersModuleViewOutput, SMMyOrdersModuleInteractorOutput>

@property (nonatomic, weak) id<SMMyOrdersModuleViewInput> view;
@property (nonatomic, strong) id<SMMyOrdersModuleInteractorInput> interactor;
@property (nonatomic, strong) id<SMMyOrdersModuleRouterInput> router;

@end
