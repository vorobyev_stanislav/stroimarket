//
//  SMMyOrdersModuleSMMyOrdersModuleCellPresenter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <RETableViewManager/RETableViewManager.h>
#import "orderModel.h"

@protocol SMMyOrdersModuleCellInput <NSObject>

@end

@interface SMMyOrdersModuleCellPresenter : RETableViewItem
@property (strong, readwrite, nonatomic) order *model;
@property (nonatomic, strong) id<SMMyOrdersModuleCellInput> cell;
@end
