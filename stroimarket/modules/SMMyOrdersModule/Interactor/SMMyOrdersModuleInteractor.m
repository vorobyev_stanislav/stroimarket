//
//  SMMyOrdersModuleSMMyOrdersModuleInteractor.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMyOrdersModuleInteractor.h"

#import "SMMyOrdersModuleInteractorOutput.h"
#import "RETableViewManager.h"
#import "RETableViewSection.h"
#import "SMMyOrdersModuleCellPresenter.h"

@interface SMMyOrdersModuleInteractor()
@property (weak) RETableViewManager* manager;
@end

@implementation SMMyOrdersModuleInteractor

#pragma mark - Методы SMMyOrdersModuleInteractorInput
- (void) setTableViewManager:(RETableViewManager*)manager {
    _manager = manager;
}

- (void)getMyOrders {
    
    [self.orderService getOrders:^(NSArray<order *> *orders) {
        [self.manager.sections[0] removeAllItems];
        
        for(int i=0;i<orders.count;i++) {
            SMMyOrdersModuleCellPresenter *pres2 = [SMMyOrdersModuleCellPresenter item];
            pres2.model = orders[i];
            pres2.selectionHandler = ^(SMMyOrdersModuleCellPresenter *newValue){
                [newValue deselectRowAnimated:YES];
                [self.output showOrder:nil];
            };
            [self.manager.sections[0] addItem:(id)pres2];
        }
        
        [self.manager.sections[0] reloadSectionWithAnimation:UITableViewRowAnimationNone];
    }];
}

@end
