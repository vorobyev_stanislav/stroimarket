//
//  SMMyOrdersModuleSMMyOrdersModuleInteractor.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMyOrdersModuleInteractorInput.h"
#import "orderService.h"

@protocol SMMyOrdersModuleInteractorOutput;

@interface SMMyOrdersModuleInteractor : NSObject <SMMyOrdersModuleInteractorInput>

@property (nonatomic, weak) id<SMMyOrdersModuleInteractorOutput> output;
@property (nonatomic, strong) orderService *orderService;

@end
