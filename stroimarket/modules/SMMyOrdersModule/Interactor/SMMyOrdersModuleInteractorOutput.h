//
//  SMMyOrdersModuleSMMyOrdersModuleInteractorOutput.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SMMyOrdersModuleInteractorOutput <NSObject>

- (void)progressShow;
- (void)progressDismiss;
- (void)showOrder:(id)order;

@end
