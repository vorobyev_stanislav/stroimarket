//
//  SMMyOrdersModuleSMMyOrdersModuleRouter.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMyOrdersModuleRouter.h"
#import "SMOrderDetailModuleModuleInput.h"
//#import <ViperMcFlurry/ViperMcFlurry.h>
#import "ViperMcFlurry.h"

@implementation SMMyOrdersModuleRouter

#pragma mark - Методы SMMyOrdersModuleRouterInput
- (void)goBack {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openOrderDetailModuleWithParams:(NSDictionary *)params {
    [[self.transitionHandler openModuleUsingSegue:@"to order"] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SMOrderDetailModuleModuleInput> moduleInput) {
        [moduleInput configureModule:params];
        return nil;
    }];
}

@end
