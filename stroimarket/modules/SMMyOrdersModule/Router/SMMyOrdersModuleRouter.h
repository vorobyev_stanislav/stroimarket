//
//  SMMyOrdersModuleSMMyOrdersModuleRouter.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMyOrdersModuleRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SMMyOrdersModuleRouter : NSObject <SMMyOrdersModuleRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
