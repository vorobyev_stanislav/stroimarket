//
//  SMMyOrdersModuleSMMyOrdersModuleTableViewCell.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import <RETableViewManager/RETableViewManager.h>
#import "SMMyOrdersModuleCellPresenter.h"

@interface SMMyOrdersModuleTableViewCell : RETableViewCell <SMMyOrdersModuleCellInput>

@property (strong, readwrite, nonatomic) SMMyOrdersModuleCellPresenter *item;

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *order;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *price;

@end
