//
//  SMMyOrdersModuleSMMyOrdersModuleTableViewCell.m
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import "SMMyOrdersModuleTableViewCell.h"
#import "Functions.h"

@implementation SMMyOrdersModuleTableViewCell
#pragma mark - Методы RETableViewCell
+ (CGFloat)heightWithItem:(RETableViewItem *)item tableViewManager:(RETableViewManager *)tableViewManager {
    return 90*[Functions koefX];
}

- (void)cellDidLoad
{
    [super cellDidLoad];
    [self createViewElements];
    
}

- (void)cellWillAppear
{
    [super cellWillAppear];
    
    self.order.text = [NSString stringWithFormat:@"Заказ №%li", self.item.model.orderId];
    
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"dd.MM.y HH:mm:ss"];
    NSDate *dd = [df dateFromString:self.item.model.date_order];
    [df setDateFormat:@"dd.MM.y"];
    self.date.text = [NSString stringWithFormat:@"%@", [df stringFromDate:dd]];
    
    //self.order.text = [NSString stringWithFormat:@"Заказ №%li", self.item.model.orderId];
}

#pragma mark - Методы обработки событий от визуальных элементов

#pragma mark - Методы SMMyOrdersModuleCellInput

#pragma mark - Вспомогательные функции
- (void) createViewElements {
    self.backgroundColor = [UIColor clearColor];
    
    self.backView = [UIFabric viewWithBackgroundColor:[colorScheme colorWithSchemeName:@"backgroundCellColor"] andSuperView:self];
    self.backView.layer.cornerRadius = 5;
    
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(5);
        make.right.equalTo(self).with.offset(-5);
        make.top.equalTo(self).with.offset(5);
        make.bottom.equalTo(self).with.offset(0);
    }];
    
    self.order = [UIFabric labelWithText:@"Заказ №12345" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.backView];
    self.order.alpha = 0.87f;

    [self.order mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(10);
        make.top.equalTo(self.backView).with.offset(5);
    }];
    
    self.date = [UIFabric labelWithText:@"07.04.2017" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.backView];
    self.date.alpha = 0.54f;
    
    [self.date mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(10);
        make.top.equalTo(self.order.mas_bottom).with.offset(1);
    }];
    
    self.status = [UIFabric labelWithText:@"Статус: принят" andTextColor:[colorScheme colorWithSchemeName:@"menuTextColor"] andFontName:@"Roboto" andFontModificator:@"Regular" andFontSize:16 andSuperView:self.backView];
    self.status.alpha = 0.54f;
    
    [self.status mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).with.offset(10);
        make.top.equalTo(self.date.mas_bottom).with.offset(1);
    }];


    self.price = [UIFabric labelWithText:@"2234 P" andTextColor:[colorScheme colorWithSchemeName:@"navigationBarColor"] andFontName:@"Roboto" andFontModificator:@"Medium" andFontSize:16 andSuperView:self.backView];
    
    [self.price mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView).with.offset(-10);
        make.top.equalTo(self.backView).with.offset(10);
    }];
}
@end
