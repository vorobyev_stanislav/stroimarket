//
//  SMMyOrdersModuleSMMyOrdersModuleViewController.h
//  stroimarket
//
//  Created by apple on 07/04/2017.
//  Copyright © 2017 Vorobyev S.A.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFabric.h"
#import "SMMyOrdersModuleViewInput.h"
#import "RETableViewManager.h"

@protocol SMMyOrdersModuleViewOutput;

@interface SMMyOrdersModuleViewController : UIViewController <SMMyOrdersModuleViewInput>

@property (nonatomic, strong) id<SMMyOrdersModuleViewOutput> output;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RETableViewManager *tableManager;

@end
