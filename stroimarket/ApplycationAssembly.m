//
//  ApplycationAssembly.m
//  worldCore
//
//  Created by Maxim Gall on 30.03.16.
//  Copyright © 2016 Maxim Gall. All rights reserved.
//

#import "ApplycationAssembly.h"
#import "AppDelegate.h"
#import "notificationService.h"

@interface ApplycationAssembly ()

@end

@implementation ApplycationAssembly
- (AppDelegate *)appDelegate {
    return [TyphoonDefinition withClass:[AppDelegate class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(mainController) with:[self mainViewController]];
                              [definition injectProperty:@selector(catalogController) with:[self catalogViewController]];
                              [definition injectProperty:@selector(selectedGoodsController) with:[self selectedGoodsViewController]];
                              [definition injectProperty:@selector(myOrdersController) with:[self myOrdersViewController]];
                              [definition injectProperty:@selector(bonusesController) with:[self bonusesViewController]];
                              [definition injectProperty:@selector(notificationsController) with:[self NotificationsViewController]];
                              [definition injectProperty:@selector(shopsController) with:[self shopsViewController]];
                              [definition injectProperty:@selector(reportController) with:[self reportViewController]];
                              [definition injectProperty:@selector(aboutController) with:[self aboutViewController]];
                              [definition injectProperty:@selector(notificationService) with:[self notificationServiceAppDelegateModule]];
                        }];
}

- (SlideNavigationController *) firstViewController {
    
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                            [factoryMethod injectParameterWith:@"SMSlideNavigationModuleViewController"];
                                        }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                    }];
}

- (UIViewController *)mainViewController {
    
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMSplashModuleViewController"];
//                                             [factoryMethod injectParameterWith:@"SMRegisterModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
    
}

- (UIViewController *)catalogViewController {
    
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMCatalogModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)selectedGoodsViewController {
    
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMSelectedGoodsModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)myOrdersViewController {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMMyOrdersModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)bonusesViewController {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMBonusesModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)NotificationsViewController {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMNotificationsModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)shopsViewController {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMShopsModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)reportViewController {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMReportModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIViewController *)aboutViewController {
    return [TyphoonStoryboardDefinition withFactory:[self mainStoryboard]
                                           selector:@selector(instantiateViewControllerWithIdentifier:)
                                         parameters:^(TyphoonMethod *factoryMethod) {
                                             [factoryMethod injectParameterWith:@"SMAboutModuleViewController"];
                                         }
                                      configuration:^(TyphoonFactoryDefinition *definition) {
                                      }];
}

- (UIStoryboard*)mainStoryboard {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}

- (notificationService *)notificationServiceAppDelegateModule {
    return [TyphoonDefinition withClass:[notificationService class]
                          configuration:^(TyphoonDefinition *definition) {
                              
                          }];
}

@end
