//
//  AppDelegate.m
//  stroimarket
//
//  Created by apple on 07.04.17.
//  Copyright © 2017 Vorobyev Stanislav. All rights reserved.
//

#import "AppDelegate.h"
#import "SMLeftMenuModuleViewController.h"
@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
@import UIKit;

@interface AppDelegate () {

NSString *InstanceID;
    
}
@end

@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [FIRApp configure];
    
    [FIRMessaging messaging].remoteMessageDelegate = self;
    
    [MySingleton sharedMySingleton].profile = [profile new];
    
    [[MySingleton sharedMySingleton] bk_addObserverForKeyPath:@"menu" task:^(id task){
        NSLog(@"appdelegate wishes changed %@ %li", task, (long)[MySingleton sharedMySingleton].menu);
        [MySingleton sharedMySingleton].reload = 0;
        //        [self viewWillAppear:NO];
        if([MySingleton sharedMySingleton].menu == 1) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.mainController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 2) {
            [MySingleton sharedMySingleton].catalogReload = 0;
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.catalogController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 3) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.selectedGoodsController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 4) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.myOrdersController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 5) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.bonusesController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 6) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.notificationsController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 7) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.shopsController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 8) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.reportController withCompletion:nil];
        } else if([MySingleton sharedMySingleton].menu == 9) {
            [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:self.aboutController withCompletion:nil];
        }
    }];
    
    [Fabric with:@[[Crashlytics class]]];
    //AIzaSyCNdrDIRRM-TT_gVBbW1TRLsSHdrCfj2LU
    //[GMSServices provideAPIKey:@"AIzaSyDZTVTmbLQIyGvtJ2fY6erE-ipdqnBvYnw"];
    [GMSServices provideAPIKey:@"AIzaSyCNdrDIRRM-TT_gVBbW1TRLsSHdrCfj2LU"];
    
    
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
//        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
//            UIUserNotificationType allNotificationTypes =
//            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
//            UIUserNotificationSettings *settings =
//            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
//            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
//        } else {
//            // iOS 10 or later
//#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
//            // For iOS 10 display notification (sent via APNS)
//            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
//            UNAuthorizationOptions authOptions =
//            UNAuthorizationOptionAlert
//            | UNAuthorizationOptionSound
//            | UNAuthorizationOptionBadge;
//            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
//            }];
//#endif
        }
    
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (localNotif) {
        NSString *json = [localNotif valueForKey:@"data"];
        // Parse your string to dictionary
        NSDictionary *dict = [json valueForKey:@"aps"];
        NSLog(@"alert %@", [dict valueForKey:@"alert"]);
        
        [self.notificationService addNotification:[dict valueForKey:@"alert"] completed:^() {
            
        }];
    }
    
    //    [SVProgressHUD setBackgroundColor:[UIColor blackColor]];
    //    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    //    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    self.firstViewController = [[SlideNavigationController alloc] initWithRootViewController:self.mainController];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    SMLeftMenuModuleViewController *leftMenu = (SMLeftMenuModuleViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"SMLeftMenuModuleViewController"];
    
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;//self.leftController;
    [SlideNavigationController sharedInstance].enableShadow = YES;
    [SlideNavigationController sharedInstance].enableSwipeGesture = YES;
    
    [MySingleton sharedMySingleton].menu = 0;
    [MySingleton sharedMySingleton].reload = 0;
    
    self.window.rootViewController = self.firstViewController;
    
    return YES;
}

#pragma push
    // [START receive_message]
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
//    NSLog(@"push %@", userInfo);
//    Notification *item = [Notification MR_createEntity];
//    if ([[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] isKindOfClass:[NSString class]]) {
//        item.title = @"Сообщение";
//        item.body = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
//    }else{
//        NSDictionary *dict = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
//        item.title = [dict objectForKey:@"title"];
//        item.body = [dict objectForKey:@"body"];
//    }
//    item.type = @"1";
//    item.date = [NSDate new];
//    
//    
//    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
//    
//    [RNNotificationView showWithImage:nil title:item.title message:item.body duration:5 iconSize:CGSizeMake(22, 22) onTap:nil];
    
//}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSLog(@"instanceId_notification=>%@",[notification object]);
    InstanceID = [NSString stringWithFormat:@"%@",[notification object]];
    
    [self connectToFcm];
}

- (void)connectToFcm {
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            
            NSLog(@"InstanceID_connectToFcm = %@", InstanceID);
            [[NSUserDefaults standardUserDefaults] setValue:InstanceID forKey:@"fcmToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [self sendDeviceInfo];
                    NSLog(@"instanceId_tokenRefreshNotification22=>%@",[[FIRInstanceID instanceID] token]);
                    [[FIRMessaging messaging] subscribeToTopic:@"/topics/ceramicclub"];
                    [[FIRMessaging messaging] subscribeToTopic:@"/topics/1"];
                    NSLog(@"Subscribed to news topic");
                    
                });
            });
            
            
        }
    }];
}

// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[Messaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"userInfo %@", userInfo);
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[Messaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"fetch %@", userInfo);
    
    NSLog(@"aps %@", userInfo[@"aps"]);
    NSDictionary *dict = [userInfo valueForKey:@"aps"];
    NSLog(@"alert %@", [dict valueForKey:@"alert"]);
    NSDictionary *alert = [dict valueForKey:@"alert"];
    [self.notificationService addNotification:[alert valueForKey:@"body"] completed:^() {
        
    }];
    
    completionHandler(UIBackgroundFetchResultNewData);
}
// [END receive_message]


// [START refresh_token]
- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSLog(@"FCM registration token: %@", fcmToken);
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START ios_10_data_message]
// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
// To enable direct data messages, you can set [Messaging messaging].shouldEstablishDirectChannel to YES.
- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    NSLog(@"Received data message: %@", remoteMessage.appData);
}
// [END ios_10_data_message]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs device token can be paired to
// the FCM registration token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"APNs device token retrieved: %@", deviceToken);
    
    // With swizzling disabled you must set the APNs device token here.
//     [FIMessaging messaging].APNSToken = deviceToken;
    
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
}

/*- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    NSLog(@"notification %@", userInfo);
    
//    Notification *item = [Notification MR_createEntity];
//    if ([[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] isKindOfClass:[NSString class]]) {
//        item.title = @"Сообщение";
//        item.body = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
//    }else{
//        NSDictionary *dict = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
//        item.title = [dict objectForKey:@"title"];
//        item.body = [dict objectForKey:@"body"];
//    }
//    item.type = @"1";
//    item.date = [NSDate new];
//    
//    
//    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
//    
//    [RNNotificationView showWithImage:nil title:item.title message:item.body duration:5 iconSize:CGSizeMake(22, 22) onTap:nil];
    // Print full message.
    
}
    // [END receive_message]
    
    
    // [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}
    // [END refresh_token]
    
    // [START connect_to_fcm]
- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}
    // [END connect_to_fcm]
    
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the InstanceID token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSLog(@"APNs token retrieved: %@", deviceToken);
    //With swizzling disabled you must set the APNs token here.
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
    
    //
    //    //If debug build:
    //    FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .Sandbox)
    //
    //    //If production build:
    //    FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .Prod)
    //
    //    //Or let firebase find out the type of build:
    //    FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .Unknown)
}*/

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
